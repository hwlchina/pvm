package com.baidu.iit.pxp.test;

import com.baidu.iit.pvm.runtime.ProcessInstance;
import com.baidu.iit.pxp.PxpServiceManager;
import com.baidu.iit.pxp.converter.BpmnXMLConverter;
import com.baidu.iit.pxp.io.InputStreamProvider;
import com.baidu.iit.pxp.io.InputStreamSource;
import com.baidu.iit.pxp.model.BpmnModel;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:39
 */
public class ConverterTest {

    @Test
    public void xmlToModule(){

        BpmnXMLConverter converter=new BpmnXMLConverter();
        InputStream inputStream=  ConverterTest.class.getResourceAsStream("testParseDiagramInterchangeElements.xml");
        InputStreamProvider inputStreamSource=new InputStreamSource(inputStream);
        BpmnModel model= converter.convertToBpmnModel(inputStreamSource, false,false);

        Assert.assertNotNull(model);

    }


    @Test
    public void xmlToProcessDefinetion(){

        InputStream inputStream=  ConverterTest.class.getResourceAsStream("testParseDiagramInterchangeElements.xml");

        PxpServiceManager pxpServiceManager=new PxpServiceManager();

        List<ProcessInstance> processInstances= pxpServiceManager.createProcessInstance(inputStream);

       Assert.assertSame(1,processInstances.size());
    }
}

package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmEventConstacts;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.WaitState;

/**
 * 流程结束事件测试
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:46
 */
public class PvmProcessInstanceEndTest extends PvmTestCase {

    public void testSimpleProcessInstanceEnd() {
        EventCollector eventCollector = new EventCollector();

        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("wait")
                .endActivity()
                .createActivity("wait")
                .behavior(new WaitState())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();
        System.err.println(eventCollector);
        processInstance.deleteCascade("test");
        System.err.println();
        System.err.println(eventCollector);
    }
}

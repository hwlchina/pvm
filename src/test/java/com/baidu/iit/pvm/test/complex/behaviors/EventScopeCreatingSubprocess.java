package com.baidu.iit.pvm.test.complex.behaviors;

import com.baidu.iit.pvm.PvmActivity;
import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.CompositeActivityBehavior;
import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pvm.runtime.InterpretableExecution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:29
 */
public class EventScopeCreatingSubprocess implements CompositeActivityBehavior {

    public void execute(ActivityExecution execution) throws Exception {
        List<PvmActivity> startActivities = new ArrayList<PvmActivity>();
        for (PvmActivity activity: execution.getActivity().getActivities()) {
            if (activity.getIncomingTransitions().isEmpty()) {
                startActivities.add(activity);
            }
        }

        for (PvmActivity startActivity: startActivities) {
            execution.executeActivity(startActivity);
        }
    }

    public void lastExecutionEnded(ActivityExecution execution) {

        ActivityExecution outgoingExecution = execution.getParent().createExecution();
        outgoingExecution.setConcurrent(false);
        ((InterpretableExecution)outgoingExecution).setActivity((ActivityImpl) execution.getActivity());

        execution.setConcurrent(false);
        execution.setActive(false);
        ((InterpretableExecution)execution).setEventScope(true);

        List<PvmTransition> outgoingTransitions = execution.getActivity().getOutgoingTransitions();
        if(outgoingTransitions.isEmpty()) {
            outgoingExecution.end();
        }else {
            outgoingExecution.takeAll(outgoingTransitions, Collections.EMPTY_LIST);
        }
    }


    @SuppressWarnings("unchecked")
    public void timerFires(ActivityExecution execution, String signalName, Object signalData) throws Exception {
        PvmActivity timerActivity = execution.getActivity();
        boolean isInterrupting = (Boolean) timerActivity.getProperty("isInterrupting");
        List<ActivityExecution> recyclableExecutions = null;
        if (isInterrupting) {
            recyclableExecutions = removeAllExecutions(execution);
        } else {
            recyclableExecutions = Collections.EMPTY_LIST;
        }
        execution.takeAll(timerActivity.getOutgoingTransitions(), recyclableExecutions);
    }

    private List<ActivityExecution> removeAllExecutions(ActivityExecution execution) {
        return null;
    }

}

package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.*;
import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pvm.process.ProcessDefinitionImpl;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.End;
import com.baidu.iit.pvm.test.complex.behaviors.WaitState;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:47
 */
public class PvmScopeAndEventsTest extends PvmTestCase {

    /**
     * +--------------------------------------------------------------------------------+
     * | mostOuterNestedActivity                                                        |
     * | +----------------------------------------------------------------------------+ |
     * | | outerScope (scope)                                                         | |
     * | | +----------------------------------+ +-----------------------------------+ | |
     * | | | firstInnerScope (scope)          | | secondInnerScope (scope)          | | |
     * | | | +------------------------------+ | | +-------------------------------+ | | |
     * | | | | firstMostInnerNestedActivity | | | | secondMostInnerNestedActivity | | | |
     * | | | | +-------+   +-------------+  | | | | +--------------+    +-----+   | | | |
     * | | | | | start |-->| waitInFirst |--------->| waitInSecond |--> | end |   | | | |
     * | | | | +-------+   +-------------+  | | | | +--------------+    +-----+   | | | |
     * | | | +------------------------------+ | | +-------------------------------+ | | |
     * | | +----------------------------------+ +-----------------------------------+ | |
     * | +----------------------------------------------------------------------------+ |
     * +--------------------------------------------------------------------------------+
     */
    public void testStartEndWithScopesAndNestedActivities() {
        EventCollector eventCollector = new EventCollector();

        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder("scopes and events")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("mostOuterNestedActivity")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("outerScope")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("firstInnerScope")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("firstMostInnerNestedActivity")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("waitInFirst")
                .endActivity()
                .createActivity("waitInFirst")
                .behavior(new WaitState())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("waitInSecond")
                .endActivity()
                .endActivity()
                .endActivity()
                .createActivity("secondInnerScope")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("secondMostInnerNestedActivity")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("waitInSecond")
                .behavior(new WaitState())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .endActivity()
                .endActivity()
                .endActivity()
                .endActivity()
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> expectedEvents = new ArrayList<String>();
        expectedEvents.add("start on ProcessDefinition(scopes and events)");
        expectedEvents.add("start on Activity(mostOuterNestedActivity)");
        expectedEvents.add("start on Activity(outerScope)");
        expectedEvents.add("start on Activity(firstInnerScope)");
        expectedEvents.add("start on Activity(firstMostInnerNestedActivity)");
        expectedEvents.add("start on Activity(start)");
        expectedEvents.add("end on Activity(start)");
        expectedEvents.add("start on Activity(waitInFirst)");

        assertEquals("expected " + expectedEvents + ", but was \n" + eventCollector + "\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        PvmExecution execution = processInstance.findExecution("waitInFirst");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(waitInFirst)");
        expectedEvents.add("end on Activity(firstMostInnerNestedActivity)");
        expectedEvents.add("end on Activity(firstInnerScope)");
        expectedEvents.add("start on Activity(secondInnerScope)");
        expectedEvents.add("start on Activity(secondMostInnerNestedActivity)");
        expectedEvents.add("start on Activity(waitInSecond)");

        assertEquals("expected " + expectedEvents + ", but was \n" + eventCollector + "\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        execution = processInstance.findExecution("waitInSecond");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(waitInSecond)");
        expectedEvents.add("start on Activity(end)");
        expectedEvents.add("end on Activity(end)");
        expectedEvents.add("end on Activity(secondMostInnerNestedActivity)");
        expectedEvents.add("end on Activity(secondInnerScope)");
        expectedEvents.add("end on Activity(outerScope)");
        expectedEvents.add("end on Activity(mostOuterNestedActivity)");
        expectedEvents.add("end on ProcessDefinition(scopes and events)");

        assertEquals("expected " + expectedEvents + ", but was \n" + eventCollector + "\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();
    }

    /**
     * +--------------------------------------------------------------------------------+
     * | mostOuterNestedActivity                                                        |
     * | +----------------------------------------------------------------------------+ |
     * | | outerScope (scope)                                                         | |
     * | | +----------------------------------+ +-----------------------------------+ | |
     * | | | firstInnerScope (scope)          | | secondInnerScope (scope)          | | |
     * | | | +------------------------------+ | | +-------------------------------+ | | |
     * | | | | firstMostInnerNestedActivity | | | | secondMostInnerNestedActivity | | | |
     * | | | | +-------+   +-------------+  | | | | +--------------+    +-----+   | | | |
     * | | | | | start |-->| waitInFirst |--------->| waitInSecond |--> | end |   | | | |
     * | | | | +-------+   +-------------+  | | | | +--------------+    +-----+   | | | |
     * | | | +------------------------------+ | | +-------------------------------+ | | |
     * | | +----------------------------------+ +-----------------------------------+ | |
     * | +----------------------------------------------------------------------------+ |
     * +--------------------------------------------------------------------------------+
     */
    public void testStartEndWithScopesAndNestedActivitiesNotAtInitial() {
        EventCollector eventCollector = new EventCollector();

        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder("scopes and events")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("mostOuterNestedActivity")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("outerScope")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("firstInnerScope")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("firstMostInnerNestedActivity")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("waitInFirst")
                .endActivity()
                .createActivity("waitInFirst")
                .behavior(new WaitState())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("waitInSecond")
                .endActivity()
                .endActivity()
                .endActivity()
                .createActivity("secondInnerScope")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("secondMostInnerNestedActivity")
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("waitInSecond")
                .behavior(new WaitState())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .executionListener(PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener(PvmEventConstacts.EVENTNAME_END, eventCollector)
                .endActivity()
                .endActivity()
                .endActivity()
                .endActivity()
                .endActivity()
                .buildProcessDefinition();

        ActivityImpl alternativeInitial = (ActivityImpl) processDefinition.findActivity("waitInFirst");
        PvmProcessInstance processInstance = ((ProcessDefinitionImpl) processDefinition).createProcessInstanceForInitial(alternativeInitial);
        processInstance.start();

        List<String> expectedEvents = new ArrayList<String>();
        expectedEvents.add("start on ProcessDefinition(scopes and events)");
        expectedEvents.add("start on Activity(mostOuterNestedActivity)");
        expectedEvents.add("start on Activity(outerScope)");
        expectedEvents.add("start on Activity(firstInnerScope)");
        expectedEvents.add("start on Activity(firstMostInnerNestedActivity)");
        expectedEvents.add("start on Activity(waitInFirst)");

        assertEquals("expected " + expectedEvents + ", but was \n" + eventCollector + "\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        PvmExecution execution = processInstance.findExecution("waitInFirst");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(waitInFirst)");
        expectedEvents.add("end on Activity(firstMostInnerNestedActivity)");
        expectedEvents.add("end on Activity(firstInnerScope)");
        expectedEvents.add("start on Activity(secondInnerScope)");
        expectedEvents.add("start on Activity(secondMostInnerNestedActivity)");
        expectedEvents.add("start on Activity(waitInSecond)");

        assertEquals("expected " + expectedEvents + ", but was \n" + eventCollector + "\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        execution = processInstance.findExecution("waitInSecond");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(waitInSecond)");
        expectedEvents.add("start on Activity(end)");
        expectedEvents.add("end on Activity(end)");
        expectedEvents.add("end on Activity(secondMostInnerNestedActivity)");
        expectedEvents.add("end on Activity(secondInnerScope)");
        expectedEvents.add("end on Activity(outerScope)");
        expectedEvents.add("end on Activity(mostOuterNestedActivity)");
        expectedEvents.add("end on ProcessDefinition(scopes and events)");

        assertEquals("expected " + expectedEvents + ", but was \n" + eventCollector + "\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();
    }
}

package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmExecution;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.runtime.ExecutionImpl;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.EmbeddedSubProcess;
import com.baidu.iit.pvm.test.complex.behaviors.EventScopeCreatingSubprocess;
import com.baidu.iit.pvm.test.complex.behaviors.WaitState;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:45
 */
public class PvmEventScopesTest extends PvmTestCase {

    /**
     *
     *                              创建子流程 --+
     *                                          |
     *                                          v
     *
     *           +------------------------------+
     *           | embedded subprocess          |
     * +-----+   |  +-----------+   +---------+ |   +----+   +---+
     * |start|-->|  |startInside|-->|endInside| |-->|wait|-->|end|
     * +-----+   |  +-----------+   +---------+ |   +----+   +---+
     *           +------------------------------+
     *
     *                                                           ^
     *                                                           |
     *                                               销毁子流程 --+
     *
     */
    public void testActivityEndDestroysEventScopes() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()
                .createActivity("embeddedsubprocess")
                .scope()
                .behavior(new EventScopeCreatingSubprocess())
                .createActivity("startInside")
                .behavior(new Automatic())
                .transition("endInside")
                .endActivity()
                .createActivity("endInside")
                .behavior(new Automatic())
                .endActivity()
                .transition("wait")
                .endActivity()
                .createActivity("wait")
                .behavior(new WaitState())
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new Automatic())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        boolean eventScopeFound = false;
        List<ExecutionImpl> executions = ((ExecutionImpl)processInstance).getExecutions();
        for (ExecutionImpl executionImpl : executions) {
            if(executionImpl.isEventScope()) {
                eventScopeFound = true;
                break;
            }
        }

        assertTrue(eventScopeFound);

        processInstance.signal(null, null);

        assertTrue(processInstance.isEnded());

    }


    /**
     *           +----------------------------------------------------------------------+
     *           | 嵌套子流程                                                            |
     *           |                                                                      |
     *           |                                       创建子流程 --+                  |
     *           |                                                   |                  |
     *           |                                                   v                  |
     *           |                                                                      |
     *           |                  +--------------------------------+                  |
     *           |                  | nested embedded subprocess     |                  |
     * +-----+   | +-----------+    |  +-----------------+           |   +----+   +---+ |   +---+
     * |start|-->| |startInside|--> |  |startNestedInside|           |-->|wait|-->|end| |-->|end|
     * +-----+   | +-----------+    |  +-----------------+           |   +----+   +---+ |   +---+
     *           |                  +--------------------------------+                  |
     *           |                                                                      |
     *           +----------------------------------------------------------------------+
     *
     *                                                                                  ^
     *                                                                                  |
     *                                                                      销毁子流程 --+
     */
    public void testTransitionDestroysEventScope() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()
                .createActivity("embeddedsubprocess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startInside")
                .behavior(new Automatic())
                .transition("nestedSubProcess")
                .endActivity()
                .createActivity("nestedSubProcess")
                .scope()
                .behavior(new EventScopeCreatingSubprocess())
                .createActivity("startNestedInside")
                .behavior(new Automatic())
                .endActivity()
                .transition("wait")
                .endActivity()
                .createActivity("wait")
                .behavior(new WaitState())
                .transition("endInside")
                .endActivity()
                .createActivity("endInside")
                .behavior(new Automatic())
                .endActivity()
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new Automatic())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> expectedActiveActivityIds = new ArrayList<String>();
        expectedActiveActivityIds.add("wait");
        assertEquals(expectedActiveActivityIds, processInstance.findActiveActivityIds());


        PvmExecution execution = processInstance.findExecution("wait");
        execution.signal(null, null);

        assertTrue(processInstance.isEnded());

    }

}

package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.End;
import com.baidu.iit.pvm.test.complex.behaviors.ReusableSubProcess;

/**
 * 测试可重入的子流程
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:46
 */
public class PvmReusableSubProcessTest extends PvmTestCase {

    public void testReusableSubProcess() {
        PvmProcessDefinition subProcessDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("subEnd")
                .endActivity()
                .createActivity("subEnd")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessDefinition superProcessDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("subprocess")
                .endActivity()
                .createActivity("subprocess")
                .behavior(new ReusableSubProcess(subProcessDefinition))
                .transition("superEnd")
                .endActivity()
                .createActivity("superEnd")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = superProcessDefinition.createProcessInstance();
        processInstance.start();

        assertTrue(processInstance.isEnded());
    }
}

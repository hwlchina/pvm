package com.baidu.iit.pvm.test.complex.behaviors;

import com.baidu.iit.pvm.PvmActivity;
import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.CompositeActivityBehavior;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 子流程定义测试相关的behavior测试
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:27
 */
public class EmbeddedSubProcess implements CompositeActivityBehavior {

    public void execute(ActivityExecution execution) throws Exception {
        List<PvmActivity> startActivities = new ArrayList<PvmActivity>();
        for (PvmActivity activity: execution.getActivity().getActivities()) {
            if (activity.getIncomingTransitions().isEmpty()) {
                startActivities.add(activity);
            }
        }

        for (PvmActivity startActivity: startActivities) {
            execution.executeActivity(startActivity);
        }
    }

    @SuppressWarnings("unchecked")
    public void lastExecutionEnded(ActivityExecution execution) {
        List<PvmTransition> outgoingTransitions = execution.getActivity().getOutgoingTransitions();
        if(outgoingTransitions.isEmpty()) {
            execution.end();
        }else {
            execution.takeAll(outgoingTransitions, Collections.EMPTY_LIST);
        }
    }

    @SuppressWarnings("unchecked")
    public void timerFires(ActivityExecution execution, String signalName, Object signalData) throws Exception {
        PvmActivity timerActivity = execution.getActivity();
        boolean isInterrupting = (Boolean) timerActivity.getProperty("isInterrupting");
        List<ActivityExecution> recyclableExecutions = null;
        if (isInterrupting) {
            recyclableExecutions = removeAllExecutions(execution);
        } else {
            recyclableExecutions = Collections.EMPTY_LIST;
        }
        execution.takeAll(timerActivity.getOutgoingTransitions(), recyclableExecutions);
    }

    private List<ActivityExecution> removeAllExecutions(ActivityExecution execution) {
        return null;
    }

}

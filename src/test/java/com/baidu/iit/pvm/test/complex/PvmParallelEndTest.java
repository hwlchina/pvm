package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.End;
import com.baidu.iit.pvm.test.complex.behaviors.ParallelGateway;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:46
 */
public class PvmParallelEndTest extends PvmTestCase {

    /**
     * 没有回归点的并发测试
     *
     *                   +----+
     *              +--->|end1|
     *              |    +----+
     *              |
     * +-----+   +----+
     * |start|-->|fork|
     * +-----+   +----+
     *              |
     *              |    +----+
     *              +--->|end2|
     *                   +----+
     */
    public void testParallelEnd() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .transition("end1")
                .transition("end2")
                .endActivity()
                .createActivity("end1")
                .behavior(new End())
                .endActivity()
                .createActivity("end2")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertTrue(processInstance.isEnded());
    }
}

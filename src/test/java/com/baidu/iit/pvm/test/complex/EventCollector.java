package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.delegate.DelegateExecution;
import com.baidu.iit.pvm.delegate.ExecutionListener;
import com.baidu.iit.pvm.delegate.ExecutionListenerExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:24
 */
public class EventCollector implements ExecutionListener {

    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(EventCollector.class);

    public List<String> events = new ArrayList<String>();

    public void notify(DelegateExecution execution) throws Exception {
        notify((ExecutionListenerExecution)execution);
    }

    public void notify(ExecutionListenerExecution execution) {
        log.debug("事件集合: {} on {}", execution.getEventName(), execution.getEventSource());
        events.add(execution.getEventName()+" on "+execution.getEventSource());
    }

    public String toString() {
        StringBuilder text = new StringBuilder();
        for (String event: events) {
            text.append(event);
            text.append("\n");
        }
        return text.toString();
    }
}

package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.*;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.End;
import com.baidu.iit.pvm.test.complex.behaviors.ParallelGateway;
import com.baidu.iit.pvm.test.complex.behaviors.WaitState;

import java.util.ArrayList;
import java.util.List;

/**
 * fork/join 测试
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:47
 */
public class PvmScopesAndConcurrencyTest extends PvmTestCase {

    /**
     *         +---------+
     *         |scope    |  +--+
     *         |      +---->|c1|
     *         |      |  |  +--+
     *         |      |  |
     * +-----+ |  +----+ |  +--+
     * |start|--->|fork|--->|c2|
     * +-----+ |  +----+ |  +--+
     *         |      |  |
     *         |      |  |  +--+
     *         |      +---->|c3|
     *         |         |  +--+
     *         +---------+
     */
    public void testConcurrentPathsComingOutOfScope() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("fork")
                .endActivity()
                .createActivity("scope")
                .scope()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .transition("c1")
                .transition("c2")
                .transition("c3")
                .endActivity()
                .endActivity()
                .createActivity("c1")
                .behavior(new WaitState())
                .endActivity()
                .createActivity("c2")
                .behavior(new WaitState())
                .endActivity()
                .createActivity("c3")
                .behavior(new WaitState())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> activeActivityIds = processInstance.findActiveActivityIds();
        List<String> expectedActiveActivityIds = new ArrayList<String>();
        expectedActiveActivityIds.add("c3");
        expectedActiveActivityIds.add("c1");
        expectedActiveActivityIds.add("c2");

        assertEquals(expectedActiveActivityIds, activeActivityIds);
    }

    /**
     *                      +------------+
     *                      |scope       |
     *                  +----------+     |
     *                  |   |      v     |
     * +-----+   +--------+ |   +------+ |
     * |start|-->|parallel|---->|inside| |
     * +-----+   +--------+ |   +------+ |
     *                  |   |      ^     |
     *                  +----------+     |
     *                      |            |
     *                      +------------+
     */
    public void testConcurrentPathsGoingIntoScope() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("parallel")
                .endActivity()
                .createActivity("parallel")
                .behavior(new ParallelGateway())
                .transition("inside")
                .transition("inside")
                .transition("inside")
                .endActivity()
                .createActivity("scope")
                .scope()
                .createActivity("inside")
                .behavior(new WaitState())
                .endActivity()
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> activeActivityIds = processInstance.findActiveActivityIds();
        List<String> expectedActiveActivityIds = new ArrayList<String>();
        expectedActiveActivityIds.add("inside");
        expectedActiveActivityIds.add("inside");
        expectedActiveActivityIds.add("inside");

        assertEquals(expectedActiveActivityIds, activeActivityIds);
    }

    /**
     *                      +---------+
     *                      | +----+  |
     *                  +---->| c1 |------+
     *                  |   | +----+  |   v
     * +-------+   +------+ |         | +------+   +-----+
     * | start |-->| fork | | noscope | | join |-->| end |
     * +-------+   +------+ |         | +------+   +-----+
     *                  |   | +----+  |   ^
     *                  +---->| c2 |------+
     *                      | +----+  |
     *                      +---------+
     */
    public void testConcurrentPathsThroughNonScopeNestedActivity() {
        EventCollector eventCollector = new EventCollector();

        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder("scopes and concurrency")
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("c1")
                .transition("c2")
                .endActivity()
                .createActivity("noscope")
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("c1")
                .behavior(new WaitState())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("join")
                .endActivity()
                .createActivity("c2")
                .behavior(new WaitState())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("join")
                .endActivity()
                .endActivity()
                .createActivity("join")
                .behavior(new ParallelGateway())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> expectedEvents = new ArrayList<String>();
        expectedEvents.add("start on ProcessDefinition(scopes and concurrency)");
        expectedEvents.add("start on Activity(start)");
        expectedEvents.add("end on Activity(start)");
        expectedEvents.add("start on Activity(fork)");
        expectedEvents.add("end on Activity(fork)");
        expectedEvents.add("start on Activity(noscope)");
        expectedEvents.add("start on Activity(c1)");
        expectedEvents.add("end on Activity(fork)");
        expectedEvents.add("start on Activity(noscope)");
        expectedEvents.add("start on Activity(c2)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        PvmExecution execution = processInstance.findExecution("c1");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(c1)");
        expectedEvents.add("end on Activity(noscope)");
        expectedEvents.add("start on Activity(join)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        execution = processInstance.findExecution("c2");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(c2)");
        expectedEvents.add("end on Activity(noscope)");
        expectedEvents.add("start on Activity(join)");
        expectedEvents.add("end on Activity(join)");
        expectedEvents.add("start on Activity(end)");
        expectedEvents.add("end on Activity(end)");
        expectedEvents.add("end on ProcessDefinition(scopes and concurrency)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        assertTrue(processInstance.isEnded());
    }

    /**
     *                      +---------+
     *                      | +----+  |
     *                  +---->| c1 |------+
     *                  |   | +----+  |   v
     * +-------+   +------+ |         | +------+   +-----+
     * | start |-->| fork | |  scope  | | join |-->| end |
     * +-------+   +------+ |         | +------+   +-----+
     *                  |   | +----+  |   ^
     *                  +---->| c2 |------+
     *                      | +----+  |
     *                      +---------+
     */
    public void testConcurrentPathsThroughScope() {
        EventCollector eventCollector = new EventCollector();

        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder("scopes and concurrency")
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("c1")
                .transition("c2")
                .endActivity()
                .createActivity("scope")
                .scope()
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("c1")
                .behavior(new WaitState())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("join")
                .endActivity()
                .createActivity("c2")
                .behavior(new WaitState())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("join")
                .endActivity()
                .endActivity()
                .createActivity("join")
                .behavior(new ParallelGateway())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> expectedEvents = new ArrayList<String>();
        expectedEvents.add("start on ProcessDefinition(scopes and concurrency)");
        expectedEvents.add("start on Activity(start)");
        expectedEvents.add("end on Activity(start)");
        expectedEvents.add("start on Activity(fork)");
        expectedEvents.add("end on Activity(fork)");
        expectedEvents.add("start on Activity(scope)");
        expectedEvents.add("start on Activity(c1)");
        expectedEvents.add("end on Activity(fork)");
        expectedEvents.add("start on Activity(scope)");
        expectedEvents.add("start on Activity(c2)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        PvmExecution execution = processInstance.findExecution("c1");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(c1)");
        expectedEvents.add("end on Activity(scope)");
        expectedEvents.add("start on Activity(join)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        execution = processInstance.findExecution("c2");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(c2)");
        expectedEvents.add("end on Activity(scope)");
        expectedEvents.add("start on Activity(join)");
        expectedEvents.add("end on Activity(join)");
        expectedEvents.add("start on Activity(end)");
        expectedEvents.add("end on Activity(end)");
        expectedEvents.add("end on ProcessDefinition(scopes and concurrency)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        assertTrue(processInstance.isEnded());
    }

    /**
     *           +--------------------+
     *           |            +----+  |
     *           |      +---->| c1 |------+
     *           |      |     +----+  |   v
     * +-------+ | +------+           | +------+   +-----+
     * | start |-->| fork |    scope  | | join |-->| end |
     * +-------+ | +------+           | +------+   +-----+
     *           |      |     +----+  |   ^
     *           |      +---->| c2 |------+
     *           |            +----+  |
     *           +--------------------+
     */
    public void testConcurrentPathsGoingOutOfScope() {
        EventCollector eventCollector = new EventCollector();

        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder("scopes and concurrency")
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("fork")
                .endActivity()
                .createActivity("scope")
                .scope()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("c1")
                .transition("c2")
                .endActivity()
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("c1")
                .behavior(new WaitState())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("join")
                .endActivity()
                .createActivity("c2")
                .behavior(new WaitState())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("join")
                .endActivity()
                .endActivity()
                .createActivity("join")
                .behavior(new ParallelGateway())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> expectedEvents = new ArrayList<String>();
        expectedEvents.add("start on ProcessDefinition(scopes and concurrency)");
        expectedEvents.add("start on Activity(start)");
        expectedEvents.add("end on Activity(start)");
        expectedEvents.add("start on Activity(scope)");
        expectedEvents.add("start on Activity(fork)");
        expectedEvents.add("end on Activity(fork)");
        expectedEvents.add("start on Activity(c1)");
        expectedEvents.add("end on Activity(fork)");
        expectedEvents.add("start on Activity(c2)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        PvmExecution execution = processInstance.findExecution("c1");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(c1)");
        expectedEvents.add("end on Activity(scope)");
        expectedEvents.add("start on Activity(join)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        execution = processInstance.findExecution("c2");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(c2)");
        expectedEvents.add("end on Activity(scope)");
        expectedEvents.add("start on Activity(join)");
        expectedEvents.add("end on Activity(join)");
        expectedEvents.add("start on Activity(end)");
        expectedEvents.add("end on Activity(end)");
        expectedEvents.add("end on ProcessDefinition(scopes and concurrency)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        assertTrue(processInstance.isEnded());
    }

    /**
     *
     *                      +--------------------+
     *                      | +----+             |
     *                  +---->| c1 |------+      |
     *                  |   | +----+      v      |
     * +-------+   +------+ |           +------+ | +-----+
     * | start |-->| fork | |  scope    | join |-->| end |
     * +-------+   +------+ |           +------+ | +-----+
     *                  |   | +----+      ^      |
     *                  +---->| c2 |------+      |
     *                      | +----+             |
     *                      +--------------------+
     */
    public void testConcurrentPathsJoiningInsideScope() {
        EventCollector eventCollector = new EventCollector();

        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder("scopes and concurrency")
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("c1")
                .transition("c2")
                .endActivity()
                .createActivity("scope")
                .scope()
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .createActivity("c1")
                .behavior(new WaitState())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("join")
                .endActivity()
                .createActivity("c2")
                .behavior(new WaitState())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("join")
                .endActivity()
                .createActivity("join")
                .behavior(new ParallelGateway())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .transition("end")
                .endActivity()
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .executionListener( PvmEventConstacts.EVENTNAME_START, eventCollector)
                .executionListener( PvmEventConstacts.EVENTNAME_END, eventCollector)
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> expectedEvents = new ArrayList<String>();
        expectedEvents.add("start on ProcessDefinition(scopes and concurrency)");
        expectedEvents.add("start on Activity(start)");
        expectedEvents.add("end on Activity(start)");
        expectedEvents.add("start on Activity(fork)");
        expectedEvents.add("end on Activity(fork)");
        expectedEvents.add("start on Activity(scope)");
        expectedEvents.add("start on Activity(c1)");
        expectedEvents.add("end on Activity(fork)");
        expectedEvents.add("start on Activity(scope)");
        expectedEvents.add("start on Activity(c2)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        PvmExecution execution = processInstance.findExecution("c1");
        execution.signal(null, null);

        expectedEvents = new ArrayList<String>();
        expectedEvents.add("end on Activity(c1)");
        expectedEvents.add("start on Activity(join)");

        assertEquals("expected "+expectedEvents+", but was \n"+eventCollector+"\n", expectedEvents, eventCollector.events);
        eventCollector.events.clear();

        execution = processInstance.findExecution("c2");
        execution.signal(null, null);
    }
}

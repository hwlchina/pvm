package com.baidu.iit.pvm.test.complex.behaviors;

import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityBehavior;
import com.baidu.iit.pvm.delegate.ActivityExecution;

/**
 * 模拟循环动作
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:30
 */
public class While implements ActivityBehavior {

    String variableName;
    int from;
    int to;

    public While(String variableName, int from, int to) {
        this.variableName = variableName;
        this.from = from;
        this.to = to;
    }

    public void execute(ActivityExecution execution) throws Exception {
        PvmTransition more = execution.getActivity().findOutgoingTransition("more");
        PvmTransition done = execution.getActivity().findOutgoingTransition("done");

        Integer value = (Integer) execution.getVariable(variableName);

        if (value==null) {
            execution.setVariable(variableName, from);
            execution.take(more);

        } else {
            value = value+1;

            if (value<to) {
                execution.setVariable(variableName, value);
                execution.take(more);

            } else {
                execution.take(done);
            }
        }
    }

}

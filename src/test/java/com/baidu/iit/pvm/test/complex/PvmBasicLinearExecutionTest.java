package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmExecution;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.test.WaitState;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.End;
import com.baidu.iit.pvm.test.complex.behaviors.While;

import java.util.ArrayList;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:25
 */
public class PvmBasicLinearExecutionTest extends PvmTestCase {

    /**
     * 两个节点的自动流转流程
     * +-------+   +-----+
     * | start |-->| end |
     * +-------+   +-----+
     */
    public void testStartEnd() {
        //定义流程只用开始和结束时间点
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertEquals(new ArrayList<String>(), processInstance.findActiveActivityIds());
        assertTrue(processInstance.isEnded());
    }

    /**
     * 多个点的自动执行跳转
     * +-----+   +-----+   +-------+
     * | one |-->| two |-->| three |
     * +-----+   +-----+   +-------+
     */
    public void testSingleAutomatic() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("one")
                .initial()
                .behavior(new Automatic())
                .transition("two")
                .endActivity()
                .createActivity("two")
                .behavior(new Automatic())
                .transition("three")
                .endActivity()
                .createActivity("three")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertEquals(new ArrayList<String>(), processInstance.findActiveActivityIds());
        assertTrue(processInstance.isEnded());
    }

    /**
     * 需要程序参与的一个断点的情况
     * +-----+   +-----+   +-------+
     * | one |-->| two |-->| three |
     * +-----+   +-----+   +-------+
     */
    public void testSingleWaitState() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("one")
                .initial()
                .behavior(new Automatic())
                .transition("two")
                .endActivity()
                .createActivity("two")
                .behavior(new WaitState())
                .transition("three")
                .endActivity()
                .createActivity("three")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        PvmExecution activityInstance = processInstance.findExecution("two");
        assertNotNull(activityInstance);

        activityInstance.signal(null, null);

        assertEquals(new ArrayList<String>(), processInstance.findActiveActivityIds());
        assertTrue(processInstance.isEnded());
    }

    /**
     * 需要程序参与的多个断点的测试
     * +-----+   +-----+   +-------+   +------+    +------+
     * | one |-->| two |-->| three |-->| four |--> | five |
     * +-----+   +-----+   +-------+   +------+    +------+
     */
    public void testCombinationOfWaitStatesAndAutomatics() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("one")
                .endActivity()
                .createActivity("one")
                .behavior(new WaitState())
                .transition("two")
                .endActivity()
                .createActivity("two")
                .behavior(new WaitState())
                .transition("three")
                .endActivity()
                .createActivity("three")
                .behavior(new Automatic())
                .transition("four")
                .endActivity()
                .createActivity("four")
                .behavior(new Automatic())
                .transition("five")
                .endActivity()
                .createActivity("five")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        PvmExecution activityInstance = processInstance.findExecution("one");
        assertNotNull(activityInstance);
        activityInstance.signal(null, null);

        activityInstance = processInstance.findExecution("two");
        assertNotNull(activityInstance);
        activityInstance.signal(null, null);

        assertEquals(new ArrayList<String>(), processInstance.findActiveActivityIds());
        assertTrue(processInstance.isEnded());
    }

    /**
     * 模拟循环的流转状况
     *
     *
     *                  +----------------------------+
     *                  v                            |
     * +-------+   +------+   +-----+   +-----+    +-------+
     * | start |-->| loop |-->| one |-->| two |--> | three |
     * +-------+   +------+   +-----+   +-----+    +-------+
     *                  |
     *                  |   +-----+
     *                  +-->| end |
     *                      +-----+
     */
    public void testWhileLoop() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("loop")
                .endActivity()

                .createActivity("loop")
                .behavior(new While("count", 0, 500))
                .transition("one", "more")
                .transition("end", "done")
                .endActivity()

                .createActivity("one")
                .behavior(new Automatic())
                .transition("two")
                .endActivity()

                .createActivity("two")
                .behavior(new Automatic())
                .transition("three")
                .endActivity()
                .createActivity("three")
                .behavior(new Automatic())
                .transition("loop")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertEquals(new ArrayList<String>(), processInstance.findActiveActivityIds());
        assertTrue(processInstance.isEnded());
    }

}

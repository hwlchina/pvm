package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmExecution;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.End;
import com.baidu.iit.pvm.test.complex.behaviors.WaitState;

import java.util.ArrayList;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:47
 */
public class PvmScopeWaitStateTest extends PvmTestCase {

    /**
     * +-----+   +----------+   +---+
     * |start|-->|scopedWait|-->|end|
     * +-----+   +----------+   +---+
     */
    public void testWaitStateScope() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("scopedWait")
                .endActivity()
                .createActivity("scopedWait")
                .scope()
                .behavior(new WaitState())
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        PvmExecution execution = processInstance.findExecution("scopedWait");
        assertNotNull(execution);

        execution.signal(null, null);

        assertEquals(new ArrayList<String>(), processInstance.findActiveActivityIds());
        assertTrue(processInstance.isEnded());
    }

    /**
     *          +--------------+
     *          | outerScope   |
     * +-----+  | +----------+ |  +---+
     * |start|--->|scopedWait|--->|end|
     * +-----+  | +----------+ |  +---+
     *          +--------------+
     */
    public void testNestedScope() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("scopedWait")
                .endActivity()
                .createActivity("outerScope")
                .scope()
                .createActivity("scopedWait")
                .scope()
                .behavior(new WaitState())
                .transition("end")
                .endActivity()
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        PvmExecution activityInstance = processInstance.findExecution("scopedWait");
        assertNotNull(activityInstance);

        activityInstance.signal(null, null);

        assertEquals(new ArrayList<String>(), processInstance.findActiveActivityIds());
        assertTrue(processInstance.isEnded());
    }
}

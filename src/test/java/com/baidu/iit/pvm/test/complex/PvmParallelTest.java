package com.baidu.iit.pvm.test.complex;

import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmExecution;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.test.complex.behaviors.Automatic;
import com.baidu.iit.pvm.test.complex.behaviors.End;
import com.baidu.iit.pvm.test.complex.behaviors.ParallelGateway;
import com.baidu.iit.pvm.test.complex.behaviors.WaitState;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 测试fork/join相关的流程
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:46
 */
public class PvmParallelTest extends PvmTestCase {

    public void testSimpleAutmaticConcurrency() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .transition("c1")
                .transition("c2")
                .endActivity()
                .createActivity("c1")
                .behavior(new Automatic())
                .transition("join")
                .endActivity()
                .createActivity("c2")
                .behavior(new Automatic())
                .transition("join")
                .endActivity()
                .createActivity("join")
                .behavior(new ParallelGateway())
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertTrue(processInstance.isEnded());
    }

    public void testSimpleWaitStateConcurrency() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .transition("c1")
                .transition("c2")
                .endActivity()
                .createActivity("c1")
                .behavior(new WaitState())
                .transition("join")
                .endActivity()
                .createActivity("c2")
                .behavior(new WaitState())
                .transition("join")
                .endActivity()
                .createActivity("join")
                .behavior(new ParallelGateway())
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new WaitState())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        PvmExecution activityInstanceC1 = processInstance.findExecution("c1");
        assertNotNull(activityInstanceC1);

        PvmExecution activityInstanceC2 = processInstance.findExecution("c2");
        assertNotNull(activityInstanceC2);

        activityInstanceC1.signal(null, null);
        activityInstanceC2.signal(null, null);

        List<String> activityNames = processInstance.findActiveActivityIds();
        List<String> expectedActivityNames = new ArrayList<String>();
        expectedActivityNames.add("end");

        assertEquals(expectedActivityNames, activityNames);
    }

    public void testUnstructuredConcurrencyTwoJoins() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .transition("c1")
                .transition("c2")
                .transition("c3")
                .endActivity()
                .createActivity("c1")
                .behavior(new Automatic())
                .transition("join1")
                .endActivity()
                .createActivity("c2")
                .behavior(new Automatic())
                .transition("join1")
                .endActivity()
                .createActivity("c3")
                .behavior(new Automatic())
                .transition("join2")
                .endActivity()
                .createActivity("join1")
                .behavior(new ParallelGateway())
                .transition("c4")
                .endActivity()
                .createActivity("c4")
                .behavior(new Automatic())
                .transition("join2")
                .endActivity()
                .createActivity("join2")
                .behavior(new ParallelGateway())
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new WaitState())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertNotNull(processInstance.findExecution("end"));
    }

    public void testUnstructuredConcurrencyTwoForks() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("fork1")
                .endActivity()
                .createActivity("fork1")
                .behavior(new ParallelGateway())
                .transition("c1")
                .transition("c2")
                .transition("fork2")
                .endActivity()
                .createActivity("c1")
                .behavior(new Automatic())
                .transition("join")
                .endActivity()
                .createActivity("c2")
                .behavior(new Automatic())
                .transition("join")
                .endActivity()
                .createActivity("fork2")
                .behavior(new ParallelGateway())
                .transition("c3")
                .transition("c4")
                .endActivity()
                .createActivity("c3")
                .behavior(new Automatic())
                .transition("join")
                .endActivity()
                .createActivity("c4")
                .behavior(new Automatic())
                .transition("join")
                .endActivity()
                .createActivity("join")
                .behavior(new ParallelGateway())
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new WaitState())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertNotNull(processInstance.findExecution("end"));
    }

    public void testJoinForkCombinedInOneParallelGateway() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .transition("c1")
                .transition("c2")
                .transition("c3")
                .endActivity()
                .createActivity("c1")
                .behavior(new Automatic())
                .transition("join1")
                .endActivity()
                .createActivity("c2")
                .behavior(new Automatic())
                .transition("join1")
                .endActivity()
                .createActivity("c3")
                .behavior(new Automatic())
                .transition("join2")
                .endActivity()
                .createActivity("join1")
                .behavior(new ParallelGateway())
                .transition("c4")
                .transition("c5")
                .transition("c6")
                .endActivity()
                .createActivity("c4")
                .behavior(new Automatic())
                .transition("join2")
                .endActivity()
                .createActivity("c5")
                .behavior(new Automatic())
                .transition("join2")
                .endActivity()
                .createActivity("c6")
                .behavior(new Automatic())
                .transition("join2")
                .endActivity()
                .createActivity("join2")
                .behavior(new ParallelGateway())
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new WaitState())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertNotNull(processInstance.findExecution("end"));
    }
}


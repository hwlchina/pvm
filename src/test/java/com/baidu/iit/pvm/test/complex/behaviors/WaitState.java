package com.baidu.iit.pvm.test.complex.behaviors;

import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.SignallableActivityBehavior;

/**
 * 由人工触发的下一步跳转
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:29
 */
public class WaitState implements SignallableActivityBehavior {

    public void execute(ActivityExecution execution) throws Exception {
    }

    public void signal(ActivityExecution execution, String signalName, Object signalData) throws Exception {
        PvmTransition transition = execution.getActivity().getOutgoingTransitions().get(0);
        execution.take(transition);
    }
}

package com.baidu.iit.pvm.test.complex.behaviors;

import com.baidu.iit.pvm.delegate.ActivityBehavior;
import com.baidu.iit.pvm.delegate.ActivityExecution;

/**
 * 流程结束点定义
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:28
 */
public class End implements ActivityBehavior {

    public void execute(ActivityExecution execution) throws Exception {
        execution.end();
    }

}
package com.baidu.iit.pvm.test.complex;

import junit.framework.AssertionFailedError;
import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * pvm测试基础类
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:48
 */
public abstract class PvmTestCase extends TestCase {

    protected static final String EMPTY_LINE = "                                                                                           ";

    protected static Logger log = LoggerFactory.getLogger(PvmTestCase.class);

    protected boolean isEmptyLinesEnabled = true;

    public void assertTextPresent(String expected, String actual) {
        if ( (actual==null)
                || (actual.indexOf(expected)==-1)
                ) {
            throw new AssertionFailedError("expected presence of ["+expected+"], but was ["+actual+"]");
        }
    }


    public void assertTextPresentIgnoreCase(String expected, String actual) {
        assertTextPresent(expected.toLowerCase(), actual.toLowerCase());
    }

    @Override
    protected void runTest() throws Throwable {
        if (log.isDebugEnabled()) {
            if (isEmptyLinesEnabled) {
                log.debug(EMPTY_LINE);
            }
            log.debug("#### START {}.{} ###########################################################", this.getClass().getSimpleName(), getName());
        }

        try {

            super.runTest();

        } catch (AssertionFailedError e) {
            log.error(EMPTY_LINE);
            log.error("ASSERTION FAILED: {}", e, e);
            throw e;

        } catch (Throwable e) {
            log.error(EMPTY_LINE);
            log.error("EXCEPTION: {}", e, e);
            throw e;

        } finally {
            log.debug("#### END {}.{} #############################################################", this.getClass().getSimpleName(), getName());
        }
    }

}

package com.baidu.iit.pvm.test.complex.behaviors;

import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityBehavior;
import com.baidu.iit.pvm.delegate.ActivityExecution;

import java.util.List;

/**
 * 模拟自动流转的动作
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:26
 */
public class Automatic implements ActivityBehavior {
    public void execute(ActivityExecution execution) throws Exception {


        //跳转到下一个节点
        List<PvmTransition> outgoingTransitions = execution.getActivity().getOutgoingTransitions();
        if(outgoingTransitions.isEmpty()) {
            execution.end();
        } else {
            execution.take(outgoingTransitions.get(0));
        }
    }

}

package com.baidu.iit.pvm.test.complex.behaviors;

import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.DelegateExecution;
import com.baidu.iit.pvm.delegate.SubProcessActivityBehavior;

import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:29
 */
public class ReusableSubProcess implements SubProcessActivityBehavior {

    PvmProcessDefinition processDefinition;

    public ReusableSubProcess(PvmProcessDefinition processDefinition) {
        this.processDefinition = processDefinition;
    }

    public void execute(ActivityExecution execution) throws Exception {
        PvmProcessInstance subProcessInstance = execution.createSubProcessInstance(processDefinition);
        //TODO 参数设置传递


        subProcessInstance.start();
    }

    public void completing(DelegateExecution execution, DelegateExecution subProcessInstance) throws Exception {
        //TODO 结束动作

    }

    public void completed(ActivityExecution execution) throws Exception {
        List<PvmTransition> outgoingTransitions = execution.getActivity().getOutgoingTransitions();
        execution.takeAll(outgoingTransitions, null);
    }
}

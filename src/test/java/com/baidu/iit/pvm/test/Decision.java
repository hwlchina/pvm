package com.baidu.iit.pvm.test;

import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityBehavior;
import com.baidu.iit.pvm.delegate.ActivityExecution;

/**
 * Created by 卫立 on 2014/4/7.
 */
public class Decision implements ActivityBehavior {

    public void execute(ActivityExecution execution) throws Exception {
        PvmTransition transition = null;
        String creditRating = (String) execution.getVariable("creditRating");
        if (creditRating.equals("AAA+")) {
            transition = execution.getActivity().findOutgoingTransition("wow");
        } else if (creditRating.equals("Aaa-")) {
            transition = execution.getActivity().findOutgoingTransition("nice");
        } else {
            transition = execution.getActivity().findOutgoingTransition("default");
        }

        execution.take(transition);
    }
}

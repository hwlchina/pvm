package com.baidu.iit.pvm.test;

import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmExecution;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import junit.framework.TestCase;

/**
 * Created by 卫立 on 2014/4/7.
 */
public class PvmTest extends TestCase {

    public void testPvmWaitState() {


        //定义执行流程以及各个节点的执行体
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("a")
                .initial()
                .behavior(new WaitState())
                .transition("b")
                .endActivity()
                .createActivity("b")
                .behavior(new WaitState())
                .transition("c").transition("d")
                .endActivity()
                .createActivity("c")
                .behavior(new WaitState())
                .endActivity()
                .createActivity("d").behavior(new WaitState()).endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();

        //定义上下文变量，通过上下问变量在各个Activity中传递数据
        processInstance.setVariable("spring","spring");

        processInstance.start();

        PvmExecution activityInstance = processInstance.findExecution("a");
        assertNotNull(activityInstance);

        activityInstance.signal(null, null);

        activityInstance = processInstance.findExecution("b");
        assertNotNull(activityInstance);

        activityInstance.signal(null, null);

        activityInstance = processInstance.findExecution("c");
        assertNotNull(activityInstance);
    }

    public void testPvmAutomatic() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("a")
                .initial()
                .behavior(new Automatic())
                .transition("b")
                .endActivity()
                .createActivity("b")
                .behavior(new Automatic())
                .transition("c")
                .endActivity()
                .createActivity("c")
                .behavior(new WaitState())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertNotNull(processInstance.findExecution("c"));
    }


    /**
     * 多条件跳转流程测试
     */
    public void testPvmDecision() {

        //多条件跳转流程
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("checkCredit")
                .endActivity()
                .createActivity("checkCredit")
                .behavior(new Decision())
                .transition("askDaughterOut", "wow")
                .transition("takeToGolf", "nice")
                .transition("ignore", "default")
                .endActivity()
                .createActivity("takeToGolf")
                .behavior(new WaitState())
                .endActivity()
                .createActivity("askDaughterOut")
                .behavior(new WaitState())
                .endActivity()
                .createActivity("ignore")
                .behavior(new WaitState())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.setVariable("creditRating", "Aaa-");
        processInstance.start();
        assertNotNull(processInstance.findExecution("takeToGolf"));

        processInstance = processDefinition.createProcessInstance();
        processInstance.setVariable("creditRating", "AAA+");
        processInstance.start();
        assertNotNull(processInstance.findExecution("askDaughterOut"));

        processInstance = processDefinition.createProcessInstance();
        processInstance.setVariable("creditRating", "bb-");
        processInstance.start();
        assertNotNull(processInstance.findExecution("ignore"));

    }
}

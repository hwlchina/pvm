package com.baidu.iit.pvm.test;

import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityBehavior;
import com.baidu.iit.pvm.delegate.ActivityExecution;

/**
 * Created by 卫立 on 2014/4/7.
 */
public class Automatic implements ActivityBehavior {

    public void execute(ActivityExecution activityContext) throws Exception {

       PvmTransition defaultOutgoingTransition = activityContext.getActivity().getOutgoingTransitions().get(0);

       activityContext.take(defaultOutgoingTransition);
    }
}

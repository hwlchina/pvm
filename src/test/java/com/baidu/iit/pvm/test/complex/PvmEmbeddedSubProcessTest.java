package com.baidu.iit.pvm.test.complex;

import com.alibaba.fastjson.JSON;
import com.baidu.iit.pvm.ProcessDefinitionBuilder;
import com.baidu.iit.pvm.PvmExecution;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.test.complex.behaviors.*;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-15
 * Time: 下午4:25
 */
public class PvmEmbeddedSubProcessTest extends PvmTestCase {

    /**
     * 线性的子流程测试
     *           +------------------------------+
     *           | embedded subprocess          |
     * +-----+   |  +-----------+   +---------+ |   +---+
     * |start|-->|  |startInside|-->|endInside| |-->|end|
     * +-----+   |  +-----------+   +---------+ |   +---+
     *           +------------------------------+
     */
    public void testEmbeddedSubProcess() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()

                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()

                .createActivity("embeddedsubprocess")
                .scope()

                .behavior(new EmbeddedSubProcess())
                    .createActivity("startInside")
                    .behavior(new Automatic())
                    .transition("endInside")
                    .endActivity()

                    .createActivity("endInside")
                    .behavior(new End())
                    .endActivity()

                .transition("end")
                .endActivity()

                .createActivity("end")
                .behavior(new WaitState())
                .endActivity()

                .buildProcessDefinition();

       System.out.println(JSON.toJSONString(processDefinition));

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> expectedActiveActivityIds = new ArrayList<String>();
        expectedActiveActivityIds.add("end");

        assertEquals(expectedActiveActivityIds, processInstance.findActiveActivityIds());
    }

    /**
     * 并发子流程测试
     *           +----------------------------------------+
     *           | embeddedsubprocess        +----------+ |
     *           |                     +---->|endInside1| |
     *           |                     |     +----------+ |
     *           |                     |                  |
     * +-----+   |  +-----------+   +----+   +----------+ |   +---+
     * |start|-->|  |startInside|-->|fork|-->|endInside2| |-->|end|
     * +-----+   |  +-----------+   +----+   +----------+ |   +---+
     *           |                     |                  |
     *           |                     |     +----------+ |
     *           |                     +---->|endInside3| |
     *           |                           +----------+ |
     *           +----------------------------------------+
     */
    public void testMultipleConcurrentEndsInsideEmbeddedSubProcess() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()
                .createActivity("embeddedsubprocess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startInside")
                .behavior(new Automatic())
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .transition("endInside1")
                .transition("endInside2")
                .transition("endInside3")
                .endActivity()
                .createActivity("endInside1")
                .behavior(new End())
                .endActivity()
                .createActivity("endInside2")
                .behavior(new End())
                .endActivity()
                .createActivity("endInside3")
                .behavior(new End())
                .endActivity()
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertTrue(processInstance.isEnded());
    }

    /**
     *           +-------------------------------------------------+
     *           | embeddedsubprocess        +----------+          |
     *           |                     +---->|endInside1|          |
     *           |                     |     +----------+          |
     *           |                     |                           |
     * +-----+   |  +-----------+   +----+   +----+   +----------+ |   +---+
     * |start|-->|  |startInside|-->|fork|-->|wait|-->|endInside2| |-->|end|
     * +-----+   |  +-----------+   +----+   +----+   +----------+ |   +---+
     *           |                     |                           |
     *           |                     |     +----------+          |
     *           |                     +---->|endInside3|          |
     *           |                           +----------+          |
     *           +-------------------------------------------------+
     */
    public void testMultipleConcurrentEndsInsideEmbeddedSubProcessWithWaitState() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()
                .createActivity("embeddedsubprocess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startInside")
                .behavior(new Automatic())
                .transition("fork")
                .endActivity()
                .createActivity("fork")
                .behavior(new ParallelGateway())
                .transition("endInside1")
                .transition("wait")
                .transition("endInside3")
                .endActivity()
                .createActivity("endInside1")
                .behavior(new End())
                .endActivity()
                .createActivity("wait")
                .behavior(new WaitState())
                .transition("endInside2")
                .endActivity()
                .createActivity("endInside2")
                .behavior(new End())
                .endActivity()
                .createActivity("endInside3")
                .behavior(new End())
                .endActivity()
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new End())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertFalse(processInstance.isEnded());
        PvmExecution execution = processInstance.findExecution("wait");
        execution.signal(null, null);

        assertTrue(processInstance.isEnded());
    }

    /**
     * 多级子流程测试
     *           +-------------------------------------------------------+
     *           | embedded subprocess                                   |
     *           |                  +--------------------------------+   |
     *           |                  | nested embedded subprocess     |   |
     * +-----+   | +-----------+    |  +-----------+   +---------+   |   |   +---+
     * |start|-->| |startInside|--> |  |startInside|-->|endInside|   |   |-->|end|
     * +-----+   | +-----------+    |  +-----------+   +---------+   |   |   +---+
     *           |                  +--------------------------------+   |
     *           |                                                       |
     *           +-------------------------------------------------------+
     */
    public void testNestedSubProcessNoEnd() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()
                .createActivity("embeddedsubprocess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startInside")
                .behavior(new Automatic())
                .transition("nestedSubProcess")
                .endActivity()
                .createActivity("nestedSubProcess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startNestedInside")
                .behavior(new Automatic())
                .transition("endInside")
                .endActivity()
                .createActivity("endInside")
                .behavior(new End())
                .endActivity()
                .endActivity()
                .transition("end")
                .endActivity()
                .createActivity("end")
                .behavior(new WaitState())
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        List<String> expectedActiveActivityIds = new ArrayList<String>();
        expectedActiveActivityIds.add("end");

        assertEquals(expectedActiveActivityIds, processInstance.findActiveActivityIds());
    }

    /**
     * 作为结束点的子流程测试
     *           +------------------------------+
     *           | embedded subprocess          |
     * +-----+   |  +-----------+               |
     * |start|-->|  |startInside|               |
     * +-----+   |  +-----------+               |
     *           +------------------------------+
     */
    public void testEmbeddedSubProcessWithoutEndEvents() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()
                .createActivity("embeddedsubprocess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startInside")
                .behavior(new Automatic())
                .endActivity()
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertTrue(processInstance.isEnded());
    }

    /**
     * 多级子流程作为结束点的测试
     *           +-------------------------------------------------------+
     *           | embedded subprocess                                   |
     *           |                  +--------------------------------+   |
     *           |                  | nested embedded subprocess     |   |
     * +-----+   | +-----------+    |  +-----------+                 |   |
     * |start|-->| |startInside|--> |  |startInside|                 |   |
     * +-----+   | +-----------+    |  +-----------+                 |   |
     *           |                  +--------------------------------+   |
     *           |                                                       |
     *           +-------------------------------------------------------+
     */
    public void testNestedSubProcessBothNoEnd() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()
                .createActivity("embeddedsubprocess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startInside")
                .behavior(new Automatic())
                .transition("nestedSubProcess")
                .endActivity()
                .createActivity("nestedSubProcess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startNestedInside")
                .behavior(new Automatic())
                .endActivity()
                .endActivity()
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertTrue(processInstance.isEnded());
    }


    /**
     * 子流程测试
     *           +------------------------------+
     *           | embedded subprocess          |
     * +-----+   |  +-----------+   +---------+ |
     * |start|-->|  |startInside|-->|endInside| |
     * +-----+   |  +-----------+   +---------+ |
     *           +------------------------------+
     */
    public void testEmbeddedSubProcessNoEnd() {
        PvmProcessDefinition processDefinition = new ProcessDefinitionBuilder()
                .createActivity("start")
                .initial()
                .behavior(new Automatic())
                .transition("embeddedsubprocess")
                .endActivity()
                .createActivity("embeddedsubprocess")
                .scope()
                .behavior(new EmbeddedSubProcess())
                .createActivity("startInside")
                .behavior(new Automatic())
                .transition("endInside")
                .endActivity()
                .createActivity("endInside")
                .behavior(new End())
                .endActivity()
                .endActivity()
                .buildProcessDefinition();

        PvmProcessInstance processInstance = processDefinition.createProcessInstance();
        processInstance.start();

        assertTrue(processInstance.isEnded());
    }

}

package com.baidu.iit.pvm.runtime;

import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午7:02
 */
public interface ProcessInstance extends Execution {

    String getProcessDefinitionId();

    String getBusinessKey();

    Map<String, Object> getProcessVariables();


}

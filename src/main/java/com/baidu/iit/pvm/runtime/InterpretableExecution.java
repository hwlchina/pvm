package com.baidu.iit.pvm.runtime;

import com.baidu.iit.pvm.PvmProcessElement;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.ExecutionListenerExecution;
import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pvm.process.ProcessDefinitionImpl;
import com.baidu.iit.pvm.process.TransitionImpl;

/**
 * 可中断流程调度接口定义，该接口定义了流程之间是怎么进行流转的方法
 * Created by 卫立 on 2014/4/7.
 */
public interface InterpretableExecution extends ActivityExecution, ExecutionListenerExecution, PvmProcessInstance {

    /**
     * 获取下一步要跳转的路径
     *
     * @param transition
     */
    void take(PvmTransition transition);

    /**
     * 设置事件的名称
     *
     * @param eventName
     */
    void setEventName(String eventName);

    /**
     * 设置事件的来源
     *
     * @param element
     */
    void setEventSource(PvmProcessElement element);

    /**
     * 事件监听器当前的调度值
     *
     * @return
     */
    Integer getExecutionListenerIndex();

    /**
     * 设置要执行的监听器
     * @param executionListenerIndex
     */
    void setExecutionListenerIndex(Integer executionListenerIndex);

    /**
     * 获取流程的定义
     * @return
     */
    ProcessDefinitionImpl getProcessDefinition();


    /**
     * 设置当前的执行节点
     * @param activity
     */
    void setActivity(ActivityImpl activity);


    /**
     * 调度器具体的执行类
     * @param etomicOperation
     */
    void performOperation(AtomicOperation etomicOperation);

    /**
     * 是否是单元
     * @return
     */
    boolean isScope();

    /**
     * 销毁子流程操作
     */
    void destroy();

    /**
     * 移除操作，可以动态的改变流程的定义
     */
    void remove();


    /**
     * 获取替换的调度器
     * @return
     */
    InterpretableExecution getReplacedBy();

    /**
     * 替换流程调度器
     * @param replacedBy
     */
    void setReplacedBy(InterpretableExecution replacedBy);


    /**
     * 获取子流程定义
     * @return
     */
    InterpretableExecution getSubProcessInstance();

    /**
     * 设置子流程实例
     * @param subProcessInstance
     */
    void setSubProcessInstance(InterpretableExecution subProcessInstance);

    /**
     * 获取父类的执行调度器
     * @return
     */
    InterpretableExecution getSuperExecution();

    /**
     * 进行联级删除操作
     * @param deleteReason
     */
    void deleteCascade(String deleteReason);

    /**
     * 是否是删除的根节点
     * @return
     */
    boolean isDeleteRoot();

    /**
     * 获取跳转路径
     * @return
     */
    TransitionImpl getTransition();

    /**
     * 设置跳转路径
     * @param object
     */
    void setTransition(TransitionImpl object);

    /**
     * 初始化调度器的功能
     */
    void initialize();

    /**
     * 设置上级调度器
     * @param parent
     */
    void setParent(InterpretableExecution parent);

    /**
     * 设置流程定义
     * @param processDefinitionImpl
     */
    void setProcessDefinition(ProcessDefinitionImpl processDefinitionImpl);

    /**
     * 设置流程实例
     * @param processInstance
     */
    void setProcessInstance(InterpretableExecution processInstance);

    /**
     * 是否是事件的单元
     * @return
     */
    boolean isEventScope();

    /**
     * 设置是否是事件单元
     * @param isEventScope
     */
    void setEventScope(boolean isEventScope);

    /**
     * 获取开始执行调度器
     * @return
     */
    StartingExecution getStartingExecution();

    /**
     * 去除开始调度器
     */
    void disposeStartingExecution();
}

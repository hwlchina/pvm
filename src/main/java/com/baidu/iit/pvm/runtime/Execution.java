package com.baidu.iit.pvm.runtime;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午7:03
 */
public interface Execution {

    String getId();

    boolean isEnded();

    String getActivityId();

    String getProcessInstanceId();

    String getParentId();


}

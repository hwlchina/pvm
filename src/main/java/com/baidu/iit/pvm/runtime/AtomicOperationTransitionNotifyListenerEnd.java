package com.baidu.iit.pvm.runtime;

import com.baidu.iit.pvm.PvmEventConstacts;
import com.baidu.iit.pvm.process.ScopeImpl;

/**
 * 路径选择完毕是跳转的对象
 *
 * @ThreadSafe
 *
 * Created by 卫立 on 2014/4/7.
 */
public class AtomicOperationTransitionNotifyListenerEnd extends AbstractEventAtomicOperation {

    @Override
    protected ScopeImpl getScope(InterpretableExecution execution) {
        return (ScopeImpl) execution.getActivity();
    }

    @Override
    protected String getEventName() {
        return PvmEventConstacts.EVENTNAME_END;
    }


    @Override
    protected void eventNotificationsCompleted(InterpretableExecution execution) {
        //跳转到是否是单元节点的判断中
        execution.performOperation(TRANSITION_DESTROY_SCOPE);
    }
}

package com.baidu.iit.pvm.runtime;

import com.baidu.iit.pvm.PvmEventConstacts;
import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pvm.process.ProcessDefinitionImpl;
import com.baidu.iit.pvm.process.ScopeImpl;

import java.util.List;

/**
 * 流程开始时触发的动作
 *
 * @ThreadSafe
 * Created by 卫立 on 2014/4/7.
 */
public class AtomicOperationProcessStart extends AbstractEventAtomicOperation {

    @Override
    protected ScopeImpl getScope(InterpretableExecution execution) {
        return execution.getProcessDefinition();
    }

    @Override
    protected String getEventName() {
        return PvmEventConstacts.EVENTNAME_START;
    }

    @Override
    protected void eventNotificationsCompleted(InterpretableExecution execution) {
        ProcessDefinitionImpl processDefinition = execution.getProcessDefinition();
        StartingExecution startingExecution = execution.getStartingExecution();

        //构建scope的起点堆栈
        List<ActivityImpl> initialActivityStack = processDefinition.getInitialActivityStack(startingExecution.getInitial());

        //获取单元中的第一个节点作为起始
        execution.setActivity(initialActivityStack.get(0));

        execution.performOperation(PROCESS_START_INITIAL);
    }
}
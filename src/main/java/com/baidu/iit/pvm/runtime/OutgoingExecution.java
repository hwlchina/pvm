package com.baidu.iit.pvm.runtime;

import com.baidu.iit.pvm.PvmTransition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 出路径执行操作类，该类的使用需要每次新创建一个对象
 * Created by 卫立 on 2014/4/7.
 *
 * @NotThreadSafe
 */
public class OutgoingExecution {

    private static Logger log = LoggerFactory.getLogger(OutgoingExecution.class);

    protected InterpretableExecution outgoingExecution;
    protected PvmTransition outgoingTransition;
    protected boolean isNew;

    public OutgoingExecution(InterpretableExecution outgoingExecution, PvmTransition outgoingTransition, boolean isNew) {
        this.outgoingExecution = outgoingExecution;
        this.outgoingTransition = outgoingTransition;
        this.isNew = isNew;
    }

    public void take() {
        if (outgoingExecution.getReplacedBy()!=null) {
            outgoingExecution = outgoingExecution.getReplacedBy();
        }
        if(!outgoingExecution.isDeleteRoot()) {
            outgoingExecution.take(outgoingTransition);
        } else {
            if(log.isDebugEnabled()) {
                log.debug("Not taking transition '{}', outgoing execution has ended.", outgoingTransition);
            }
        }
    }
}
package com.baidu.iit.pvm.runtime;

/**
 *
 * 执行体的接口定义
 * 同时他也是所有的执行器的工厂类
 *
 * Created by 卫立 on 2014/4/7.
 *
 */
public interface AtomicOperation {

    //流程开始时的调度执行器
    public final static AtomicOperation PROCESS_START = new AtomicOperationProcessStart();

    //流程开始后的初始化执行器，在PROCESS_START 后面执行
    public final static AtomicOperation PROCESS_START_INITIAL = new AtomicOperationProcessStartInitial();

    //流程结束调度器
    public final static AtomicOperation PROCESS_END = new AtomicOperationProcessEnd();

    //节点进入时的调度器
    public final static AtomicOperation ACTIVITY_START = new AtomicOperationActivityStart();

    //节点动作执行调度器
    public final static AtomicOperation ACTIVITY_EXECUTE = new AtomicOperationActivityExecute();

    //节点执行结束调度器
    public final static AtomicOperation ACTIVITY_END = new AtomicOperationActivityEnd();

    //连接线跳转结束事件通知调度器
    public final static AtomicOperation TRANSITION_NOTIFY_LISTENER_END = new AtomicOperationTransitionNotifyListenerEnd();

    //单元结束调度器
    public final static AtomicOperation TRANSITION_DESTROY_SCOPE = new AtomicOperationTransitionDestroyScope();

    //连接线获取事件通知调度器
    public final static AtomicOperation TRANSITION_NOTIFY_LISTENER_TAKE = new AtomicOperationTransitionNotifyListenerTake();

    //单元起始调度器
    public final static AtomicOperation TRANSITION_CREATE_SCOPE = new AtomicOperationTransitionCreateScope();

    //连接线开始转移是事件通知调度器
    public final static AtomicOperation TRANSITION_NOTIFY_LISTENER_START = new AtomicOperationTransitionNotifyListenerStart();

    //单元级联删除调度器
    public final static AtomicOperation DELETE_CASCADE = new AtomicOperationDeleteCascade();

    //单元级联删除未节点的事件调度器
    public final static AtomicOperation DELETE_CASCADE_FIRE_ACTIVITY_END = new AtomicOperationDeleteCascadeFireActivityEnd();

    /**
     * 具体的执行操作
     *
     * @param execution
     */
    void execute(InterpretableExecution execution);

    /**
     * 是否是异步执行
     *
     * @param execution
     * @return
     */
    boolean isAsync(InterpretableExecution execution);
}

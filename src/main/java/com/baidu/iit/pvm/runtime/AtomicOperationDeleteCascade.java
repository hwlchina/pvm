package com.baidu.iit.pvm.runtime;

import java.util.List;

/**
 * 当流程进入单元操作后，在单元操作结束时触发动作
 *
 * @ThreadSafe
 * Created by 卫立 on 2014/4/7.
 */
public class AtomicOperationDeleteCascade implements AtomicOperation {

    public boolean isAsync(InterpretableExecution execution) {
        return false;
    }

    public void execute(InterpretableExecution execution) {
        InterpretableExecution firstLeaf = findFirstLeaf(execution);
        if (firstLeaf.getSubProcessInstance()!=null) {
            firstLeaf.getSubProcessInstance().deleteCascade(execution.getDeleteReason());
        }
        firstLeaf.performOperation(AtomicOperation.DELETE_CASCADE_FIRE_ACTIVITY_END);
    }

    protected InterpretableExecution findFirstLeaf(InterpretableExecution execution) {
        List<InterpretableExecution> executions = (List<InterpretableExecution>) execution.getExecutions();
        if (executions.size()>0) {
            return findFirstLeaf(executions.get(0));
        }
        return execution;
    }
}

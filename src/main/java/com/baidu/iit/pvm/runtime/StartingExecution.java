package com.baidu.iit.pvm.runtime;

import com.baidu.iit.pvm.process.ActivityImpl;

/**
 * 开始信息保存类
 * Created by 卫立 on 2014/4/7.
 *
 * @NotThreadSafe
 */
public class StartingExecution {

    //当前选择的开始节点
    protected final ActivityImpl selectedInitial;

    public StartingExecution(ActivityImpl selectedInitial) {
        this.selectedInitial = selectedInitial;
    }

    public ActivityImpl getInitial() {
        return selectedInitial;
    }

}

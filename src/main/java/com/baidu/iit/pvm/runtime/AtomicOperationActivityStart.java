package com.baidu.iit.pvm.runtime;

import com.baidu.iit.pvm.PvmEventConstacts;
import com.baidu.iit.pvm.process.ScopeImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 路径跳转后获取到节点是触发的动作
 *
 * @ThreadSafe
 * Created by 卫立 on 2014/4/7.
 */
public class AtomicOperationActivityStart extends AbstractEventAtomicOperation {

    private static Logger logger= LoggerFactory.getLogger(AtomicOperationActivityStart.class);
    protected void eventNotificationsCompleted(InterpretableExecution execution) {
        if(logger.isDebugEnabled()){
            logger.debug("execution {} 开始执行",execution);
        }
        execution.performOperation(AtomicOperation.ACTIVITY_EXECUTE);
    }

    @Override
    protected String getEventName() {
        return PvmEventConstacts.EVENTNAME_START;
    }

    @Override
    protected ScopeImpl getScope(InterpretableExecution execution) {
        return (ScopeImpl) execution.getActivity();
    }


}

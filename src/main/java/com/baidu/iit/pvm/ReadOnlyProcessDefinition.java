package com.baidu.iit.pvm;

/**
 * 定义只读流程定义的类
 * 该类的作用是只能用来读取相关的信息而不能做修改
 * Created by 卫立 on 2014/4/7.
 */
public interface ReadOnlyProcessDefinition extends PvmScope {

    /**
     * 获取流程名称
     * @return
     */
    String getName();

    /**
     * 定义流程的关键词
     * @return
     */
    String getKey();

    /**
     * 流程描述信息
     * @return
     */
    String getDescription();


    /**
     * 获取初始化节点
     * @return
     */
    PvmActivity getInitial();

}

package com.baidu.iit.pvm;

/**
 * 流程事件常量定义类
 * Created by 卫立 on 2014/4/7.
 */
public final class PvmEventConstacts {

    public static final String EVENTNAME_START = "start";
    public static final String EVENTNAME_END = "end";
    public static final String EVENTNAME_TAKE = "take";

}

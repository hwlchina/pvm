package com.baidu.iit.pvm;

import java.util.List;

/**
 * 流程实例类该流程是流程流转的载体
 * Created by 卫立 on 2014/4/7.
 */
public interface PvmProcessInstance extends PvmExecution {

    /**
     * 开始流程，流程执行的入口
     */
    void start();

    /**
     * 根据节点ID查找执行体
     * @param activityId
     * @return
     */
    PvmExecution findExecution(String activityId);

    /**
     * 获取所有节点ID名
     * @return
     */
    List<String> findActiveActivityIds();

    /**
     * 是否结束流程
     * @return
     */
    boolean isEnded();

    /**
     * 删除底下的单元流程
     * @param deleteReason
     */
    void deleteCascade(String deleteReason);
}

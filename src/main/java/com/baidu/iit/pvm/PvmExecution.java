package com.baidu.iit.pvm;

import java.util.Map;

/**
 * 定义节点的执行体，流程的跳转已经节点的执行由该接口定义
 * Created by 卫立 on 2014/4/7.
 */
public interface PvmExecution {

    /**
     * 触发下一个节点的执行
     * @param signalName  触发的 跳转名称
     * @param signalData  传递的参数
     */
    void signal(String signalName, Object signalData);


    /**
     * 获取当前的节点
     * @return
     */
    PvmActivity getActivity();

    /**
     * 根据变量的名称判断变量是否存在
     * @param variableName
     * @return
     */
    boolean hasVariable(String variableName);

    /**
     *  设置变量，变量在整个流程中上下文传递
     * @param variableName
     * @param value
     */
    void setVariable(String variableName, Object value);


    /**
     * 根据名称获取变量
     * @param variableName
     * @return
     */
    Object getVariable(String variableName);


    /**
     * 获取所有的上下文变量
     * @return
     */
    Map<String, Object> getVariables();

}

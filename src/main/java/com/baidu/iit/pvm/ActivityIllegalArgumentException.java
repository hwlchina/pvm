package com.baidu.iit.pvm;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午7:36
 */
public class ActivityIllegalArgumentException extends ActivityException {

    public ActivityIllegalArgumentException(String message) {
        super(message);
    }

    public ActivityIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }
}

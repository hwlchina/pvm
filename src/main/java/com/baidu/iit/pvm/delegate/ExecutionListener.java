package com.baidu.iit.pvm.delegate;

import java.io.Serializable;

/**
 * 流程执行任务监听接口
 * Created by 卫立 on 2014/4/7.
 */
public interface ExecutionListener extends Serializable {

    /**
     * 事件通知接口
     * @param execution
     * @throws Exception
     */
    void notify(DelegateExecution execution) throws Exception;

}

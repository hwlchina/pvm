package com.baidu.iit.pvm.delegate;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * 定义流程执行上下问的 变量操作
 * Created by 卫立 on 2014/4/7.
 */
public interface VariableScope {

    /**
     * 获取所有的变量定义，全局变量
     * @return
     */
    Map<String, Object> getVariables();


    /**
     * 获取当前线程的变量
     * @return
     */
    Map<String, Object> getVariablesLocal();

    /**
     * 根据变量名称获取全局的变量值
     * @param variableName
     * @return
     */
    Object getVariable(String variableName);

    /**
     * 根据变量名获取当前线程的变量值
     * @param variableName
     * @return
     */
    Object getVariableLocal(String variableName);

    /**
     * 获取全局的所有变量名称
     * @return
     */
    Set<String> getVariableNames();

    /**
     * 获取当前线程的所有变量值
     * @return
     */
    Set<String> getVariableNamesLocal();


    /**
     * 设置全局的变量值
     * @param variableName
     * @param value
     */
    void setVariable(String variableName, Object value);

    /**
     * 设置当前线程的变量值
     * @param variableName
     * @param value
     * @return
     */
    Object setVariableLocal(String variableName, Object value);


    /**
     * 设置全局变量值
     * @param variables
     */
    void setVariables(Map<String, ? extends Object> variables);

    /**
     * 设置当前线程的变量值
     * @param variables
     */
    void setVariablesLocal(Map<String, ? extends Object> variables);

    /**
     * 是否有全局变量
     * @return
     */
    boolean hasVariables();

    /**
     * 当前线程是否有变量
     * @return
     */
    boolean hasVariablesLocal();

    /**
     * 全局变量里是否包含某个值
     * @param variableName
     * @return
     */
    boolean hasVariable(String variableName);

    /**
     * 当前线程是否包含某个值
     * @param variableName
     * @return
     */
    boolean hasVariableLocal(String variableName);

    /**
     * 创建当前线程的变量
     * @param variableName
     * @param value
     */
    void createVariableLocal(String variableName, Object value);

    /**
     * 删除全局变量
     * @param variableName
     */
    void removeVariable(String variableName);

    /**
     * 删除当前线程的变量
     * @param variableName
     */
    void removeVariableLocal(String variableName);

    /**
     * 根据变量名批量删除全局变量
     * @param variableNames
     */
    void removeVariables(Collection<String> variableNames);

    /**
     * 根据变量名删除当前线程的变量
     * @param variableNames
     */
    void removeVariablesLocal(Collection<String> variableNames);

    /**
     * 删除全局变量
     */
    void  removeVariables();

    /**
     * 删除当前线程的变量
     */
    void removeVariablesLocal();
}
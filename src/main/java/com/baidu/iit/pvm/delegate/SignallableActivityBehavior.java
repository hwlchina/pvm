package com.baidu.iit.pvm.delegate;

/**
 * 支持中断的流程唤醒动作接口
 * Created by 卫立 on 2014/4/7.
 */
public interface SignallableActivityBehavior extends ActivityBehavior {

    void signal(ActivityExecution execution, String signalEvent, Object signalData) throws Exception;

}

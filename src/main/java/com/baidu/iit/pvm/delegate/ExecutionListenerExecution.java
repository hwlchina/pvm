package com.baidu.iit.pvm.delegate;

import com.baidu.iit.pvm.PvmProcessElement;

/**
 *
 * 流程执行监听事件的执行接口
 * Created by 卫立 on 2014/4/7.
 */
public interface ExecutionListenerExecution extends DelegateExecution {

    /**
     * 流程事件名称
     * @return
     */
    String getEventName();

    /**
     * 流程事件的源节点
     * @return
     */
    PvmProcessElement getEventSource();

    /**
     * 删除原因
     * @return
     */
    String getDeleteReason();
}

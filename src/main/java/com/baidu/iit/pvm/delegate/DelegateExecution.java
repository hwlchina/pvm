package com.baidu.iit.pvm.delegate;

/**
 * 流程信息获取接口
 * Created by 卫立 on 2014/4/7.
 */
public interface DelegateExecution extends VariableScope {

    String getId();


    /**
     * 获取流程实例的ID
     * @return
     */
    String getProcessInstanceId();

    /**
     * 获取当前的事件名称
     * @return
     */
    String getEventName();


    /**
     * 获取流程定义名称
     * @return
     */
    String getProcessDefinitionId();


    /**
     * 获取父节点状态名
     * @return
     */
    String getParentId();


    /**
     * 获取当前节点的ID
     * @return
     */
    String getCurrentActivityId();


    /**
     * 获取当前节点的名称
     * @return
     */
    String getCurrentActivityName();


}

package com.baidu.iit.pvm.delegate;

import java.io.Serializable;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午3:49
 */
public interface Condition extends Serializable {

    boolean evaluate(DelegateExecution execution);
}

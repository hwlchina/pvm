package com.baidu.iit.pvm.delegate;

import com.baidu.iit.pvm.PvmActivity;
import com.baidu.iit.pvm.PvmProcessDefinition;
import com.baidu.iit.pvm.PvmProcessInstance;
import com.baidu.iit.pvm.PvmTransition;

import java.util.List;

/**
 *
 * 节点执行类定义
 * 流程的跳转通过这个类来实现调度工作
 * Created by 卫立 on 2014/4/7.
 */
public interface ActivityExecution extends DelegateExecution {

    /**
     * 获取当前的节点信息
     * @return
     */
    PvmActivity getActivity();


    /**
     * 获取下一步的跳转路径
     * @param transition
     */
    void take(PvmTransition transition);


    /**
     * 创建执行功能
     * @return
     */
    ActivityExecution createExecution();


    /**
     * 创建子流程
     * @param processDefinition
     * @return
     */
    PvmProcessInstance createSubProcessInstance(PvmProcessDefinition processDefinition);


    /**
     * 获取上一级的执行体
     * @return
     */
    ActivityExecution getParent();


    /**
     * 获取所有的可执行体
     * @return
     */
    List<? extends ActivityExecution> getExecutions();


    /**
     * 结束流程
     */
    void end();

    /**
     * 设置是否是当前运行状态
     * @param isActive
     */
    void setActive(boolean isActive);


    /**
     * 是否是活动的
     * @return
     */
    boolean isActive();


    /**
     * 是否是完结的
     * @return
     */
    boolean isEnded();


    /**
     * 是否是并发的执行
     * @param isConcurrent
     */
    void setConcurrent(boolean isConcurrent);

    /**
     * 是否是并行执行的
     * @return
     */
    boolean isConcurrent();

    /**
     * 是否是流程实例烈性
     * @return
     */
    boolean isProcessInstanceType();


    /**
     * 取消活动状态
     */
    void inactivate();


    /**
     * 是否是单元
     * @return
     */
    boolean isScope();


    /**
     * 设置是否是单元
     * @param isScope
     */
    void setScope(boolean isScope);


    /**
     * 根据节点获取节点下的不活动的执行对象
     * @param activity
     * @return
     */
    List<ActivityExecution> findInactiveConcurrentExecutions(PvmActivity activity);

    /**
     * 流转到下一步的全部路径，并JOIN相关的操作
     * @param outgoingTransitions
     * @param joinedExecutions
     */
    void takeAll(List<PvmTransition> outgoingTransitions, List<ActivityExecution> joinedExecutions);

    /**
     * 执行当前节点的动作
     * @param activity
     */
    void executeActivity(PvmActivity activity);


    /**
     * 销毁当前的单元
     * @param string
     */
    void destroyScope(String string);
}

package com.baidu.iit.pvm.delegate;

/**
 * 子流程操作定义接口
 * Created by 卫立 on 2014/4/7.
 */
public interface SubProcessActivityBehavior extends ActivityBehavior {

    /**
     * 子流程完成前的操作
     * @param execution
     * @param subProcessInstance
     * @throws Exception
     */
    void completing(DelegateExecution execution, DelegateExecution subProcessInstance) throws Exception;

    /**
     * 子流程完成后的操作
     * @param execution
     * @throws Exception
     */
    void completed(ActivityExecution execution) throws Exception;
}

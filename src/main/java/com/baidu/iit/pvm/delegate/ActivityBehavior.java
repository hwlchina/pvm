package com.baidu.iit.pvm.delegate;

import java.io.Serializable;

/**
 * 节点执行动作接口
 * Created by 卫立 on 2014/4/7.
 */
public interface ActivityBehavior extends Serializable {

    /**
     * 执行接口
     * @param execution 流程的上下文信息，可以从这里取相关的数据信息
     * @throws Exception
     */
    void execute(ActivityExecution execution) throws Exception;
}

package com.baidu.iit.pvm.delegate;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午4:21
 */
public abstract class DelegateInvocation {

    protected Object invocationResult;
    protected Object[] invocationParameters;

    public void proceed() throws Exception {
        invoke();
    }

    protected abstract void invoke() throws Exception;

    public Object getInvocationResult() {
        return invocationResult;
    }

    public Object[] getInvocationParameters() {
        return invocationParameters;
    }

    public abstract Object getTarget();
}

package com.baidu.iit.pvm.delegate;

/**
 * 单元节点的执行动作接口
 * Created by 卫立 on 2014/4/7.
 */
public interface CompositeActivityBehavior extends ActivityBehavior {

    /**
     * 定义单元操作结束后的执行动作的接口函数
     * @param execution
     */
    void lastExecutionEnded(ActivityExecution execution);
}

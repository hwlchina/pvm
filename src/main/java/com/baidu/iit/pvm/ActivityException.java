package com.baidu.iit.pvm;

/**
 * Activity 异常处理里类
 * Created by 卫立 on 2014/4/7.
 */
public class ActivityException extends RuntimeException {

    public ActivityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActivityException(String message) {
        super(message);
    }
}

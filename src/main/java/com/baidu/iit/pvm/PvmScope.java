package com.baidu.iit.pvm;

import java.util.List;

/**
 * 流程单元定义类
 * Created by 卫立 on 2014/4/7.
 */
public interface PvmScope extends PvmProcessElement {

    /**
     * 获取单元节点下的所有执行节点
     * @return
     */
    List<? extends PvmActivity> getActivities();

    /**
     * 查找单元底下的节点
     * @param activityId
     * @return
     */
    PvmActivity findActivity(String activityId);

}

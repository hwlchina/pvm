package com.baidu.iit.pvm;

import java.io.Serializable;

/**
 * 流程基础元素定义
 * Created by 卫立 on 2014/4/7.
 */
public interface PvmProcessElement extends Serializable {

    /**
     * 获取定义的唯一表示
     * @return
     */
    String getId();

    /**
     * 获取所属的流程定义
     * @return
     */
    PvmProcessDefinition getProcessDefinition();

    /**
     * 获取流程节点的属性信息
     * @param name
     * @return
     */
    Object getProperty(String name);
}

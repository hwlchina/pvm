package com.baidu.iit.pvm.process;

import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ExecutionListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 流程中节点之间的跳转连接类定义
 * Created by 卫立 on 2014/4/7.
 */
public class TransitionImpl extends ProcessElementImpl implements PvmTransition {

    //起始节点
    protected ActivityImpl source;

    //结束节点
    protected ActivityImpl destination;

    //执行监听类
    protected List<ExecutionListener> executionListeners;


    public TransitionImpl(String id, ProcessDefinitionImpl processDefinition) {
        super(id, processDefinition);
    }

    /**
     * 获取连线对应的起始节点
     * @return
     */
    public ActivityImpl getSource() {
        return source;
    }


    /**
     * 设置起始节点
     * @param destination
     */
    public void setDestination(ActivityImpl destination) {
        this.destination = destination;
        destination.getIncomingTransitions().add(this);
    }


    /**
     * 添加简监听事件
     * @param executionListener
     */
    public void addExecutionListener(ExecutionListener executionListener) {
        if (executionListeners==null) {
            executionListeners = new ArrayList<ExecutionListener>();
        }
        executionListeners.add(executionListener);
    }



    public String toString() {
        return "("+source.getId()+")--"+(id!=null?id+"-->(":">(")+destination.getId()+")";
    }


    /**
     * 获取所有的监听事件
     * @return
     */
    public List<ExecutionListener> getExecutionListeners() {
        if (executionListeners==null) {
            return Collections.EMPTY_LIST;
        }
        return executionListeners;
    }

    /**
     * 设置起始节点
     * @param source
     */
    protected void setSource(ActivityImpl source) {
        this.source = source;
    }

    /**
     * 设置目标节点
     * @return
     */
    public ActivityImpl getDestination() {
        return destination;
    }


    /**
     * 设置执行监听器
     * @param executionListeners
     */
    public void setExecutionListeners(List<ExecutionListener> executionListeners) {
        this.executionListeners = executionListeners;
    }
}

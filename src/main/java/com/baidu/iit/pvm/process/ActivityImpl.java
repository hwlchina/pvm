package com.baidu.iit.pvm.process;

import com.baidu.iit.pvm.PvmActivity;
import com.baidu.iit.pvm.PvmException;
import com.baidu.iit.pvm.PvmTransition;
import com.baidu.iit.pvm.delegate.ActivityBehavior;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程节点实现类
 * Created by 卫立 on 2014/4/7.
 */
public class ActivityImpl extends ScopeImpl implements PvmActivity {

    //接出连接线定义
    protected List<TransitionImpl> outgoingTransitions = new ArrayList<TransitionImpl>();

    //接出连接线名称
    protected Map<String, TransitionImpl> namedOutgoingTransitions = new HashMap<String, TransitionImpl>();

    //节点的变量值
    protected Map<String, Object> variables;


    //接入连接线
    protected List<TransitionImpl> incomingTransitions = new ArrayList<TransitionImpl>();


    //节点执行时调用的动作
    protected ActivityBehavior activityBehavior;

    //前置单元
    protected ScopeImpl parent;

    //是否是执行单元
    protected boolean isScope;

    //是否是异步节点
    protected boolean isAsync;

    //是否是互斥节点
    protected boolean isExclusive;


    /**
     * 构造函数
     * @param id 节点的名称
     * @param processDefinition 流程的定义
     */
    public ActivityImpl(String id, ProcessDefinitionImpl processDefinition) {
        super(id, processDefinition);
    }

    /**
     * 创建接出连接线
     * @return
     */
    public TransitionImpl createOutgoingTransition() {
        return createOutgoingTransition(null);
    }


    /**
     * 创建接出连接线
     * @param transitionId
     * @return
     */
    public TransitionImpl createOutgoingTransition(String transitionId) {
        TransitionImpl transition = new TransitionImpl(transitionId, processDefinition);
        transition.setSource(this);
        outgoingTransitions.add(transition);

        if (transitionId!=null) {
            if (namedOutgoingTransitions.containsKey(transitionId)) {
                throw new PvmException("activity '"+id+" has duplicate transition '"+transitionId+"'");
            }
            namedOutgoingTransitions.put(transitionId, transition);
        }

        return transition;
    }


    /**
     * 根据连接线的名称获取接出连接线
     * @param transitionId
     * @return
     */
    public TransitionImpl findOutgoingTransition(String transitionId) {
        return namedOutgoingTransitions.get(transitionId);
    }

    /**
     * 获取节点的名称
     * @return
     */
    public String toString() {
        return "Activity("+id+")";
    }

    /**
     * 获取上一级的节点
     * @return
     */
    public ActivityImpl getParentActivity() {
        if (parent instanceof ActivityImpl) {
            return (ActivityImpl) parent;
        }
        return null;
    }


    /**
     * 设置接出连接线
     * O->
     * @param outgoingTransitions
     */
    protected void setOutgoingTransitions(List<TransitionImpl> outgoingTransitions) {
        this.outgoingTransitions = outgoingTransitions;
    }

    /**
     * 设置上级节点
     * @param parent
     */
    protected void setParent(ScopeImpl parent) {
        this.parent = parent;
    }


    /**
     * 设置接入连接线
     * ->O
     * @param incomingTransitions
     */
    protected void setIncomingTransitions(List<TransitionImpl> incomingTransitions) {
        this.incomingTransitions = incomingTransitions;
    }


    /**
     * 获取所有的接出连接线
     * @return
     */
    public List<PvmTransition> getOutgoingTransitions() {
        return (List) outgoingTransitions;
    }


    /**
     * 获取节点上的执行动作
     * @return
     */
    public ActivityBehavior getActivityBehavior() {
        return activityBehavior;
    }


    /**
     * 设置执行动作
     * @param activityBehavior
     */
    public void setActivityBehavior(ActivityBehavior activityBehavior) {
        this.activityBehavior = activityBehavior;
    }


    /**
     * 获取上级单元
     * @return
     */
    public ScopeImpl getParent() {
        return parent;
    }


    /**
     * 获取所有的接入连接线
     * @return
     */
    public List<PvmTransition> getIncomingTransitions() {
        return (List) incomingTransitions;
    }

    /**
     * 获取所有的变量信息
     * @return
     */
    public Map<String, Object> getVariables() {
        return variables;
    }


    /**
     * 设置变量信息
     * @param variables
     */
    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    /**
     * 是否是单元节点
     * @return
     */
    public boolean isScope() {
        return isScope;
    }


    /**
     * 设置是否是单元节点
     * @param isScope
     */
    public void setScope(boolean isScope) {
        this.isScope = isScope;
    }

    /**
     * 是否是异步执行节点
     * @return
     */
    public boolean isAsync() {
        return isAsync;
    }

    /**
     * 设置是否是异步执行节点
     * @param isAsync
     */
    public void setAsync(boolean isAsync) {
        this.isAsync = isAsync;
    }

    /**
     * 是否是排斥节点
     * @return
     */
    public boolean isExclusive() {
        return isExclusive;
    }

    /**
     * 设置是否是排斥节点
     * @param isExclusive
     */
    public void setExclusive(boolean isExclusive) {
        this.isExclusive = isExclusive;
    }

}

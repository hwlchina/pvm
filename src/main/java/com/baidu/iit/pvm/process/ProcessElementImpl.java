package com.baidu.iit.pvm.process;

import com.baidu.iit.pvm.PvmProcessElement;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 流程节点元素定义类
 * Created by 卫立 on 2014/4/7.
 */
public class ProcessElementImpl implements PvmProcessElement {

    //标注元素的名称
    protected String id;

    //所属的流程定义
    protected ProcessDefinitionImpl processDefinition;

    //节点的属性定义
    protected Map<String, Object> properties;

    /**
     * 流程元素的ID和所属流程定义
     *
     * @param id
     * @param processDefinition
     */
    public ProcessElementImpl(String id, ProcessDefinitionImpl processDefinition) {
        this.id = id;
        this.processDefinition = processDefinition;
    }


    /**
     * 设置流程属性
     *
     * @param name
     * @param value
     */
    public void setProperty(String name, Object value) {
        if (properties == null) {
            properties = new HashMap<String, Object>();
        }
        properties.put(name, value);
    }


    /**
     * 获取流程熟悉
     *
     * @param name
     * @return
     */
    public Object getProperty(String name) {
        if (properties == null) {
            return null;
        }
        return properties.get(name);
    }


    /**
     * 获取所有的流程属性信息
     *
     * @return
     */
    public Map<String, Object> getProperties() {
        if (properties == null) {
            return Collections.EMPTY_MAP;
        }
        return properties;
    }


    public String getId() {
        return id;
    }

    /**
     * 设置属性
     *
     * @param properties
     */
    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    /**
     * 获取
     *
     * @return
     */
    public ProcessDefinitionImpl getProcessDefinition() {
        return processDefinition;
    }

}

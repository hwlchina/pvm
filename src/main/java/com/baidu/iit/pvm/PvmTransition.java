package com.baidu.iit.pvm;

/**
 * 定义节点和节点直接的跳转类接口
 * Created by 卫立 on 2014/4/7.
 */
public interface PvmTransition extends PvmProcessElement {


    /**
     * 获取跳转的始发节点
     * @return
     */
    PvmActivity getSource();

    /**
     * 获取跳转后的目标节点
     * @return
     */
    PvmActivity getDestination();
}

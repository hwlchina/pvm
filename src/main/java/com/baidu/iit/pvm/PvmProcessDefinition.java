package com.baidu.iit.pvm;

/**
 * 流程定义模块
 * Created by 卫立 on 2014/4/7.
 */
public interface PvmProcessDefinition extends ReadOnlyProcessDefinition {

    /**
     * 创建流程实体接口
     * @return
     */
    PvmProcessInstance createProcessInstance();

}

package com.baidu.iit.pvm;

/**
 * 引擎执行异常处理
 * Created by 卫立 on 2014/4/7.
 */
public class PvmException extends RuntimeException {

    public PvmException(String message, Throwable cause) {
        super(message, cause);
    }

    public PvmException(String message) {
        super(message);
    }
}

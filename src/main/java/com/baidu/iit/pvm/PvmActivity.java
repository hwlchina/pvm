package com.baidu.iit.pvm;

import java.util.List;

/**
 *
 * 流程节点定义类
 * Created by 卫立 on 2014/4/7.
 */
public interface PvmActivity extends PvmScope {

    //判断是否是异步节点
    boolean isAsync();

    /**
     * 判断该节点是否有互斥操作
     * @return
     */
    boolean isExclusive();


    /**
     * 获取该节点的上级单元
     * @return
     */
    PvmScope getParent();

    /**
     * 获取所有的流入路径
     * @return
     */
    List<PvmTransition> getIncomingTransitions();

    /**
     * 获取所有的出口路径
     * @return
     */
    List<PvmTransition> getOutgoingTransitions();


    /**
     * 根据ID查找出口路径
     * @param transitionId
     * @return
     */
    PvmTransition findOutgoingTransition(String transitionId);
}

package com.baidu.iit.pxp.listener;

import com.baidu.iit.pxp.event.ActivityEvent;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 上午11:40
 */
public interface ActivityEventListener {

    void onEvent(ActivityEvent event);

    boolean isFailOnException();
}

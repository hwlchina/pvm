package com.baidu.iit.pxp.listener;

import com.baidu.iit.pxp.model.DelegateTask;

import java.io.Serializable;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午10:49
 */
public interface TaskListener extends Serializable {

    String EVENTNAME_CREATE = "create";
    String EVENTNAME_ASSIGNMENT = "assignment";
    String EVENTNAME_COMPLETE = "complete";
    String EVENTNAME_DELETE = "delete";


    String EVENTNAME_ALL_EVENTS = "all";

    void notify(DelegateTask delegateTask);
}

package com.baidu.iit.pxp.listener;

import com.baidu.iit.pxp.event.ActivityEntityEvent;
import com.baidu.iit.pxp.event.ActivityEvent;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午4:00
 */
public abstract class BaseDelegateEventListener implements ActivityEventListener {

    protected Class<?> entityClass;

    public void setEntityClass(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    protected boolean isValidEvent(ActivityEvent event) {
        boolean valid = false;
        if(entityClass != null) {
            if(event instanceof ActivityEntityEvent) {
                Object entity = ((ActivityEntityEvent) event).getEntity();
                if(entity != null) {
                    valid = entityClass.isAssignableFrom(entity.getClass());
                }
            }
        } else {
            valid = true;
        }
        return valid;
    }

}

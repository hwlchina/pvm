package com.baidu.iit.pxp.listener;

import com.baidu.iit.pxp.delegate.Expression;
import com.baidu.iit.pxp.model.DelegateTask;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午3:47
 */
public class ExpressionTaskListener implements TaskListener {

    protected Expression expression;

    public ExpressionTaskListener(Expression expression) {
        this.expression = expression;
    }

    public void notify(DelegateTask delegateTask) {
        expression.getValue(delegateTask);
    }


    public String getExpressionText() {
        return expression.getExpressionText();
    }

}

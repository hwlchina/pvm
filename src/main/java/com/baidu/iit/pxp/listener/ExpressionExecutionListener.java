package com.baidu.iit.pxp.listener;

import com.baidu.iit.pvm.delegate.DelegateExecution;
import com.baidu.iit.pvm.delegate.ExecutionListener;
import com.baidu.iit.pxp.delegate.Expression;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午3:52
 */
public class ExpressionExecutionListener implements ExecutionListener {

    protected Expression expression;

    public ExpressionExecutionListener(Expression expression) {
        this.expression = expression;
    }

    public void notify(DelegateExecution execution) throws Exception {
        expression.getValue(execution);
    }

    public String getExpressionText() {
        return expression.getExpressionText();
    }
}

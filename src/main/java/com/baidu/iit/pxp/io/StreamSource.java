package com.baidu.iit.pxp.io;

import java.io.InputStream;

/**
 * Created by 卫立 on 2014/4/24.
 */
public interface StreamSource extends InputStreamProvider {
    InputStream getInputStream();

}

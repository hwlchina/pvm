package com.baidu.iit.pxp.io;

import com.baidu.iit.pvm.ActivityException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:36
 */
public class InputStreamSource implements StreamSource {

    protected BufferedInputStream inputStream;
    protected byte[] bytes;

    public InputStreamSource(InputStream inputStream) {
        this.inputStream = new BufferedInputStream(inputStream);
    }

    public InputStream getInputStream() {
        if (bytes == null) {
            try {
                bytes = getBytesFromInputStream(inputStream);
            } catch (IOException e) {
                throw new ActivityException("Could not read from inputstream", e);
            }
        }
        return new BufferedInputStream(new ByteArrayInputStream(bytes));
    }

    public String toString() {
        return "InputStream";
    }

    public  byte[] getBytesFromInputStream(InputStream inStream) throws IOException {
        long length = inStream.available();
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = inStream.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new ActivityException("Could not completely read inputstream ");
        }

        inStream.close();
        return bytes;
    }

}

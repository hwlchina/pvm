package com.baidu.iit.pxp.io;

import java.io.InputStream;

/**
 * Created by 卫立 on 2014/4/24.
 */
public interface InputStreamProvider {

    /**
     * Creates a <b>NEW</b> {@link java.io.InputStream} to the provided resource.
     */
    InputStream getInputStream();

}
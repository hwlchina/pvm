package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午3:20
 */
public class DataGridField extends BaseElement {

    protected String name;
    protected String value;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public DataGridField clone() {
        DataGridField clone = new DataGridField();
        clone.setValues(this);
        return clone;
    }

    public void setValues(DataGridField otherField) {
        setName(otherField.getName());
        setValue(otherField.getValue());
    }
}

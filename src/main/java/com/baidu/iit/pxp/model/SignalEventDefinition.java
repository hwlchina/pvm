package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午7:44
 */
public class SignalEventDefinition extends EventDefinition {

    protected String signalRef;
    protected boolean async;

    public String getSignalRef() {
        return signalRef;
    }

    public void setSignalRef(String signalRef) {
        this.signalRef = signalRef;
    }

    public boolean isAsync() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }

    public SignalEventDefinition clone() {
        SignalEventDefinition clone = new SignalEventDefinition();
        clone.setValues(this);
        return clone;
    }

    public void setValues(SignalEventDefinition otherDefinition) {
        super.setValues(otherDefinition);
        setSignalRef(otherDefinition.getSignalRef());
        setAsync(otherDefinition.isAsync());
    }
}

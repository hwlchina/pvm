package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:39
 */
public class EventListener extends BaseElement {

    protected String events;
    protected String implementationType;
    protected String implementation;
    protected String entityType;

    public String getEvents() {
        return events;
    }

    public void setEvents(String events) {
        this.events = events;
    }

    public String getImplementationType() {
        return implementationType;
    }

    public void setImplementationType(String implementationType) {
        this.implementationType = implementationType;
    }

    public String getImplementation() {
        return implementation;
    }

    public void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityType() {
        return entityType;
    }

    public EventListener clone() {
        EventListener clone = new EventListener();
        clone.setValues(this);
        return clone;
    }

    public void setValues(EventListener otherListener) {
        setEvents(otherListener.getEvents());
        setImplementation(otherListener.getImplementation());
        setImplementationType(otherListener.getImplementationType());
        setEntityType(otherListener.getEntityType());
    }
}

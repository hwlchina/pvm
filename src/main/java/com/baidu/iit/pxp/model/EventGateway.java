package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:43
 */
public class EventGateway extends Gateway {

    public EventGateway clone() {
        EventGateway clone = new EventGateway();
        clone.setValues(this);
        return clone;
    }

    public void setValues(EventGateway otherElement) {
        super.setValues(otherElement);
    }
}

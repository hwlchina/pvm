package com.baidu.iit.pxp.model;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午7:12
 */
public abstract class BaseElement implements HasExtensionAttributes {

    protected String id;
    protected int xmlRowNumber;
    protected int xmlColumnNumber;
    protected Map<String, List<ExtensionElement>> extensionElements = new LinkedHashMap<String, List<ExtensionElement>>();
    protected Map<String, List<ExtensionAttribute>> attributes = new LinkedHashMap<String, List<ExtensionAttribute>>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getXmlRowNumber() {
        return xmlRowNumber;
    }

    public void setXmlRowNumber(int xmlRowNumber) {
        this.xmlRowNumber = xmlRowNumber;
    }

    public int getXmlColumnNumber() {
        return xmlColumnNumber;
    }

    public void setXmlColumnNumber(int xmlColumnNumber) {
        this.xmlColumnNumber = xmlColumnNumber;
    }

    public Map<String, List<ExtensionElement>> getExtensionElements() {
        return extensionElements;
    }

    public void addExtensionElement(ExtensionElement extensionElement) {
        if (extensionElement != null && StringUtils.isNotEmpty(extensionElement.getName())) {
            List<ExtensionElement> elementList = null;
            if (this.extensionElements.containsKey(extensionElement.getName()) == false) {
                elementList = new ArrayList<ExtensionElement>();
                this.extensionElements.put(extensionElement.getName(), elementList);
            }
            this.extensionElements.get(extensionElement.getName()).add(extensionElement);
        }
    }

    public void setExtensionElements(Map<String, List<ExtensionElement>> extensionElements) {
        this.extensionElements = extensionElements;
    }

    @Override
    public Map<String, List<ExtensionAttribute>> getAttributes() {
        return attributes;
    }

    @Override
    public String getAttributeValue(String namespace, String name) {
        List<ExtensionAttribute> attributes = getAttributes().get(name);
        if (attributes != null && !attributes.isEmpty()) {
            for (ExtensionAttribute attribute : attributes) {
                if ( namespace.equals(attribute.getNamespace()))
                    return attribute.getValue();
            }
        }
        return null;
    }

    @Override
    public void addAttribute(ExtensionAttribute attribute) {
        if (attribute != null && StringUtils.isNotEmpty(attribute.getName())) {
            List<ExtensionAttribute> attributeList = null;
            if (this.attributes.containsKey(attribute.getName()) == false) {
                attributeList = new ArrayList<ExtensionAttribute>();
                this.attributes.put(attribute.getName(), attributeList);
            }
            this.attributes.get(attribute.getName()).add(attribute);
        }
    }

    @Override
    public void setAttributes(Map<String, List<ExtensionAttribute>> attributes) {
        this.attributes = attributes;
    }

    public void setValues(BaseElement otherElement) {
        setId(otherElement.getId());

        extensionElements = new LinkedHashMap<String, List<ExtensionElement>>();
        if (otherElement.getExtensionElements() != null && otherElement.getExtensionElements().size() > 0) {
            for (String key : otherElement.getExtensionElements().keySet()) {
                List<ExtensionElement> otherElementList = otherElement.getExtensionElements().get(key);
                if (otherElementList != null && otherElementList.size() > 0) {
                    List<ExtensionElement> elementList = new ArrayList<ExtensionElement>();
                    for (ExtensionElement extensionElement : otherElementList) {
                        elementList.add(extensionElement.clone());
                    }
                    extensionElements.put(key, elementList);
                }
            }
        }

        attributes = new LinkedHashMap<String, List<ExtensionAttribute>>();
        if (otherElement.getAttributes() != null && otherElement.getAttributes().size() > 0) {
            for (String key : otherElement.getAttributes().keySet()) {
                List<ExtensionAttribute> otherAttributeList = otherElement.getAttributes().get(key);
                if (otherAttributeList != null && otherAttributeList.size() > 0) {
                    List<ExtensionAttribute> attributeList = new ArrayList<ExtensionAttribute>();
                    for (ExtensionAttribute extensionAttribute : otherAttributeList) {
                        attributeList.add(extensionAttribute.clone());
                    }
                    attributes.put(key, attributeList);
                }
            }
        }
    }

    public abstract BaseElement clone();
}
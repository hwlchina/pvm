package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:34
 */
public class ManualTask extends Task {

    public ManualTask clone() {
        ManualTask clone = new ManualTask();
        clone.setValues(this);
        return clone;
    }

    public void setValues(ManualTask otherElement) {
        super.setValues(otherElement);
    }
}


package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:41
 */
public class ExclusiveGateway extends Gateway {

    public ExclusiveGateway clone() {
        ExclusiveGateway clone = new ExclusiveGateway();
        clone.setValues(this);
        return clone;
    }

    public void setValues(ExclusiveGateway otherElement) {
        super.setValues(otherElement);
    }
}
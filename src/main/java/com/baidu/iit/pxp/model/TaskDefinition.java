package com.baidu.iit.pxp.model;

import com.baidu.iit.pxp.delegate.Expression;
import com.baidu.iit.pxp.listener.TaskListener;

import java.io.Serializable;
import java.util.*;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午10:47
 */
public class TaskDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    protected String key;

    protected Expression nameExpression;
    protected Expression ownerExpression;
    protected Expression descriptionExpression;
    protected Expression assigneeExpression;
    protected Set<Expression> candidateUserIdExpressions = new HashSet<Expression>();
    protected Set<Expression> candidateGroupIdExpressions = new HashSet<Expression>();
    protected Expression dueDateExpression;
    protected Expression priorityExpression;
    protected Expression categoryExpression;


    protected Map<String, List<TaskListener>> taskListeners = new HashMap<String, List<TaskListener>>();

    public TaskDefinition() {
    }


    public Expression getNameExpression() {
        return nameExpression;
    }

    public void setNameExpression(Expression nameExpression) {
        this.nameExpression = nameExpression;
    }

    public Expression getOwnerExpression() {
        return ownerExpression;
    }

    public void setOwnerExpression(Expression ownerExpression) {
        this.ownerExpression = ownerExpression;
    }

    public Expression getDescriptionExpression() {
        return descriptionExpression;
    }

    public void setDescriptionExpression(Expression descriptionExpression) {
        this.descriptionExpression = descriptionExpression;
    }

    public Expression getAssigneeExpression() {
        return assigneeExpression;
    }

    public void setAssigneeExpression(Expression assigneeExpression) {
        this.assigneeExpression = assigneeExpression;
    }

    public Set<Expression> getCandidateUserIdExpressions() {
        return candidateUserIdExpressions;
    }

    public void addCandidateUserIdExpression(Expression userId) {
        candidateUserIdExpressions.add(userId);
    }

    public Set<Expression> getCandidateGroupIdExpressions() {
        return candidateGroupIdExpressions;
    }

    public void addCandidateGroupIdExpression(Expression groupId) {
        candidateGroupIdExpressions.add(groupId);
    }

    public Expression getPriorityExpression() {
        return priorityExpression;
    }

    public void setPriorityExpression(Expression priorityExpression) {
        this.priorityExpression = priorityExpression;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Expression getDueDateExpression() {
        return dueDateExpression;
    }

    public void setDueDateExpression(Expression dueDateExpression) {
        this.dueDateExpression = dueDateExpression;
    }

    public Expression getCategoryExpression() {
        return categoryExpression;
    }

    public void setCategoryExpression(Expression categoryExpression) {
        this.categoryExpression = categoryExpression;
    }

    public Map<String, List<TaskListener>> getTaskListeners() {
        return taskListeners;
    }

    public void setTaskListeners(Map<String, List<TaskListener>> taskListeners) {
        this.taskListeners = taskListeners;
    }

    public List<TaskListener> getTaskListener(String eventName) {
        return taskListeners.get(eventName);
    }

    public void addTaskListener(String eventName, TaskListener taskListener) {
        if (TaskListener.EVENTNAME_ALL_EVENTS.equals(eventName)) {
            this.addTaskListener(TaskListener.EVENTNAME_CREATE, taskListener);
            this.addTaskListener(TaskListener.EVENTNAME_ASSIGNMENT, taskListener);
            this.addTaskListener(TaskListener.EVENTNAME_COMPLETE, taskListener);
            this.addTaskListener(TaskListener.EVENTNAME_DELETE, taskListener);

        } else {
            List<TaskListener> taskEventListeners = taskListeners.get(eventName);
            if (taskEventListeners == null) {
                taskEventListeners = new ArrayList<TaskListener>();
                taskListeners.put(eventName, taskEventListeners);
            }
            taskEventListeners.add(taskListener);
        }
    }

}

package com.baidu.iit.pxp.model;

import java.io.Serializable;
import java.util.Comparator;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午10:44
 */
public class ErrorEventDefinitionData implements Serializable {

    public static Comparator<ErrorEventDefinitionData> comparator = new Comparator<ErrorEventDefinitionData>() {
        public int compare(ErrorEventDefinitionData o1, ErrorEventDefinitionData o2) {
            return o2.getPrecedence().compareTo(o1.getPrecedence());
        }
    };

    private static final long serialVersionUID = 1L;

    protected final String handlerActivityId;
    protected String errorCode;
    protected Integer precedence =0;

    public ErrorEventDefinitionData(String handlerActivityId) {
        this.handlerActivityId=handlerActivityId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getHandlerActivityId() {
        return handlerActivityId;
    }

    public Integer getPrecedence() {
        return precedence + (errorCode != null ? 1 : 0);
    }

    public void setPrecedence(Integer precedence) {
        this.precedence = precedence;
    }

    public boolean catches(String errorCode) {
        return errorCode == null || this.errorCode == null || this.errorCode.equals(errorCode) ;
    }

}

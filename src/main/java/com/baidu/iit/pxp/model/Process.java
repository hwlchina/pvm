package com.baidu.iit.pxp.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:38
 */
public class Process extends BaseElement implements FlowElementsContainer, HasExecutionListeners {

    protected String name;
    protected boolean executable = true;
    protected String documentation;
    protected List<ActivitiListener> executionListeners = new ArrayList<ActivitiListener>();

    protected List<FlowElement> flowElementList = new ArrayList<FlowElement>();
    protected List<ValuedDataObject> dataObjects = new ArrayList<ValuedDataObject>();
    ;
    protected List<EventListener> eventListeners = new ArrayList<EventListener>();

    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ActivitiListener> getExecutionListeners() {
        return executionListeners;
    }

    public void setExecutionListeners(List<ActivitiListener> executionListeners) {
        this.executionListeners = executionListeners;
    }

    public FlowElement getFlowElement(String flowElementId) {
        return findFlowElementInList(flowElementId);
    }

    public FlowElement getFlowElementRecursive(String flowElementId) {
        return getFlowElementRecursive(this, flowElementId);
    }

    protected FlowElement getFlowElementRecursive(FlowElementsContainer flowElementsContainer, String flowElementId) {
        for (FlowElement flowElement : flowElementsContainer.getFlowElements()) {
            if (flowElement.getId() != null && flowElement.getId().equals(flowElementId)) {
                return flowElement;
            } else if (flowElement instanceof FlowElementsContainer) {
                FlowElement result = getFlowElementRecursive((FlowElementsContainer) flowElement, flowElementId);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }


    protected FlowElement findFlowElementInList(String flowElementId) {
        for (FlowElement f : flowElementList) {
            if (f.getId() != null && f.getId().equals(flowElementId)) {
                return f;
            }
        }
        return null;
    }

    public Collection<FlowElement> getFlowElements() {
        return flowElementList;
    }

    public void addFlowElement(FlowElement element) {
        flowElementList.add(element);
    }

    public void removeFlowElement(String elementId) {
        FlowElement element = findFlowElementInList(elementId);
        if (element != null) {
            flowElementList.remove(element);
        }
    }


    public List<EventListener> getEventListeners() {
        return eventListeners;
    }

    public void setEventListeners(List<EventListener> eventListeners) {
        this.eventListeners = eventListeners;
    }


    public <FlowElementType extends FlowElement> List<FlowElementType> findFlowElementsOfType(Class<FlowElementType> type) {
        return findFlowElementsOfType(type, true);
    }

    public <FlowElementType extends FlowElement> List<FlowElementType> findFlowElementsOfType(Class<FlowElementType> type, boolean goIntoSubprocesses) {
        List<FlowElementType> foundFlowElements = new ArrayList<FlowElementType>();
        for (FlowElement flowElement : this.getFlowElements()) {
            if (type.isInstance(flowElement)) {
                foundFlowElements.add((FlowElementType) flowElement);
            }
            if (flowElement instanceof SubProcess) {
                if (goIntoSubprocesses) {
                    foundFlowElements.addAll(findFlowElementsInSubProcessOfType((SubProcess) flowElement, type));
                }
            }
        }
        return foundFlowElements;
    }

    public <FlowElementType extends FlowElement> List<FlowElementType> findFlowElementsInSubProcessOfType(SubProcess subProcess, Class<FlowElementType> type) {
        return findFlowElementsInSubProcessOfType(subProcess, type, true);
    }

    @SuppressWarnings("unchecked")
    public <FlowElementType extends FlowElement> List<FlowElementType> findFlowElementsInSubProcessOfType(SubProcess subProcess, Class<FlowElementType> type, boolean goIntoSubprocesses) {
        List<FlowElementType> foundFlowElements = new ArrayList<FlowElementType>();
        for (FlowElement flowElement : subProcess.getFlowElements()) {
            if (type.isInstance(flowElement)) {
                foundFlowElements.add((FlowElementType) flowElement);
            }
            if (flowElement instanceof SubProcess) {
                if (goIntoSubprocesses) {
                    foundFlowElements.addAll(findFlowElementsInSubProcessOfType((SubProcess) flowElement, type));
                }
            }
        }
        return foundFlowElements;
    }

    public FlowElementsContainer findParent(FlowElement childElement) {
        return findParent(childElement, this);
    }

    public FlowElementsContainer findParent(FlowElement childElement, FlowElementsContainer flowElementsContainer) {
        for (FlowElement flowElement : flowElementsContainer.getFlowElements()) {
            if (childElement.getId() != null && childElement.getId().equals(flowElement.getId())) {
                return flowElementsContainer;
            }
            if (flowElement instanceof FlowElementsContainer) {
                FlowElementsContainer result = findParent(childElement, (FlowElementsContainer) flowElement);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    public Process clone() {
        Process clone = new Process();
        clone.setValues(this);
        return clone;
    }

    public void setValues(Process otherElement) {
        super.setValues(otherElement);
        setName(otherElement.getName());
        setDocumentation(otherElement.getDocumentation());
        executionListeners = new ArrayList<ActivitiListener>();
        if (otherElement.getExecutionListeners() != null && otherElement.getExecutionListeners().size() > 0) {
            for (ActivitiListener listener : otherElement.getExecutionListeners()) {
                executionListeners.add(listener.clone());
            }
        }


        eventListeners = new ArrayList<EventListener>();
        if (otherElement.getEventListeners() != null && !otherElement.getEventListeners().isEmpty()) {
            for (EventListener listener : otherElement.getEventListeners()) {
                eventListeners.add(listener.clone());
            }
        }

        for (ValuedDataObject thisObject : getDataObjects()) {
            boolean exists = false;
            for (ValuedDataObject otherObject : otherElement.getDataObjects()) {
                if (thisObject.getId().equals(otherObject.getId())) {
                    exists = true;
                }
            }
            if (!exists) {
                removeFlowElement(thisObject.getId());
            }
        }

        dataObjects = new ArrayList<ValuedDataObject>();
        if (otherElement.getDataObjects() != null && otherElement.getDataObjects().size() > 0) {
            for (ValuedDataObject dataObject : otherElement.getDataObjects()) {
                ValuedDataObject clone = dataObject.clone();
                dataObjects.add(clone);
                removeFlowElement(clone.getId());
                addFlowElement(clone);
            }
        }
    }

    public List<ValuedDataObject> getDataObjects() {
        return dataObjects;
    }

    public void setDataObjects(List<ValuedDataObject> dataObjects) {
        this.dataObjects = dataObjects;
    }
}

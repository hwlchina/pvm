package com.baidu.iit.pxp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:13
 */
public abstract class FlowElement extends BaseElement implements HasExecutionListeners {

    protected String name;
    protected String documentation;
    protected List<ActivitiListener> executionListeners = new ArrayList<ActivitiListener>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public List<ActivitiListener> getExecutionListeners() {
        return executionListeners;
    }
    public void setExecutionListeners(List<ActivitiListener> executionListeners) {
        this.executionListeners = executionListeners;
    }

    public abstract FlowElement clone();

    public void setValues(FlowElement otherElement) {
        super.setValues(otherElement);
        setName(otherElement.getName());
        setDocumentation(otherElement.getDocumentation());

        executionListeners = new ArrayList<ActivitiListener>();
        if (otherElement.getExecutionListeners() != null && otherElement.getExecutionListeners().size() > 0) {
            for (ActivitiListener listener : otherElement.getExecutionListeners()) {
                executionListeners.add(listener.clone());
            }
        }
    }
}
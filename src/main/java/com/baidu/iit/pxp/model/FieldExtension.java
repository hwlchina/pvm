package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:16
 */
public class FieldExtension extends BaseElement {

    protected String fieldName;
    protected String stringValue;
    protected String expression;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public FieldExtension clone() {
        FieldExtension clone = new FieldExtension();
        clone.setValues(this);
        return clone;
    }

    public void setValues(FieldExtension otherExtension) {
        setFieldName(otherExtension.getFieldName());
        setStringValue(otherExtension.getStringValue());
        setExpression(otherExtension.getExpression());
    }
}
package com.baidu.iit.pxp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午3:18
 */
public class DataGrid implements ComplexDataType {

    protected List<DataGridRow> rows = new ArrayList<DataGridRow>();

    public List<DataGridRow> getRows() {
        return rows;
    }

    public void setRows(List<DataGridRow> rows) {
        this.rows = rows;
    }

    public DataGrid clone() {
        DataGrid clone = new DataGrid();
        clone.setValues(this);
        return clone;
    }

    public void setValues(DataGrid otherGrid) {
        rows = new ArrayList<DataGridRow>();
        if (otherGrid.getRows() != null && otherGrid.getRows().size() > 0) {
            for (DataGridRow row : otherGrid.getRows()) {
                rows.add(row.clone());
            }
        }
    }
}

package com.baidu.iit.pxp.model;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午7:09
 */
public class BpmnModel {

    protected Map<String, List<ExtensionAttribute>> definitionsAttributes = new LinkedHashMap<String, List<ExtensionAttribute>>();

    protected List<Process> processes = new ArrayList<Process>();

    protected Map<String, String> errorMap = new LinkedHashMap<String, String>();

    protected Map<String, ItemDefinition> itemDefinitionMap = new LinkedHashMap<String, ItemDefinition>();

    protected List<Import> imports = new ArrayList<Import>();

    protected List<Interface> interfaces = new ArrayList<Interface>();

    protected Map<String, String> namespaceMap = new LinkedHashMap<String, String>();

    protected String targetNamespace;

    protected int nextFlowIdCounter = 1;


    public Map<String, List<ExtensionAttribute>> getDefinitionsAttributes() {
        return definitionsAttributes;
    }

    public String getDefinitionsAttributeValue(String namespace, String name) {
        List<ExtensionAttribute> attributes = getDefinitionsAttributes().get(name);
        if (attributes != null && !attributes.isEmpty()) {
            for (ExtensionAttribute attribute : attributes) {
                if (namespace.equals(attribute.getNamespace()))
                    return attribute.getValue();
            }
        }
        return null;
    }

    public void addDefinitionsAttribute(ExtensionAttribute attribute) {
        if (attribute != null && StringUtils.isNotEmpty(attribute.getName())) {
            List<ExtensionAttribute> attributeList = null;
            if (this.definitionsAttributes.containsKey(attribute.getName()) == false) {
                attributeList = new ArrayList<ExtensionAttribute>();
                this.definitionsAttributes.put(attribute.getName(), attributeList);
            }
            this.definitionsAttributes.get(attribute.getName()).add(attribute);
        }
    }

    public void setDefinitionsAttributes(Map<String, List<ExtensionAttribute>> attributes) {
        this.definitionsAttributes = attributes;
    }


    public List<Process> getProcesses() {
        return processes;
    }

    public void addProcess(Process process) {
        processes.add(process);
    }


    public FlowElement getFlowElement(String id) {
        FlowElement foundFlowElement = null;
        for (Process process : processes) {
            foundFlowElement = process.getFlowElement(id);
            if (foundFlowElement != null) {
                break;
            }
        }

        if (foundFlowElement == null) {
            for (Process process : processes) {
                for (FlowElement flowElement : process.findFlowElementsOfType(SubProcess.class)) {
                    foundFlowElement = getFlowElementInSubProcess(id, (SubProcess) flowElement);
                    if (foundFlowElement != null) {
                        break;
                    }
                }
                if (foundFlowElement != null) {
                    break;
                }
            }
        }

        return foundFlowElement;
    }

    protected FlowElement getFlowElementInSubProcess(String id, SubProcess subProcess) {
        FlowElement foundFlowElement = subProcess.getFlowElement(id);
        if (foundFlowElement == null) {
            for (FlowElement flowElement : subProcess.getFlowElements()) {
                if (flowElement instanceof SubProcess) {
                    foundFlowElement = getFlowElementInSubProcess(id, (SubProcess) flowElement);
                    if (foundFlowElement != null) {
                        break;
                    }
                }
            }
        }
        return foundFlowElement;
    }




    public Map<String, String> getErrors() {
        return errorMap;
    }

    public void setErrors(Map<String, String> errorMap) {
        this.errorMap = errorMap;
    }

    public void addError(String errorRef, String errorCode) {
        if (StringUtils.isNotEmpty(errorRef)) {
            errorMap.put(errorRef, errorCode);
        }
    }

    public boolean containsErrorRef(String errorRef) {
        return errorMap.containsKey(errorRef);
    }

    public Map<String, ItemDefinition> getItemDefinitions() {
        return itemDefinitionMap;
    }

    public void setItemDefinitions(Map<String, ItemDefinition> itemDefinitionMap) {
        this.itemDefinitionMap = itemDefinitionMap;
    }

    public void addItemDefinition(String id, ItemDefinition item) {
        if (StringUtils.isNotEmpty(id)) {
            itemDefinitionMap.put(id, item);
        }
    }

    public boolean containsItemDefinitionId(String id) {
        return itemDefinitionMap.containsKey(id);
    }

    public List<Import> getImports() {
        return imports;
    }

    public void setImports(List<Import> imports) {
        this.imports = imports;
    }

    public List<Interface> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(List<Interface> interfaces) {
        this.interfaces = interfaces;
    }


    public void addNamespace(String prefix, String uri) {
        namespaceMap.put(prefix, uri);
    }

    public boolean containsNamespacePrefix(String prefix) {
        return namespaceMap.containsKey(prefix);
    }

    public String getNamespace(String prefix) {
        return namespaceMap.get(prefix);
    }

    public Map<String, String> getNamespaces() {
        return namespaceMap;
    }

    public String getTargetNamespace() {
        return targetNamespace;
    }

    public void setTargetNamespace(String targetNamespace) {
        this.targetNamespace = targetNamespace;
    }

}

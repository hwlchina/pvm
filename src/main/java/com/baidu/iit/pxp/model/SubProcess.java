package com.baidu.iit.pxp.model;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:28
 */
public class SubProcess extends Activity implements FlowElementsContainer {

    protected List<FlowElement> flowElementList = new ArrayList<FlowElement>();
    protected List<ValuedDataObject> dataObjects = new ArrayList<ValuedDataObject>();

    public FlowElement getFlowElement(String id) {
        FlowElement foundElement = null;
        if (StringUtils.isNotEmpty(id)) {
            for (FlowElement element : flowElementList) {
                if (id.equals(element.getId())) {
                    foundElement = element;
                    break;
                }
            }
        }
        return foundElement;
    }

    public Collection<FlowElement> getFlowElements() {
        return flowElementList;
    }

    public void addFlowElement(FlowElement element) {
        flowElementList.add(element);
    }

    public void removeFlowElement(String elementId) {
        FlowElement element = getFlowElement(elementId);
        if (element != null) {
            flowElementList.remove(element);
        }
    }




    public SubProcess clone() {
        SubProcess clone = new SubProcess();
        clone.setValues(this);
        return clone;
    }

    public void setValues(SubProcess otherElement) {
        super.setValues(otherElement);


        for (ValuedDataObject thisObject : getDataObjects()) {
            boolean exists = false;
            for (ValuedDataObject otherObject : otherElement.getDataObjects()) {
                if (thisObject.getId().equals(otherObject.getId())) {
                    exists = true;
                }
            }
            if (!exists) {
                removeFlowElement(thisObject.getId());
            }
        }

        dataObjects = new ArrayList<ValuedDataObject>();
        if (otherElement.getDataObjects() != null && otherElement.getDataObjects().size() > 0) {
            for (ValuedDataObject dataObject : otherElement.getDataObjects()) {
                ValuedDataObject clone = dataObject.clone();
                dataObjects.add(clone);
                removeFlowElement(clone.getId());
                addFlowElement(clone);
            }
        }
    }

    public List<ValuedDataObject> getDataObjects() {
        return dataObjects;
    }

    public void setDataObjects(List<ValuedDataObject> dataObjects) {
        this.dataObjects = dataObjects;
    }
}

package com.baidu.iit.pxp.model;

import com.baidu.iit.pvm.delegate.DelegateExecution;
import com.baidu.iit.pvm.delegate.VariableScope;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午10:50
 */
public interface DelegateTask extends VariableScope {

    String getId();

    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);


    String getProcessInstanceId();

    String getExecutionId();

    String getProcessDefinitionId();

    String getTaskDefinitionKey();

    DelegateExecution getExecution();

    String getEventName();

    String getCategory();

    void setCategory(String category);

}

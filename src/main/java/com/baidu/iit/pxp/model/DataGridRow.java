package com.baidu.iit.pxp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午3:19
 */
public class DataGridRow {

    protected int index;
    protected List<DataGridField> fields = new ArrayList<DataGridField>();

    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
    public List<DataGridField> getFields() {
        return fields;
    }
    public void setFields(List<DataGridField> fields) {
        this.fields = fields;
    }

    public DataGridRow clone() {
        DataGridRow clone = new DataGridRow();
        clone.setValues(this);
        return clone;
    }

    public void setValues(DataGridRow otherRow) {
        setIndex(otherRow.getIndex());

        fields = new ArrayList<DataGridField>();
        if (otherRow.getFields() != null && otherRow.getFields().size() > 0) {
            for (DataGridField field : otherRow.getFields()) {
                fields.add(field.clone());
            }
        }
    }
}


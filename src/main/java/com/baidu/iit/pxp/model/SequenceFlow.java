package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:22
 */
public class SequenceFlow extends FlowElement {

    protected String conditionExpression;
    protected String sourceRef;
    protected String targetRef;

    public SequenceFlow() {

    }

    public SequenceFlow(String sourceRef, String targetRef) {
        this.sourceRef = sourceRef;
        this.targetRef = targetRef;
    }

    public String getConditionExpression() {
        return conditionExpression;
    }

    public void setConditionExpression(String conditionExpression) {
        this.conditionExpression = conditionExpression;
    }

    public String getSourceRef() {
        return sourceRef;
    }

    public void setSourceRef(String sourceRef) {
        this.sourceRef = sourceRef;
    }

    public String getTargetRef() {
        return targetRef;
    }

    public void setTargetRef(String targetRef) {
        this.targetRef = targetRef;
    }

    public String toString() {
        return sourceRef + " --> " + targetRef;
    }

    public SequenceFlow clone() {
        SequenceFlow clone = new SequenceFlow();
        clone.setValues(this);
        return clone;
    }

    public void setValues(SequenceFlow otherFlow) {
        super.setValues(otherFlow);
        setConditionExpression(otherFlow.getConditionExpression());
        setSourceRef(otherFlow.getSourceRef());
        setTargetRef(otherFlow.getTargetRef());
    }
}

package com.baidu.iit.pxp.model;

import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:17
 */
public interface HasExecutionListeners {

    List<ActivitiListener> getExecutionListeners();

    void setExecutionListeners(List<ActivitiListener> executionListeners);
}
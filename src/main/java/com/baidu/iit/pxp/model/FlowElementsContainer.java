package com.baidu.iit.pxp.model;

import java.util.Collection;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:29
 */
public interface FlowElementsContainer {

    FlowElement getFlowElement(String id);
    Collection<FlowElement> getFlowElements();
    void addFlowElement(FlowElement element);
    void removeFlowElement(String elementId);
}

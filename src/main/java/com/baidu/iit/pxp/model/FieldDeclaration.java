package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午1:50
 */
public class FieldDeclaration {

    protected String name;
    protected String type;
    protected Object value;

    public FieldDeclaration(String name, String type, Object value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public FieldDeclaration() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午10:28
 */
public enum AssociationDirection {
    NONE("None"),
    ONE("One"),
    BOTH("Both");

    String value;

    AssociationDirection(final String value)
    {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

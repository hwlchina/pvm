package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:46
 */
public class CancelEventDefinition extends EventDefinition {

    public CancelEventDefinition clone() {
        CancelEventDefinition clone = new CancelEventDefinition();
        clone.setValues(this);
        return clone;
    }

    public void setValues(CancelEventDefinition otherDefinition) {
        super.setValues(otherDefinition);
    }
}

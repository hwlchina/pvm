package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:24
 */
public abstract class EventDefinition extends BaseElement {

    public abstract EventDefinition clone();
}

package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:42
 */
public class InclusiveGateway extends Gateway {

    public InclusiveGateway clone() {
        InclusiveGateway clone = new InclusiveGateway();
        clone.setValues(this);
        return clone;
    }

    public void setValues(InclusiveGateway otherElement) {
        super.setValues(otherElement);
    }
}

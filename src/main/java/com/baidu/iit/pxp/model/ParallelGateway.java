package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:41
 */
public class ParallelGateway extends Gateway {

    public ParallelGateway clone() {
        ParallelGateway clone = new ParallelGateway();
        clone.setValues(this);
        return clone;
    }

    public void setValues(ParallelGateway otherElement) {
        super.setValues(otherElement);
    }
}
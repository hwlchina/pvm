package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午2:51
 */
public class Signal extends BaseElement {

    public static final String SCOPE_GLOBAL = "global";
    public static final String SCOPE_PROCESS_INSTANCE = "processInstance";

    protected String name;

    protected String scope;

    public Signal() {
    }

    public Signal(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Signal clone() {
        Signal clone = new Signal();
        clone.setValues(this);
        return clone;
    }

    public void setValues(Signal otherElement) {
        super.setValues(otherElement);
        setName(otherElement.getName());
        setScope(otherElement.getScope());
    }
}

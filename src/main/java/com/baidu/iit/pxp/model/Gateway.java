package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午12:37
 */
public abstract class Gateway extends FlowNode {

    protected String defaultFlow;

    public String getDefaultFlow() {
        return defaultFlow;
    }

    public void setDefaultFlow(String defaultFlow) {
        this.defaultFlow = defaultFlow;
    }

    public abstract Gateway clone();

    public void setValues(Gateway otherElement) {
        super.setValues(otherElement);
        setDefaultFlow(otherElement.getDefaultFlow());
    }
}
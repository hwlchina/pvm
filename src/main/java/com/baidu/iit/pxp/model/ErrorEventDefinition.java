package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:46
 */
public class ErrorEventDefinition extends EventDefinition {

    protected String errorCode;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorEventDefinition clone() {
        ErrorEventDefinition clone = new ErrorEventDefinition();
        clone.setValues(this);
        return clone;
    }

    public void setValues(ErrorEventDefinition otherDefinition) {
        super.setValues(otherDefinition);
        setErrorCode(otherDefinition.getErrorCode());
    }
}
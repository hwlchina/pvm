package com.baidu.iit.pxp.model;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午2:50
 */
public class GraphicInfo {

    protected double x;
    protected double y;
    protected double height;
    protected double width;
    protected FlowElement element;
    protected Boolean expanded;
    protected int xmlRowNumber;
    protected int xmlColumnNumber;
    public double getX() {
        return x;
    }
    public void setX(double x) {
        this.x = x;
    }
    public double getY() {
        return y;
    }
    public void setY(double y) {
        this.y = y;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public Boolean getExpanded() {
        return expanded;
    }
    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }
    public FlowElement getElement() {
        return element;
    }
    public void setElement(FlowElement element) {
        this.element = element;
    }
    public int getXmlRowNumber() {
        return xmlRowNumber;
    }
    public void setXmlRowNumber(int xmlRowNumber) {
        this.xmlRowNumber = xmlRowNumber;
    }
    public int getXmlColumnNumber() {
        return xmlColumnNumber;
    }
    public void setXmlColumnNumber(int xmlColumnNumber) {
        this.xmlColumnNumber = xmlColumnNumber;
    }
}

package com.baidu.iit.pxp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:21
 */public abstract class FlowNode extends FlowElement {

    protected List<SequenceFlow> incomingFlows = new ArrayList<SequenceFlow>();
    protected List<SequenceFlow> outgoingFlows = new ArrayList<SequenceFlow>();

    public List<SequenceFlow> getIncomingFlows() {
        return incomingFlows;
    }

    public void setIncomingFlows(List<SequenceFlow> incomingFlows) {
        this.incomingFlows = incomingFlows;
    }

    public List<SequenceFlow> getOutgoingFlows() {
        return outgoingFlows;
    }

    public void setOutgoingFlows(List<SequenceFlow> outgoingFlows) {
        this.outgoingFlows = outgoingFlows;
    }

    public void setValues(FlowNode otherNode) {
        super.setValues(otherNode);
    }
}


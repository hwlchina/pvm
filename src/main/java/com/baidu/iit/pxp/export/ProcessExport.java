package com.baidu.iit.pxp.export;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.ExtensionAttribute;

import java.util.Arrays;
import java.util.List;


/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午2:26
 */
public class ProcessExport {

    public static final List<ExtensionAttribute> defaultProcessAttributes = Arrays.asList(
            new ExtensionAttribute(BpmnXMLConstants.ATTRIBUTE_ID),
            new ExtensionAttribute(BpmnXMLConstants.ATTRIBUTE_NAME),
            new ExtensionAttribute(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_PROCESS_CANDIDATE_USERS),
            new ExtensionAttribute(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_PROCESS_CANDIDATE_GROUPS)
    );

}

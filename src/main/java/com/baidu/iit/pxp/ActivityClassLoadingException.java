package com.baidu.iit.pxp;

import com.baidu.iit.pvm.ActivityException;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:45
 */
public class ActivityClassLoadingException extends ActivityException {

    private static final long serialVersionUID = 1L;
    protected String className;

    public ActivityClassLoadingException(String className, Throwable cause) {
        super(getExceptionMessageMessage(className, cause), cause);
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    private static String getExceptionMessageMessage(String className, Throwable cause) {
        if(cause instanceof ClassNotFoundException) {
            return "Class not found: " + className;
        } else {
            return "Could not load class: " + className;
        }
    }

}

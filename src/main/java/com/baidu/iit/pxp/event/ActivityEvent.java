package com.baidu.iit.pxp.event;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 上午11:41
 */
public interface ActivityEvent {

    ActivityEventType getType();

    String getExecutionId();

    String getProcessInstanceId();

    String getProcessDefinitionId();

}

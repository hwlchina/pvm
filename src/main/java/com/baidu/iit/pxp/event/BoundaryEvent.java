package com.baidu.iit.pxp.event;

import com.baidu.iit.pxp.model.Activity;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:25
 */
public class BoundaryEvent extends Event {

    protected Activity attachedToRef;
    protected String attachedToRefId;
    protected boolean cancelActivity = true;

    public Activity getAttachedToRef() {
        return attachedToRef;
    }
    public void setAttachedToRef(Activity attachedToRef) {
        this.attachedToRef = attachedToRef;
    }
    public String getAttachedToRefId() {
        return attachedToRefId;
    }
    public void setAttachedToRefId(String attachedToRefId) {
        this.attachedToRefId = attachedToRefId;
    }
    public boolean isCancelActivity() {
        return cancelActivity;
    }
    public void setCancelActivity(boolean cancelActivity) {
        this.cancelActivity = cancelActivity;
    }

    public BoundaryEvent clone() {
        BoundaryEvent clone = new BoundaryEvent();
        clone.setValues(this);
        return clone;
    }

    public void setValues(BoundaryEvent otherEvent) {
        super.setValues(otherEvent);
        setAttachedToRefId(otherEvent.getAttachedToRefId());
        setAttachedToRef(otherEvent.getAttachedToRef());
        setCancelActivity(otherEvent.isCancelActivity());
    }
}


package com.baidu.iit.pxp.event;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:08
 */
public class StartEvent extends Event {

    protected String initiator;
    protected String formKey;

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public StartEvent clone() {
        StartEvent clone = new StartEvent();
        clone.setValues(this);
        return clone;
    }

    public void setValues(StartEvent otherEvent) {
        super.setValues(otherEvent);
        setInitiator(otherEvent.getInitiator());
        setFormKey(otherEvent.getFormKey());
    }
}

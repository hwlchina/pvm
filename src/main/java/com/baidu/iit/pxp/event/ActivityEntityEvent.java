package com.baidu.iit.pxp.event;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午4:01
 */

public interface ActivityEntityEvent extends ActivityEvent {

    Object getEntity();
}

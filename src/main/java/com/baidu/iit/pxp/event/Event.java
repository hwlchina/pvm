package com.baidu.iit.pxp.event;

import com.baidu.iit.pxp.model.EventDefinition;
import com.baidu.iit.pxp.model.FlowNode;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午10:22
 */
public abstract class Event extends FlowNode {

    protected List<EventDefinition> eventDefinitions = new ArrayList<EventDefinition>();

    public List<EventDefinition> getEventDefinitions() {
        return eventDefinitions;
    }

    public void setEventDefinitions(List<EventDefinition> eventDefinitions) {
        this.eventDefinitions = eventDefinitions;
    }

    public void addEventDefinition(EventDefinition eventDefinition) {
        eventDefinitions.add(eventDefinition);
    }

    public void setValues(Event otherEvent) {
        super.setValues(otherEvent);

        eventDefinitions = new ArrayList<EventDefinition>();
        if (otherEvent.getEventDefinitions() != null && otherEvent.getEventDefinitions().size() > 0) {
            for (EventDefinition eventDef : otherEvent.getEventDefinitions()) {
                eventDefinitions.add(eventDef.clone());
            }
        }
    }
}

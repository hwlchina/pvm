package com.baidu.iit.pxp.event;

import com.baidu.iit.pvm.ActivityIllegalArgumentException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 上午11:42
 */
public enum ActivityEventType {

    ENTITY_CREATED,


    ENTITY_INITIALIZED,

    ENTITY_UPDATED,


    ENTITY_DELETED,

    ENTITY_SUSPENDED,


    ENTITY_ACTIVATED,

    TIMER_FIRED,

    JOB_EXECUTION_SUCCESS,

    JOB_EXECUTION_FAILURE,


    JOB_RETRIES_DECREMENTED,

    CUSTOM,

    ENGINE_CREATED,


    ENGINE_CLOSED,


    ACTIVITY_STARTED,


    ACTIVITY_COMPLETED,


    ACTIVITY_SIGNALED,


    ACTIVITY_COMPENSATE,


    ACTIVITY_MESSAGE_RECEIVED,

    ACTIVITY_ERROR_RECEIVED,

    UNCAUGHT_BPMN_ERROR,

    VARIABLE_CREATED,

    VARIABLE_UPDATED,


    VARIABLE_DELETED,


    TASK_ASSIGNED,


    TASK_COMPLETED,


    MEMBERSHIP_CREATED,


    MEMBERSHIP_DELETED,

    MEMBERSHIPS_DELETED;

    public static final ActivityEventType[] EMPTY_ARRAY = new ActivityEventType[]{};

    public static ActivityEventType[] getTypesFromString(String string) {
        List<ActivityEventType> result = new ArrayList<ActivityEventType>();
        if (string != null && !string.isEmpty()) {
            String[] split = StringUtils.split(string, ",");
            for (String typeName : split) {
                boolean found = false;
                for (ActivityEventType type : values()) {
                    if (typeName.equals(type.name())) {
                        result.add(type);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    throw new ActivityIllegalArgumentException("Invalid event-type: " + typeName);
                }
            }
        }

        return result.toArray(EMPTY_ARRAY);
    }
}

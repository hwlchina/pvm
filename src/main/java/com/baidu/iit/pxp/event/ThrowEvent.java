package com.baidu.iit.pxp.event;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:44
 */
public class ThrowEvent extends Event {

    public ThrowEvent clone() {
        ThrowEvent clone = new ThrowEvent();
        clone.setValues(this);
        return clone;
    }

    public void setValues(ThrowEvent otherEvent) {
        super.setValues(otherEvent);
    }
}

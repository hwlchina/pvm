package com.baidu.iit.pxp.event;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:14
 */
public class EndEvent extends Event {

    public EndEvent clone() {
        EndEvent clone = new EndEvent();
        clone.setValues(this);
        return clone;
    }

    public void setValues(EndEvent otherEvent) {
        super.setValues(otherEvent);
    }
}

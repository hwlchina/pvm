package com.baidu.iit.pxp.variable;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:05
 */
public interface VariableType {

    String getTypeName();

    boolean isCachable();

    void setValue(Object value, ValueFields valueFields);

    Object getValue(ValueFields valueFields);

}

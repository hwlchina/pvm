package com.baidu.iit.pxp.variable;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:00
 */
public interface ValueFields {

    String getName();

    String getTextValue();

    void setTextValue(String textValue);

    String getTextValue2();

    void setTextValue2(String textValue2);

    Long getLongValue();

    void setLongValue(Long longValue);

    Double getDoubleValue();

    void setDoubleValue(Double doubleValue);

    byte[] getBytes();

    void setBytes(byte[] bytes);


    Object getCachedValue();

    void setCachedValue(Object cachedValue);

}

package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.delegate.ExecutionListener;
import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pvm.process.ScopeImpl;
import com.baidu.iit.pvm.process.TransitionImpl;
import com.baidu.iit.pxp.constants.ImplementationTypeConstants;
import com.baidu.iit.pxp.model.*;
import com.baidu.iit.pxp.parser.BpmnParse;
import com.baidu.iit.pxp.parser.BpmnParseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 上午11:32
 */
public abstract class AbstractBpmnParseHandler<T extends BaseElement> implements BpmnParseHandler {


    private static Logger logger = LoggerFactory.getLogger(AbstractBpmnParseHandler.class);

    public static final String PROPERTYNAME_IS_FOR_COMPENSATION = "isForCompensation";

    public static final String PROPERTYNAME_EVENT_SUBSCRIPTION_DECLARATION = "eventDefinitions";

    public static final String PROPERTYNAME_ERROR_EVENT_DEFINITIONS = "errorEventDefinitions";

    public static final String PROPERTYNAME_TIMER_DECLARATION = "timerDeclarations";

    public Set<Class<? extends BaseElement>> getHandledTypes() {
        Set<Class<? extends BaseElement>> types = new HashSet<Class<? extends BaseElement>>();
        types.add(getHandledType());
        return types;
    }

    protected Class<? extends BaseElement> getHandledType() {
        return null;
    }

    @SuppressWarnings("unchecked")
    public void parse(BpmnParse bpmnParse, BaseElement element) {
        T baseElement = (T) element;
        executeParse(bpmnParse, baseElement);
    }

    protected abstract void executeParse(BpmnParse bpmnParse, T element);

    protected ActivityImpl findActivity(BpmnParse bpmnParse, String id) {
        return bpmnParse.getCurrentScope().findActivity(id);
    }

    public ActivityImpl createActivityOnCurrentScope(BpmnParse bpmnParse, FlowElement flowElement, String xmlLocalName) {
        return createActivityOnScope(bpmnParse, flowElement, xmlLocalName, bpmnParse.getCurrentScope());
    }

    public ActivityImpl createActivityOnScope(BpmnParse bpmnParse, FlowElement flowElement, String xmlLocalName, ScopeImpl scopeElement) {
        if (logger.isDebugEnabled()) {
            logger.debug("Parsing activity {}", flowElement.getId());
        }

        ActivityImpl activity = scopeElement.createActivity(flowElement.getId());
        bpmnParse.setCurrentActivity(activity);

        activity.setProperty("name", flowElement.getName());
        activity.setProperty("documentation", flowElement.getDocumentation());
        if (flowElement instanceof Activity) {
            Activity modelActivity = (Activity) flowElement;
            activity.setProperty("default", modelActivity.getDefaultFlow());
            if (modelActivity.isForCompensation()) {
                activity.setProperty(PROPERTYNAME_IS_FOR_COMPENSATION, true);
            }
        } else if (flowElement instanceof Gateway) {
            activity.setProperty("default", ((Gateway) flowElement).getDefaultFlow());
        }
        activity.setProperty("type", xmlLocalName);

        return activity;
    }

    protected void createExecutionListenersOnScope(BpmnParse bpmnParse, List<ActivitiListener> activitiListenerList, ScopeImpl scope) {
        for (ActivitiListener activitiListener : activitiListenerList) {
            scope.addExecutionListener(activitiListener.getEvent(), createExecutionListener(bpmnParse, activitiListener));
        }
    }

    protected void createExecutionListenersOnTransition(BpmnParse bpmnParse, List<ActivitiListener> activitiListenerList, TransitionImpl transition) {
        for (ActivitiListener activitiListener : activitiListenerList) {
            transition.addExecutionListener(createExecutionListener(bpmnParse, activitiListener));
        }
    }

    protected ExecutionListener createExecutionListener(BpmnParse bpmnParse, ActivitiListener activitiListener) {
        ExecutionListener executionListener = null;

        if (ImplementationTypeConstants.IMPLEMENTATION_TYPE_CLASS.equalsIgnoreCase(activitiListener.getImplementationType())) {
            executionListener = bpmnParse.getListenerFactory().createClassDelegateExecutionListener(activitiListener);
        } else if (ImplementationTypeConstants.IMPLEMENTATION_TYPE_EXPRESSION.equalsIgnoreCase(activitiListener.getImplementationType())) {
            executionListener = bpmnParse.getListenerFactory().createExpressionExecutionListener(activitiListener);
        } else if (ImplementationTypeConstants.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION.equalsIgnoreCase(activitiListener.getImplementationType())) {
            executionListener = bpmnParse.getListenerFactory().createDelegateExpressionExecutionListener(activitiListener);
        }
        return executionListener;
    }


    protected Map<String, Object> processDataObjects(BpmnParse bpmnParse, Collection<ValuedDataObject> dataObjects, ScopeImpl scope) {
        Map<String, Object> variablesMap = new HashMap<String, Object>();
        if (dataObjects != null) {
            for (ValuedDataObject dataObject : dataObjects) {
                variablesMap.put(dataObject.getName(), dataObject.getValue());
            }
        }
        return variablesMap;
    }
}

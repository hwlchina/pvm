package com.baidu.iit.pxp.handler;

import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.EventSubProcess;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午12:00
 */
public class EventSubProcessParseHandler extends SubProcessParseHandler {

    protected Class< ? extends BaseElement> getHandledType() {
        return EventSubProcess.class;
    }

}

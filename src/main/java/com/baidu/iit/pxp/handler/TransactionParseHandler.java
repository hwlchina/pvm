package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.Transaction;
import com.baidu.iit.pxp.parser.BpmnParse;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午12:02
 */
public class TransactionParseHandler extends AbstractActivityBpmnParseHandler<Transaction> {

    public Class< ? extends BaseElement> getHandledType() {
        return Transaction.class;
    }

    protected void executeParse(BpmnParse bpmnParse, Transaction transaction) {

        ActivityImpl activity = createActivityOnCurrentScope(bpmnParse, transaction, BpmnXMLConstants.ELEMENT_TRANSACTION);

        activity.setAsync(transaction.isAsynchronous());
        activity.setExclusive(!transaction.isNotExclusive());

        activity.setScope(true);
        activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createTransactionActivityBehavior(transaction));
        bpmnParse.setCurrentScope(activity);
        bpmnParse.processFlowElements(transaction.getFlowElements());
        bpmnParse.removeCurrentScope();
    }

}

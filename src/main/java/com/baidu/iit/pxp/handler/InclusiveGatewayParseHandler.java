package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.InclusiveGateway;
import com.baidu.iit.pxp.parser.BpmnParse;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午11:08
 */
public class InclusiveGatewayParseHandler extends AbstractActivityBpmnParseHandler<InclusiveGateway> {

    public Class< ? extends BaseElement> getHandledType() {
        return InclusiveGateway.class;
    }

    protected void executeParse(BpmnParse bpmnParse, InclusiveGateway gateway) {
        ActivityImpl activity = createActivityOnCurrentScope(bpmnParse, gateway, BpmnXMLConstants.ELEMENT_GATEWAY_INCLUSIVE);
        activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createInclusiveGatewayActivityBehavior(gateway));
    }

}

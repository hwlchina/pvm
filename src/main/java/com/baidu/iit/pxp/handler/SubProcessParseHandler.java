package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.EventSubProcess;
import com.baidu.iit.pxp.model.SubProcess;
import com.baidu.iit.pxp.parser.BpmnParse;

import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午12:54
 */
public class SubProcessParseHandler extends AbstractActivityBpmnParseHandler<SubProcess> {

    protected Class<? extends BaseElement> getHandledType() {
        return SubProcess.class;
    }

    protected void executeParse(BpmnParse bpmnParse, SubProcess subProcess) {

        ActivityImpl activity = createActivityOnScope(bpmnParse, subProcess, BpmnXMLConstants.ELEMENT_SUBPROCESS, bpmnParse.getCurrentScope());

        activity.setAsync(subProcess.isAsynchronous());
        activity.setExclusive(!subProcess.isNotExclusive());

        boolean triggeredByEvent = false;
        if (subProcess instanceof EventSubProcess) {
            triggeredByEvent = true;
        }

        activity.setProperty("triggeredByEvent", triggeredByEvent);

        activity.setScope(!triggeredByEvent);
        activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createSubprocActivityBehavior(subProcess));

        bpmnParse.setCurrentScope(activity);
        bpmnParse.setCurrentSubProcess(subProcess);

        bpmnParse.processFlowElements(subProcess.getFlowElements());

        if (subProcess instanceof SubProcess) {
            Map<String, Object> variables = processDataObjects(bpmnParse, subProcess.getDataObjects(), activity);
            activity.setVariables(variables);
        }

        bpmnParse.removeCurrentScope();
        bpmnParse.removeCurrentSubProcess();
    }

}

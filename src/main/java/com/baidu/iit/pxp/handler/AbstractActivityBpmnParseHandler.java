package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.ActivityException;
import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.behavior.AbstractBpmnActivityBehavior;
import com.baidu.iit.pxp.behavior.MultiInstanceActivityBehavior;
import com.baidu.iit.pxp.el.ExpressionManager;
import com.baidu.iit.pxp.model.*;
import com.baidu.iit.pxp.parser.BpmnParse;
import org.apache.commons.lang3.StringUtils;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 上午11:30
 */
public abstract class AbstractActivityBpmnParseHandler<T extends FlowNode> extends AbstractFlowNodeBpmnParseHandler<T> {

    @Override
    public void parse(BpmnParse bpmnParse, BaseElement element) {
        super.parse(bpmnParse, element);

        if (element instanceof Activity
                && ((Activity) element).getLoopCharacteristics() != null) {
            createMultiInstanceLoopCharacteristics(bpmnParse, (Activity) element);
        }
    }

    protected void createMultiInstanceLoopCharacteristics(BpmnParse bpmnParse, Activity modelActivity) {

        MultiInstanceLoopCharacteristics loopCharacteristics = modelActivity.getLoopCharacteristics();

        // Activity Behavior
        MultiInstanceActivityBehavior miActivityBehavior = null;
        ActivityImpl activity = bpmnParse.getCurrentScope().findActivity(modelActivity.getId());
        if (activity == null) {
            throw new ActivityException("Activity " + modelActivity.getId() + " needed for multi instance cannot bv found");
        }

        if (loopCharacteristics.isSequential()) {
            miActivityBehavior = bpmnParse.getActivityBehaviorFactory().createSequentialMultiInstanceBehavior(
                    activity, (AbstractBpmnActivityBehavior) activity.getActivityBehavior());
        } else {
            miActivityBehavior = bpmnParse.getActivityBehaviorFactory().createParallelMultiInstanceBehavior(
                    activity, (AbstractBpmnActivityBehavior) activity.getActivityBehavior());
        }

        // ActivityImpl settings
        activity.setScope(true);
        activity.setProperty("multiInstance", loopCharacteristics.isSequential() ? "sequential" : "parallel");
        activity.setActivityBehavior(miActivityBehavior);

        ExpressionManager expressionManager = bpmnParse.getExpressionManager();
        BpmnModel bpmnModel = bpmnParse.getBpmnModel();

        // loopcardinality
        if (StringUtils.isNotEmpty(loopCharacteristics.getLoopCardinality())) {
            miActivityBehavior.setLoopCardinalityExpression(expressionManager.createExpression(loopCharacteristics.getLoopCardinality()));
        }

        // completion condition
        if (StringUtils.isNotEmpty(loopCharacteristics.getCompletionCondition())) {
            miActivityBehavior.setCompletionConditionExpression(expressionManager.createExpression(loopCharacteristics.getCompletionCondition()));
        }

        // activiti:collection
        if (StringUtils.isNotEmpty(loopCharacteristics.getInputDataItem())) {
            if (loopCharacteristics.getInputDataItem().contains("{")) {
                miActivityBehavior.setCollectionExpression(expressionManager.createExpression(loopCharacteristics.getInputDataItem()));
            } else {
                miActivityBehavior.setCollectionVariable(loopCharacteristics.getInputDataItem());
            }
        }

        // activiti:elementVariable
        if (StringUtils.isNotEmpty(loopCharacteristics.getElementVariable())) {
            miActivityBehavior.setCollectionElementVariable(loopCharacteristics.getElementVariable());
        }

        // activiti:elementIndexVariable
        if (StringUtils.isNotEmpty(loopCharacteristics.getElementIndexVariable())) {
            miActivityBehavior.setCollectionElementIndexVariable(loopCharacteristics.getElementIndexVariable());
        }

    }

}
package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.constants.ImplementationTypeConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.ServiceTask;
import com.baidu.iit.pxp.parser.BpmnParse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午5:29
 */
public class ServiceTaskParseHandler extends AbstractActivityBpmnParseHandler<ServiceTask> {

    private static Logger logger = LoggerFactory.getLogger(ServiceTaskParseHandler.class);

    public Class< ? extends BaseElement> getHandledType() {
        return ServiceTask.class;
    }

    protected void executeParse(BpmnParse bpmnParse, ServiceTask serviceTask) {

        ActivityImpl activity = createActivityOnCurrentScope(bpmnParse, serviceTask, BpmnXMLConstants.ELEMENT_TASK_SERVICE);
        activity.setAsync(serviceTask.isAsynchronous());
        activity.setExclusive(!serviceTask.isNotExclusive());
        if (ImplementationTypeConstants.IMPLEMENTATION_TYPE_CLASS.equalsIgnoreCase(serviceTask.getImplementationType())) {
            activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createClassDelegateServiceTask(serviceTask));
        } else if (ImplementationTypeConstants.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION.equalsIgnoreCase(serviceTask.getImplementationType())) {
            activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createServiceTaskDelegateExpressionActivityBehavior(serviceTask));
        } else if (ImplementationTypeConstants.IMPLEMENTATION_TYPE_EXPRESSION.equalsIgnoreCase(serviceTask.getImplementationType())) {
            activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createServiceTaskExpressionActivityBehavior(serviceTask));
        } else {
            logger.warn("One of the attributes 'class', 'delegateExpression', 'type', 'operation', or 'expression' is mandatory on serviceTask " + serviceTask.getId());
        }

    }
}


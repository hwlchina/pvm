package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.Task;
import com.baidu.iit.pxp.parser.BpmnParse;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 上午11:21
 */
public class TaskParseHandler extends AbstractActivityBpmnParseHandler<Task> {

    public Class< ? extends BaseElement> getHandledType() {
        return Task.class;
    }

    protected void executeParse(BpmnParse bpmnParse, Task task) {
        ActivityImpl activity = createActivityOnCurrentScope(bpmnParse, task, BpmnXMLConstants.ELEMENT_TASK);
        activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createTaskActivityBehavior(task));

        activity.setAsync(task.isAsynchronous());
        activity.setExclusive(!task.isNotExclusive());
    }

}

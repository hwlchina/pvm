package com.baidu.iit.pxp.handler;

import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.FlowNode;
import com.baidu.iit.pxp.parser.BpmnParse;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 上午11:31
 */
public abstract class AbstractFlowNodeBpmnParseHandler<T extends FlowNode> extends AbstractBpmnParseHandler<T> {

    @Override
    public void parse(BpmnParse bpmnParse, BaseElement element) {
        super.parse(bpmnParse, element);
        createExecutionListenersOnScope(bpmnParse, ((FlowNode) element).getExecutionListeners(), findActivity(bpmnParse, element.getId()));
    }


}

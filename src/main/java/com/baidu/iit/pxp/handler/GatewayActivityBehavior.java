package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pxp.behavior.FlowNodeActivityBehavior;
import com.baidu.iit.pxp.entity.ExecutionEntity;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午10:58
 */
public abstract class GatewayActivityBehavior extends FlowNodeActivityBehavior {

    protected void lockConcurrentRoot(ActivityExecution execution) {
        ActivityExecution concurrentRoot = null;
        if (execution.isConcurrent()) {
            concurrentRoot = execution.getParent();
        } else {
            concurrentRoot = execution;
        }
        ((ExecutionEntity) concurrentRoot).forceUpdate();
    }

}

package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.ExclusiveGateway;
import com.baidu.iit.pxp.parser.BpmnParse;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午11:05
 */
public class ExclusiveGatewayParseHandler extends AbstractActivityBpmnParseHandler<ExclusiveGateway> {

    public Class< ? extends BaseElement> getHandledType() {
        return ExclusiveGateway.class;
    }

    protected void executeParse(BpmnParse bpmnParse, ExclusiveGateway gateway) {
        ActivityImpl activity = createActivityOnCurrentScope(bpmnParse, gateway, BpmnXMLConstants.ELEMENT_GATEWAY_EXCLUSIVE);
        activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createExclusiveGatewayActivityBehavior(gateway));
    }

}

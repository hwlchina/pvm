package com.baidu.iit.pxp.handler;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.ParallelGateway;
import com.baidu.iit.pxp.parser.BpmnParse;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午10:57
 */
public class ParallelGatewayParseHandler extends AbstractActivityBpmnParseHandler<ParallelGateway> {

    public Class< ? extends BaseElement> getHandledType() {
        return ParallelGateway.class;
    }

    protected void executeParse(BpmnParse bpmnParse, ParallelGateway gateway) {
        ActivityImpl activity = createActivityOnCurrentScope(bpmnParse, gateway, BpmnXMLConstants.ELEMENT_GATEWAY_PARALLEL);
        activity.setActivityBehavior(bpmnParse.getActivityBehaviorFactory().createParallelGatewayActivityBehavior(gateway));
    }

}

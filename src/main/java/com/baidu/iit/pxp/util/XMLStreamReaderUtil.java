package com.baidu.iit.pxp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午9:53
 */
public class XMLStreamReaderUtil {
    protected static final Logger LOGGER = LoggerFactory.getLogger(XMLStreamReaderUtil.class);

    public static String moveDown(XMLStreamReader xtr) {
        try {
            while (xtr.hasNext()) {
                int event = xtr.next();
                switch (event) {
                    case XMLStreamConstants.END_DOCUMENT:
                        return null;
                    case XMLStreamConstants.START_ELEMENT:
                        return xtr.getLocalName();
                    case XMLStreamConstants.END_ELEMENT:
                        return null;
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Error while moving down in XML document", e);
        }
        return null;
    }

}
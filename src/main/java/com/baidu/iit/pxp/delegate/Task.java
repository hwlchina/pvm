package com.baidu.iit.pxp.delegate;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午1:22
 */
public interface Task {

    int DEFAULT_PRIORITY = 50;

    String getId();

    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    String getProcessInstanceId();

    String getExecutionId();

    String getProcessDefinitionId();

    String getTaskDefinitionKey();


    String getCategory();

    void setCategory(String category);


    void setParentTaskId(String parentTaskId);

    String getParentTaskId();

}

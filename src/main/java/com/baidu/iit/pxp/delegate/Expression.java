package com.baidu.iit.pxp.delegate;

import com.baidu.iit.pvm.delegate.VariableScope;

import java.io.Serializable;

/**
 *
 * 表达式描述
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:40
 */
public interface Expression extends Serializable {

    Object getValue(VariableScope variableScope);

    void setValue(Object value, VariableScope variableScope);

    String getExpressionText();

}

package com.baidu.iit.pxp.delegate;

import com.baidu.iit.pvm.delegate.DelegateExecution;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午3:38
 */
public interface JavaDelegate {

    void execute(DelegateExecution execution) throws Exception;

}

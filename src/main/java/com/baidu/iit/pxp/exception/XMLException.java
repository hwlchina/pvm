package com.baidu.iit.pxp.exception;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午12:45
 */
public class XMLException extends RuntimeException {

    public XMLException(String message) {
        super(message);
    }

    public XMLException(String message, Throwable t) {
        super(message, t);
    }
}

package com.baidu.iit.pxp.constants;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:50
 */
public final class ImplementationTypeConstants {
    public final static String IMPLEMENTATION_TYPE_CLASS = "class";
    public final static String IMPLEMENTATION_TYPE_EXPRESSION = "expression";
    public final static String IMPLEMENTATION_TYPE_DELEGATEEXPRESSION = "delegateExpression";
    public final static String IMPLEMENTATION_TYPE_THROW_SIGNAL_EVENT = "throwSignalEvent";
    public final static String IMPLEMENTATION_TYPE_THROW_GLOBAL_SIGNAL_EVENT = "throwGlobalSignalEvent";
    public final static String IMPLEMENTATION_TYPE_THROW_MESSAGE_EVENT = "throwMessageEvent";
    public final static String IMPLEMENTATION_TYPE_THROW_ERROR_EVENT = "throwErrorEvent";
    public final static String IMPLEMENTATION_TYPE_INVALID_THROW_EVENT = "invalidThrowEvent";

}

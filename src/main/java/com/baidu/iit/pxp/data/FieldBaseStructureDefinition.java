package com.baidu.iit.pxp.data;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:30
 */
public interface FieldBaseStructureDefinition extends StructureDefinition {

    int getFieldSize();

    String getFieldNameAt(int index);

    Class<?> getFieldTypeAt(int index);
}
package com.baidu.iit.pxp.data;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:37
 */
public enum ItemKind {
    Information,
    Physical
}
package com.baidu.iit.pxp.data;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:38
 */
public class ItemInstance {

    protected ItemDefinitionData item;

    protected StructureInstance structureInstance;

    public ItemInstance(ItemDefinitionData item, StructureInstance structureInstance) {
        this.item = item;
        this.structureInstance = structureInstance;
    }

    public ItemDefinitionData getItem() {
        return this.item;
    }

    public StructureInstance getStructureInstance() {
        return this.structureInstance;
    }

    private FieldBaseStructureInstance getFieldBaseStructureInstance() {
        return (FieldBaseStructureInstance) this.structureInstance;
    }

    public Object getFieldValue(String fieldName) {
        return this.getFieldBaseStructureInstance().getFieldValue(fieldName);
    }

    public void setFieldValue(String fieldName, Object value) {
        this.getFieldBaseStructureInstance().setFieldValue(fieldName, value);
    }
}

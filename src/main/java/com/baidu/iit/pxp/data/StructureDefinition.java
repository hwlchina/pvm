package com.baidu.iit.pxp.data;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:25
 */
public interface StructureDefinition {


    String getId();

    StructureInstance createInstance();
}

package com.baidu.iit.pxp.data;

import java.io.Serializable;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午2:00
 */
public abstract class ExpressionData implements Serializable {


    @Override
    public abstract boolean equals(Object obj);


    public abstract String getExpressionString();


    @Override
    public abstract int hashCode();


    public abstract boolean isLiteralText();
}

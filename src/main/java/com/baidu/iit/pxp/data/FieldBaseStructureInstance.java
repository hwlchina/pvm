package com.baidu.iit.pxp.data;

import java.util.HashMap;
import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:32
 */
public class FieldBaseStructureInstance implements StructureInstance {

    protected FieldBaseStructureDefinition structureDefinition;

    protected Map<String, Object> fieldValues;

    public FieldBaseStructureInstance(FieldBaseStructureDefinition structureDefinition) {
        this.structureDefinition = structureDefinition;
        this.fieldValues = new HashMap<String, Object>();
    }

    public Object getFieldValue(String fieldName) {
        return this.fieldValues.get(fieldName);
    }

    public void setFieldValue(String fieldName, Object value) {
        this.fieldValues.put(fieldName, value);
    }

    public int getFieldSize() {
        return this.structureDefinition.getFieldSize();
    }

    public String getFieldNameAt(int index) {
        return this.structureDefinition.getFieldNameAt(index);
    }

    public Object[] toArray() {
        int fieldSize = this.getFieldSize();
        Object[] arguments = new Object[fieldSize];
        for (int i = 0; i < fieldSize; i++) {
            String fieldName = this.getFieldNameAt(i);
            Object argument = this.getFieldValue(fieldName);
            arguments[i] = argument;
        }
        return arguments;
    }

    public void loadFrom(Object[] array) {
        int fieldSize = this.getFieldSize();
        for (int i = 0; i < fieldSize; i++) {
            String fieldName = this.getFieldNameAt(i);
            Object fieldValue = array[i];
            this.setFieldValue(fieldName, fieldValue);
        }
    }
}

package com.baidu.iit.pxp.data;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:25
 */
public class ClassStructureDefinition implements FieldBaseStructureDefinition {

    protected String id;

    protected Class< ? > classStructure;

    public ClassStructureDefinition(Class<?> classStructure) {
        this(classStructure.getName(), classStructure);
    }

    public ClassStructureDefinition(String id, Class<?> classStructure) {
        this.id = id;
        this.classStructure = classStructure;
    }

    public String getId() {
        return this.id;
    }

    public int getFieldSize() {
        // TODO
        return 0;
    }

    public String getFieldNameAt(int index) {
        // TODO
        return null;
    }

    public Class< ? > getFieldTypeAt(int index) {
        // TODO
        return null;
    }

    public StructureInstance createInstance() {
        return new FieldBaseStructureInstance(this);
    }
}
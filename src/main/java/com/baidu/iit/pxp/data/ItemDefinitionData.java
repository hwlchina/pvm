package com.baidu.iit.pxp.data;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:37
 */
public class ItemDefinitionData {

    protected String id;

    protected StructureDefinition structure;

    protected boolean isCollection;

    protected ItemKind itemKind;

    private ItemDefinitionData() {
        this.isCollection = false;
        this.itemKind = ItemKind.Information;
    }

    public ItemDefinitionData(String id, StructureDefinition structure) {
        this();
        this.id = id;
        this.structure = structure;
    }

    public ItemInstance createInstance() {
        return new ItemInstance(this, this.structure.createInstance());
    }

    public StructureDefinition getStructureDefinition() {
        return this.structure;
    }

    public boolean isCollection() {
        return isCollection;
    }

    public void setCollection(boolean isCollection) {
        this.isCollection = isCollection;
    }

    public ItemKind getItemKind() {
        return itemKind;
    }

    public void setItemKind(ItemKind itemKind) {
        this.itemKind = itemKind;
    }

    public String getId() {
        return this.id;
    }
}

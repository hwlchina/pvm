package com.baidu.iit.pxp.data;

/**
 *
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:26
 */
public interface StructureInstance {


    Object[] toArray();

    void loadFrom(Object[] array);
}

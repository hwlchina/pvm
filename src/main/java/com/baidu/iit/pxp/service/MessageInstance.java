package com.baidu.iit.pxp.service;

import com.baidu.iit.pxp.data.ItemInstance;
import com.baidu.iit.pxp.data.StructureInstance;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午12:26
 */
public class MessageInstance {

    protected MessageDefinition message;

    protected ItemInstance item;

    public MessageInstance(MessageDefinition message, ItemInstance item) {
        this.message = message;
        this.item = item;
    }

    public StructureInstance getStructureInstance() {
        return this.item.getStructureInstance();
    }

    public MessageDefinition getMessage() {
        return this.message;
    }
}

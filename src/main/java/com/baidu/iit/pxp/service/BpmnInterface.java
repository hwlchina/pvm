package com.baidu.iit.pxp.service;

import com.baidu.iit.pxp.BpmnInterfaceImplementation;
import com.baidu.iit.pxp.model.Operation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午10:57
 */
public class BpmnInterface {

    protected String id;

    protected String name;

    protected BpmnInterfaceImplementation implementation;


    protected Map<String, Operation> operations = new HashMap<String, Operation>();

    public BpmnInterface() {

    }

    public BpmnInterface(String id, String name) {
        setId(id);
        setName(name);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addOperation(Operation operation) {
        operations.put(operation.getId(), operation);
    }

    public Operation getOperation(String operationId) {
        return operations.get(operationId);
    }

    public Collection<Operation> getOperations() {
        return operations.values();
    }

    public BpmnInterfaceImplementation getImplementation() {
        return implementation;
    }

    public void setImplementation(BpmnInterfaceImplementation implementation) {
        this.implementation = implementation;
    }
}

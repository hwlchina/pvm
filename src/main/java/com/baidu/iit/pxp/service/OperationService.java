package com.baidu.iit.pxp.service;

/**
 *
 *
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午2:01
 */
public class OperationService {

    protected String id;

    protected String name;

    protected MessageDefinition inMessage;

    protected MessageDefinition outMessage;

    protected OperationImplementation implementation;


    protected BpmnInterface bpmnInterface;

    public OperationService() {

    }

    public OperationService(String id, String name, BpmnInterface bpmnInterface, MessageDefinition inMessage) {
        setId(id);
        setName(name);
        setInterface(bpmnInterface);
        setInMessage(inMessage);
    }

    public MessageInstance sendMessage(MessageInstance message) {
        return this.implementation.sendFor(message, this);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BpmnInterface getInterface() {
        return bpmnInterface;
    }

    public void setInterface(BpmnInterface bpmnInterface) {
        this.bpmnInterface = bpmnInterface;
    }

    public MessageDefinition getInMessage() {
        return inMessage;
    }

    public void setInMessage(MessageDefinition inMessage) {
        this.inMessage = inMessage;
    }

    public MessageDefinition getOutMessage() {
        return outMessage;
    }

    public void setOutMessage(MessageDefinition outMessage) {
        this.outMessage = outMessage;
    }

    public OperationImplementation getImplementation() {
        return implementation;
    }

    public void setImplementation(OperationImplementation implementation) {
        this.implementation = implementation;
    }

}

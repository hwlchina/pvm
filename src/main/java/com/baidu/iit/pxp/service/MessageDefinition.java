package com.baidu.iit.pxp.service;

import com.baidu.iit.pxp.data.ItemDefinitionData;
import com.baidu.iit.pxp.data.StructureDefinition;
import com.baidu.iit.pxp.model.ItemDefinition;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午12:24
 */
public class MessageDefinition {

    protected String id;

    protected ItemDefinitionData itemDefinition;

    protected String name;

    public MessageDefinition(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public MessageInstance createInstance() {
        return new MessageInstance(this, this.itemDefinition.createInstance());
    }

    public ItemDefinitionData getItemDefinition() {
        return this.itemDefinition;
    }

    public StructureDefinition getStructureDefinition() {
        return this.itemDefinition.getStructureDefinition();
    }

    public void setItemDefinition(ItemDefinitionData itemDefinition) {
        this.itemDefinition = itemDefinition;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
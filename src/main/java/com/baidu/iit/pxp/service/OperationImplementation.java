package com.baidu.iit.pxp.service;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午12:28
 */
public interface OperationImplementation {


    String getId();

    String getName();

    MessageInstance sendFor(MessageInstance message, OperationService operation);
}

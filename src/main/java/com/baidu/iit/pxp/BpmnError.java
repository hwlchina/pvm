package com.baidu.iit.pxp;

import com.baidu.iit.pvm.ActivityException;
import com.baidu.iit.pvm.ActivityIllegalArgumentException;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午5:05
 */
public class BpmnError extends ActivityException {

    private static final long serialVersionUID = 1L;

    private String errorCode;

    public BpmnError(String errorCode) {
        super("");
        setErrorCode(errorCode);
    }

    public BpmnError(String errorCode, String message) {
        super(message + " (errorCode='" + errorCode + "')");
        setErrorCode(errorCode);
    }

    protected void setErrorCode(String errorCode) {
        if (errorCode == null) {
            throw new ActivityIllegalArgumentException("Error Code must not be null.");
        }
        if (errorCode.length() < 1) {
            throw new ActivityIllegalArgumentException("Error Code must not be empty.");
        }
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String toString() {
        return super.toString() + " (errorCode='" + errorCode + "')";
    }

}
package com.baidu.iit.pxp.behavior;

import com.baidu.iit.pvm.ActivityException;
import com.baidu.iit.pvm.PvmActivity;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.CompositeActivityBehavior;
import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.parser.BpmnParse;

import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:20
 */
public class SubProcessActivityBehavior extends AbstractBpmnActivityBehavior implements CompositeActivityBehavior {

    public void execute(ActivityExecution execution) throws Exception {
        PvmActivity activity = execution.getActivity();
        ActivityImpl initialActivity = (ActivityImpl) activity.getProperty(BpmnParse.PROPERTYNAME_INITIAL);

        if (initialActivity == null) {
            throw new ActivityException("No initial activity found for subprocess "
                    + execution.getActivity().getId());
        }

        Map<String, Object> dataObjectVars = ((ActivityImpl) activity).getVariables();
        if (dataObjectVars != null) {
            execution.setVariables(dataObjectVars);
        }

        execution.executeActivity(initialActivity);
    }

    public void lastExecutionEnded(ActivityExecution execution) {
        // ScopeUtil.createEventScopeExecution((ExecutionEntity) execution);

        Map<String, Object> dataObjectVars = ((ActivityImpl) execution.getActivity()).getVariables();
        if (dataObjectVars != null) {
            execution.removeVariables(dataObjectVars.keySet());
        }

        bpmnActivityBehavior.performDefaultOutgoingBehavior(execution);
    }

}

package com.baidu.iit.pxp.behavior;

import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pxp.util.ErrorPropagation;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:22
 */
public class ErrorEndEventActivityBehavior extends FlowNodeActivityBehavior {

    protected String errorCode;

    public ErrorEndEventActivityBehavior(String errorCode) {
        this.errorCode = errorCode;
    }

    public void execute(ActivityExecution execution) throws Exception {
        ErrorPropagation.propagateError(errorCode, execution);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}

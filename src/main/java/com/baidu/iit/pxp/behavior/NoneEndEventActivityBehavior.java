package com.baidu.iit.pxp.behavior;

import com.baidu.iit.pvm.delegate.ActivityExecution;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:16
 */
public class NoneEndEventActivityBehavior extends FlowNodeActivityBehavior {

    public void execute(ActivityExecution execution) throws Exception {
        execution.end();
    }

}

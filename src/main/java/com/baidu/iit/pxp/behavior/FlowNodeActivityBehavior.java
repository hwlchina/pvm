package com.baidu.iit.pxp.behavior;

import com.baidu.iit.pvm.ActivityException;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.SignallableActivityBehavior;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午3:43
 */
public abstract class FlowNodeActivityBehavior implements SignallableActivityBehavior {

    protected BpmnActivityBehavior bpmnActivityBehavior = new BpmnActivityBehavior();

    public void execute(ActivityExecution execution) throws Exception {
        leave(execution);
    }

    protected void leave(ActivityExecution execution) {
        bpmnActivityBehavior.performDefaultOutgoingBehavior(execution);
    }

    protected void leaveIgnoreConditions(ActivityExecution activityContext) {
        bpmnActivityBehavior.performIgnoreConditionsOutgoingBehavior(activityContext);
    }

    public void signal(ActivityExecution execution, String signalName, Object signalData) throws Exception {
        // concrete activity behaviours that do accept signals should override this method;
        throw new ActivityException("this activity doesn't accept signals");
    }

}

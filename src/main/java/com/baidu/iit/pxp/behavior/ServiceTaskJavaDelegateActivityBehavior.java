package com.baidu.iit.pxp.behavior;

import com.baidu.iit.pvm.delegate.ActivityBehavior;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.DelegateExecution;
import com.baidu.iit.pvm.delegate.ExecutionListener;
import com.baidu.iit.pxp.delegate.JavaDelegate;
import com.baidu.iit.pxp.invocation.JavaDelegateInvocation;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午3:40
 */

public class ServiceTaskJavaDelegateActivityBehavior extends TaskActivityBehavior implements ActivityBehavior, ExecutionListener {

    protected JavaDelegate javaDelegate;

    protected ServiceTaskJavaDelegateActivityBehavior() {
    }

    public ServiceTaskJavaDelegateActivityBehavior(JavaDelegate javaDelegate) {
        this.javaDelegate = javaDelegate;
    }

    public void execute(ActivityExecution execution) throws Exception {
        execute((DelegateExecution) execution);
        leave(execution);
    }

    public void notify(DelegateExecution execution) throws Exception {
        execute((DelegateExecution) execution);
    }

    public void execute(DelegateExecution execution) throws Exception {
        JavaDelegateInvocation invocation= new JavaDelegateInvocation(javaDelegate, execution);
        invocation.proceed();
    }
}

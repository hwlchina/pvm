package com.baidu.iit.pxp.behavior;

import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pvm.runtime.InterpretableExecution;

import java.util.Collections;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 上午9:18
 */
public class EventSubProcessStartEventActivityBehavior extends NoneStartEventActivityBehavior {

    protected boolean isInterrupting = true;
    protected String activityId;

    public EventSubProcessStartEventActivityBehavior(String activityId) {
        this.activityId = activityId;
    }

    @Override
    public void execute(ActivityExecution execution) throws Exception {

        InterpretableExecution interpretableExecution = (InterpretableExecution) execution;
        ActivityImpl activity = interpretableExecution.getProcessDefinition().findActivity(activityId);

        ActivityExecution outgoingExecution = execution;

        if(isInterrupting) {
            execution.destroyScope("Event subprocess triggered using activity "+ activityId);
        } else{
            outgoingExecution = execution.createExecution();
            outgoingExecution.setActive(true);
            outgoingExecution.setScope(false);
            outgoingExecution.setConcurrent(true);
        }

        ((InterpretableExecution)outgoingExecution).setActivity(activity);

        outgoingExecution.takeAll(activity.getOutgoingTransitions(), Collections.EMPTY_LIST);
    }

    public void setInterrupting(boolean b) {
        isInterrupting = b;
    }

    public boolean isInterrupting() {
        return isInterrupting;
    }

}

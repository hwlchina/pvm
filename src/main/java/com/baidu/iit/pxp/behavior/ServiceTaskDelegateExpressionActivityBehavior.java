package com.baidu.iit.pxp.behavior;

import com.baidu.iit.pvm.ActivityIllegalArgumentException;
import com.baidu.iit.pvm.delegate.ActivityBehavior;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.DelegateInvocation;
import com.baidu.iit.pvm.delegate.SignallableActivityBehavior;
import com.baidu.iit.pxp.BpmnError;
import com.baidu.iit.pxp.delegate.ClassDelegate;
import com.baidu.iit.pxp.delegate.Expression;
import com.baidu.iit.pxp.delegate.JavaDelegate;
import com.baidu.iit.pxp.invocation.ActivityBehaviorInvocation;
import com.baidu.iit.pxp.invocation.JavaDelegateInvocation;
import com.baidu.iit.pxp.model.FieldDeclaration;
import com.baidu.iit.pxp.util.ErrorPropagation;

import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午5:36
 */
public class ServiceTaskDelegateExpressionActivityBehavior extends TaskActivityBehavior {

    protected Expression expression;
    private final List<FieldDeclaration> fieldDeclarations;

    public ServiceTaskDelegateExpressionActivityBehavior(Expression expression, List<FieldDeclaration> fieldDeclarations) {
        this.expression = expression;
        this.fieldDeclarations = fieldDeclarations;
    }

    @Override
    public void signal(ActivityExecution execution, String signalName, Object signalData) throws Exception {
        Object delegate = expression.getValue(execution);
        if (delegate instanceof SignallableActivityBehavior) {
            ClassDelegate.applyFieldDeclaration(fieldDeclarations, delegate);
            ((SignallableActivityBehavior) delegate).signal(execution, signalName, signalData);
        }
    }

    public void execute(ActivityExecution execution) throws Exception {

        try {

            Object delegate = expression.getValue(execution);
            ClassDelegate.applyFieldDeclaration(fieldDeclarations, delegate);

            if (delegate instanceof ActivityBehavior) {
                DelegateInvocation invocation = new ActivityBehaviorInvocation((ActivityBehavior) delegate, execution);
                invocation.proceed();

            } else if (delegate instanceof JavaDelegate) {
                DelegateInvocation invocation = new JavaDelegateInvocation((JavaDelegate) delegate, execution);
                invocation.proceed();

                leave(execution);

            } else {
                throw new ActivityIllegalArgumentException("Delegate expression " + expression
                        + " did neither resolve to an implementation of " + ActivityBehavior.class
                        + " nor " + JavaDelegate.class);
            }
        } catch (Exception exc) {

            Throwable cause = exc;
            BpmnError error = null;
            while (cause != null) {
                if (cause instanceof BpmnError) {
                    error = (BpmnError) cause;
                    break;
                }
                cause = cause.getCause();
            }

            if (error != null) {
                ErrorPropagation.propagateError(error, execution);
            } else {
                throw exc;
            }

        }
    }

}

package com.baidu.iit.pxp.entity;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pvm.process.ProcessDefinitionImpl;
import com.baidu.iit.pvm.runtime.InterpretableExecution;
import com.baidu.iit.pxp.model.TaskDefinition;
import com.baidu.iit.pxp.util.ActivityEventSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午9:07
 */
public class ProcessDefinitionEntity extends ProcessDefinitionImpl implements ProcessDefinition {

    protected String key;
    protected String category;
    protected String resourceName;

    protected Map<String, TaskDefinition> taskDefinitions;
    protected Map<String, Object> variables;

    protected int suspensionState = SuspensionState.ACTIVE.getStateCode();
    protected transient ActivityEventSupport eventSupport;

    public ProcessDefinitionEntity() {
        super(null);
        eventSupport = new ActivityEventSupport();
    }

    public ExecutionEntity createProcessInstance(String businessKey, ActivityImpl initial) {
        ExecutionEntity processInstance = null;

        if (initial == null) {
            processInstance = (ExecutionEntity) super.createProcessInstance();
        } else {
            processInstance = (ExecutionEntity) super.createProcessInstanceForInitial(initial);
        }

        processInstance.setExecutions(new ArrayList<ExecutionEntity>());
        processInstance.setProcessDefinition(processDefinition);

        if (businessKey != null) {
            processInstance.setBusinessKey(businessKey);
        }


        processInstance.setProcessInstance(processInstance);

        Map<String, Object> dataObjectVars = getVariables();
        if (dataObjectVars != null) {
            processInstance.setVariables(dataObjectVars);
        }
        return processInstance;
    }

    public ExecutionEntity createProcessInstance(String businessKey) {
        return createProcessInstance(businessKey, null);
    }

    public ExecutionEntity createProcessInstance() {
        return createProcessInstance(null);
    }


    @Override
    protected InterpretableExecution newProcessInstance(ActivityImpl activityImpl) {
        ExecutionEntity processInstance = new ExecutionEntity(activityImpl);
        return processInstance;
    }



    public String toString() {
        return "ProcessDefinitionEntity[" + id + "]";
    }



    public Object getPersistentState() {
        Map<String, Object> persistentState = new HashMap<String, Object>();
        persistentState.put("suspensionState", this.suspensionState);
        persistentState.put("category", this.category);
        return persistentState;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }


    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ActivityEventSupport getEventSupport() {
        return eventSupport;
    }

    public Map<String, TaskDefinition> getTaskDefinitions() {
        return taskDefinitions;
    }

    public void setTaskDefinitions(Map<String, TaskDefinition> taskDefinitions) {
        this.taskDefinitions = taskDefinitions;
    }


}

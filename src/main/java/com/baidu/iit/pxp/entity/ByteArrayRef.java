package com.baidu.iit.pxp.entity;

import java.io.Serializable;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:10
 */
public final class ByteArrayRef implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private ByteArrayEntity entity;
    protected boolean deleted = false;

    public ByteArrayRef() {
    }

    public ByteArrayRef(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte[] getBytes() {

        return (entity != null ? entity.getBytes() : null);
    }

    public void setValue(String name, byte[] bytes) {
        this.name = name;
        setBytes(bytes);
    }

    private void setBytes(byte[] bytes) {
        if (id == null) {
            if (bytes != null) {
                entity = ByteArrayEntity.createAndInsert(name, bytes);
                id = entity.getId();
            }
        }
        else {

            entity.setBytes(bytes);
        }
    }

    public ByteArrayEntity getEntity() {
        return entity;
    }

    public void delete() {
        if (!deleted && id != null) {
            if (entity != null) {

            }
            else {

            }
            deleted = true;
        }
    }



    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public String toString() {
        return "ByteArrayRef[id=" + id + ", name=" + name + ", entity=" + entity + (deleted ? ", deleted]" :"]");
    }
}

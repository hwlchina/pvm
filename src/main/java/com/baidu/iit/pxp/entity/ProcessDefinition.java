package com.baidu.iit.pxp.entity;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:33
 */
public interface ProcessDefinition {

    String getId();

    String getCategory();

    String getName();

    String getKey();

    String getDescription();

    String getResourceName();
}

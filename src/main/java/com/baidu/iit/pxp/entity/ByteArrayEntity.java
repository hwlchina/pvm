package com.baidu.iit.pxp.entity;

import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.util.Arrays;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:14
 */
public class ByteArrayEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    protected String id;
    protected int revision;
    protected String name;
    protected byte[] bytes;
    protected String deploymentId;

    protected ByteArrayEntity() {
    }

    public ByteArrayEntity(String name, byte[] bytes) {
        this.name = name;
        this.bytes = bytes;
    }

    public ByteArrayEntity(byte[] bytes) {
        this.bytes = bytes;
    }

    public static ByteArrayEntity createAndInsert(String name, byte[] bytes) {
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(name, bytes);

        return byteArrayEntity;
    }

    public static ByteArrayEntity createAndInsert(byte[] bytes) {
        return createAndInsert(null, bytes);
    }

    public byte[] getBytes() {
        return bytes;
    }

    public Object getPersistentState() {
        return new PersistentState(name, bytes);
    }

    public int getRevisionNext() {
        return revision + 1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    @Override
    public String toString() {
        return "ByteArrayEntity[id=" + id + ", name=" + name + ", size=" + (bytes != null ? bytes.length : 0) + "]";
    }

    private static class PersistentState {

        private final String name;
        private final byte[] bytes;

        public PersistentState(String name, byte[] bytes) {
            this.name = name;
            this.bytes = bytes;
        }

        public boolean equals(Object obj) {
            if (obj instanceof PersistentState) {
                PersistentState other = (PersistentState) obj;
                return ObjectUtils.equals(this.name, other.name)
                        && Arrays.equals(this.bytes, other.bytes);
            }
            return false;
        }

        @Override
        public int hashCode() {
            throw new UnsupportedOperationException();
        }

    }

}

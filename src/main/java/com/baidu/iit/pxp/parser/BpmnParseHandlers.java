package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.FlowElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午7:06
 */
public class BpmnParseHandlers {

    private static final Logger LOGGER = LoggerFactory.getLogger(BpmnParseHandlers.class);

    protected Map<Class<? extends BaseElement>, List<BpmnParseHandler>> parseHandlers;

    public BpmnParseHandlers() {
        this.parseHandlers = new HashMap<Class<? extends BaseElement>, List<BpmnParseHandler>>();
    }

    public List<BpmnParseHandler> getHandlersFor(Class<? extends BaseElement> clazz) {
        return parseHandlers.get(clazz);
    }

    public void addHandlers(List<BpmnParseHandler> bpmnParseHandlers) {
        for (BpmnParseHandler bpmnParseHandler : bpmnParseHandlers) {
            addHandler(bpmnParseHandler);
        }
    }

    public void addHandler(BpmnParseHandler bpmnParseHandler) {
        for (Class<? extends BaseElement> type : bpmnParseHandler.getHandledTypes()) {
            List<BpmnParseHandler> handlers = parseHandlers.get(type);
            if (handlers == null) {
                handlers = new ArrayList<BpmnParseHandler>();
                parseHandlers.put(type, handlers);
            }
            handlers.add(bpmnParseHandler);
        }
    }

    public void parseElement(BpmnParse bpmnParse, BaseElement element) {

        if (element instanceof FlowElement) {
            bpmnParse.setCurrentFlowElement((FlowElement) element);
        }

        List<BpmnParseHandler> handlers = parseHandlers.get(element.getClass());

        if (handlers == null) {
            LOGGER.warn("Could not find matching parse handler for + " + element.getId() + " this is likely a bug.");
        } else {
            for (BpmnParseHandler handler : handlers) {
                handler.parse(bpmnParse, element);
            }
        }
    }
}

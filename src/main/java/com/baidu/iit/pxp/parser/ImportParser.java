package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.Import;
import com.baidu.iit.pxp.util.BpmnXMLUtil;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午1:17
 */
public class ImportParser {

    public void parse(XMLStreamReader xtr, BpmnModel model) throws Exception {
        Import importObject = new Import();
        BpmnXMLUtil.addXMLLocation(importObject, xtr);
        importObject.setImportType(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_IMPORT_TYPE));
        importObject.setNamespace(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_NAMESPACE));
        importObject.setLocation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LOCATION));
        model.getImports().add(importObject);
    }
}
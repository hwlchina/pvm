package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.ActivitiListener;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.HasExecutionListeners;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:46
 */
public class ExecutionListenerParser extends ActivitiListenerParser {

    public String getElementName() {
        return BpmnXMLConstants.ELEMENT_EXECUTION_LISTENER;
    }

    public void addListenerToParent(ActivitiListener listener, BaseElement parentElement) {
        if (parentElement instanceof HasExecutionListeners) {
            ((HasExecutionListeners) parentElement).getExecutionListeners().add(listener);
        }
    }
}

package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.el.ExpressionManager;
import com.baidu.iit.pxp.parser.factory.ActivityBehaviorFactory;
import com.baidu.iit.pxp.parser.factory.BpmnParseFactory;
import com.baidu.iit.pxp.parser.factory.ListenerFactory;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午7:04
 */
public class BpmnParser {

    protected ExpressionManager expressionManager;

    protected ActivityBehaviorFactory activityBehaviorFactory;

    protected ListenerFactory listenerFactory;

    protected BpmnParseFactory bpmnParseFactory;

    protected BpmnParseHandlers bpmnParserHandlers;

    public BpmnParse createParse() {
        return bpmnParseFactory.createBpmnParse(this);
    }

    public ActivityBehaviorFactory getActivityBehaviorFactory() {
        return activityBehaviorFactory;
    }

    public void setActivityBehaviorFactory(ActivityBehaviorFactory activityBehaviorFactory) {
        this.activityBehaviorFactory = activityBehaviorFactory;
    }

    public ListenerFactory getListenerFactory() {
        return listenerFactory;
    }

    public void setListenerFactory(ListenerFactory listenerFactory) {
        this.listenerFactory = listenerFactory;
    }

    public BpmnParseFactory getBpmnParseFactory() {
        return bpmnParseFactory;
    }

    public void setBpmnParseFactory(BpmnParseFactory bpmnParseFactory) {
        this.bpmnParseFactory = bpmnParseFactory;
    }

    public ExpressionManager getExpressionManager() {
        return expressionManager;
    }

    public void setExpressionManager(ExpressionManager expressionManager) {
        this.expressionManager = expressionManager;
    }

    public BpmnParseHandlers getBpmnParserHandlers() {
        return bpmnParserHandlers;
    }

    public void setBpmnParserHandlers(BpmnParseHandlers bpmnParserHandlers) {
        this.bpmnParserHandlers = bpmnParserHandlers;
    }
}

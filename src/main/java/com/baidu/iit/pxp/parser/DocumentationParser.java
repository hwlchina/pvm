package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.converter.BaseChildElementParser;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.FlowElement;
import com.baidu.iit.pxp.model.Process;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:39
 */
public class DocumentationParser extends BaseChildElementParser {

    public String getElementName() {
        return "documentation";
    }

    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        String docText = xtr.getElementText();
        if(StringUtils.isNotEmpty(docText)) {
            if (parentElement instanceof FlowElement) {
                ((FlowElement) parentElement).setDocumentation(docText.trim());
            } else if (parentElement instanceof Process) {
                ((Process) parentElement).setDocumentation(docText.trim());
            }
        }
    }
}
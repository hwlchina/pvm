package com.baidu.iit.pxp.parser.factory;

import com.baidu.iit.pxp.delegate.Expression;
import com.baidu.iit.pxp.el.ExpressionManager;
import com.baidu.iit.pxp.el.FixedValue;
import com.baidu.iit.pxp.model.FieldDeclaration;
import com.baidu.iit.pxp.model.FieldExtension;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午6:48
 */
public abstract class AbstractBehaviorFactory {
    protected ExpressionManager expressionManager;

    public List<FieldDeclaration> createFieldDeclarations(List<FieldExtension> fieldList) {
        List<FieldDeclaration> fieldDeclarations = new ArrayList<FieldDeclaration>();

        for (FieldExtension fieldExtension : fieldList) {
            FieldDeclaration fieldDeclaration = null;
            if (StringUtils.isNotEmpty(fieldExtension.getExpression())) {
                fieldDeclaration = new FieldDeclaration(fieldExtension.getFieldName(), Expression.class.getName(),
                        expressionManager.createExpression(fieldExtension.getExpression()));
            } else {
                fieldDeclaration = new FieldDeclaration(fieldExtension.getFieldName(), Expression.class.getName(),
                        new FixedValue(fieldExtension.getStringValue()));
            }

            fieldDeclarations.add(fieldDeclaration);
        }
        return fieldDeclarations;
    }


    public ExpressionManager getExpressionManager() {
        return expressionManager;
    }

    public void setExpressionManager(ExpressionManager expressionManager) {
        this.expressionManager = expressionManager;
    }


}

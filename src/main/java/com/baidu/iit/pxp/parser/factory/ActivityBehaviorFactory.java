package com.baidu.iit.pxp.parser.factory;

import com.baidu.iit.pvm.process.ActivityImpl;
import com.baidu.iit.pxp.behavior.*;
import com.baidu.iit.pxp.delegate.ClassDelegate;
import com.baidu.iit.pxp.event.EndEvent;
import com.baidu.iit.pxp.event.StartEvent;
import com.baidu.iit.pxp.model.*;

/**
 * Behavior 的具体创建类
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午6:48
 */
public interface ActivityBehaviorFactory {


    /**
     * 创建开始动作点
     *
     * @param startEvent
     * @return
     */
    public abstract NoneStartEventActivityBehavior createNoneStartEventActivityBehavior(StartEvent startEvent);


    public abstract EventSubProcessStartEventActivityBehavior createEventSubProcessStartEventActivityBehavior(StartEvent startEvent, String activityId);

    public abstract TaskActivityBehavior createTaskActivityBehavior(Task task);


    public abstract ExclusiveGatewayActivityBehavior createExclusiveGatewayActivityBehavior(ExclusiveGateway exclusiveGateway);

    public abstract ParallelGatewayActivityBehavior createParallelGatewayActivityBehavior(ParallelGateway parallelGateway);

    public abstract InclusiveGatewayActivityBehavior createInclusiveGatewayActivityBehavior(InclusiveGateway inclusiveGateway);

    public abstract EventBasedGatewayActivityBehavior createEventBasedGatewayActivityBehavior(EventGateway eventGateway);

    public abstract SequentialMultiInstanceBehavior createSequentialMultiInstanceBehavior(ActivityImpl activity, AbstractBpmnActivityBehavior innerActivityBehavior);

    public abstract ParallelMultiInstanceBehavior createParallelMultiInstanceBehavior(ActivityImpl activity, AbstractBpmnActivityBehavior innerActivityBehavior);

    public abstract SubProcessActivityBehavior createSubprocActivityBehavior(SubProcess subProcess);

    public abstract TransactionActivityBehavior createTransactionActivityBehavior(Transaction transaction);

    public abstract NoneEndEventActivityBehavior createNoneEndEventActivityBehavior(EndEvent endEvent);

    public abstract ErrorEndEventActivityBehavior createErrorEndEventActivityBehavior(EndEvent endEvent, ErrorEventDefinition errorEventDefinition);


    public abstract ClassDelegate createClassDelegateServiceTask(ServiceTask serviceTask);

    public abstract ServiceTaskDelegateExpressionActivityBehavior createServiceTaskDelegateExpressionActivityBehavior(ServiceTask serviceTask);

    public abstract ServiceTaskExpressionActivityBehavior createServiceTaskExpressionActivityBehavior(ServiceTask serviceTask);

}
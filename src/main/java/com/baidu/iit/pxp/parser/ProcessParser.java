package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.export.ProcessExport;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.Process;
import com.baidu.iit.pxp.util.BpmnXMLUtil;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午2:02
 */
public class ProcessParser {

    /**
     * 流程定义元素解析
     *
     * @param xtr
     * @param model
     * @return
     * @throws Exception
     */
    public Process parse(XMLStreamReader xtr, BpmnModel model) throws Exception {
        Process process = null;
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_ID))) {
            String processId = xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_ID);
            process = new Process();
            process.setId(processId);
            BpmnXMLUtil.addXMLLocation(process, xtr);
            process.setName(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_NAME));
            BpmnXMLUtil.addCustomAttributes(xtr, process, ProcessExport.defaultProcessAttributes);
            model.getProcesses().add(process);
        }
        return process;
    }

}

package com.baidu.iit.pxp.parser.factory;

import com.baidu.iit.pvm.delegate.ExecutionListener;
import com.baidu.iit.pxp.listener.ActivityEventListener;
import com.baidu.iit.pxp.listener.TaskListener;
import com.baidu.iit.pxp.model.ActivitiListener;
import com.baidu.iit.pxp.model.EventListener;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午6:47
 */
public interface ListenerFactory {

    public abstract TaskListener createClassDelegateTaskListener(ActivitiListener activitiListener);

    public abstract TaskListener createExpressionTaskListener(ActivitiListener activitiListener);

    public abstract TaskListener createDelegateExpressionTaskListener(ActivitiListener activitiListener);

    public abstract ExecutionListener createClassDelegateExecutionListener(ActivitiListener activitiListener);

    public abstract ExecutionListener createExpressionExecutionListener(ActivitiListener activitiListener);

    public abstract ExecutionListener createDelegateExpressionExecutionListener(ActivitiListener activitiListener);

    public abstract ActivityEventListener createClassDelegateEventListener(EventListener eventListener);

    public abstract ActivityEventListener createDelegateExpressionEventListener(EventListener eventListener);

}
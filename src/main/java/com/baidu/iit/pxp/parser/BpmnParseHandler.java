package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.model.BaseElement;

import java.util.Collection;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 上午9:59
 */
public interface BpmnParseHandler {

    Collection<Class<? extends BaseElement>> getHandledTypes();

    void parse(BpmnParse bpmnParse, BaseElement element);

}
package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.model.Import;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午7:21
 */
public interface XMLImporter {

    /**
     * 导入xml中的插入文件
     * @param theImport
     * @param parse
     */
    void importFrom(Import theImport, BpmnParse parse);
}

package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.converter.BaseChildElementParser;
import com.baidu.iit.pxp.model.Activity;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.MultiInstanceLoopCharacteristics;
import com.baidu.iit.pxp.util.BpmnXMLUtil;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午9:58
 */
public class MultiInstanceParser extends BaseChildElementParser {

    public String getElementName() {
        return BpmnXMLConstants.ELEMENT_MULTIINSTANCE;
    }

    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        if (parentElement instanceof Activity == false) return;

        MultiInstanceLoopCharacteristics multiInstanceDef = new MultiInstanceLoopCharacteristics();
        BpmnXMLUtil.addXMLLocation(multiInstanceDef, xtr);
        if (xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_MULTIINSTANCE_SEQUENTIAL) != null) {
            multiInstanceDef.setSequential(Boolean.valueOf(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_MULTIINSTANCE_SEQUENTIAL)));
        }
        multiInstanceDef.setInputDataItem(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_MULTIINSTANCE_COLLECTION));
        multiInstanceDef.setElementVariable(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_MULTIINSTANCE_VARIABLE));
        multiInstanceDef.setElementIndexVariable(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_MULTIINSTANCE_INDEX_VARIABLE));

        boolean readyWithMultiInstance = false;
        try {
            while (readyWithMultiInstance == false && xtr.hasNext()) {
                xtr.next();
                if (xtr.isStartElement() && BpmnXMLConstants.ELEMENT_MULTIINSTANCE_CARDINALITY.equalsIgnoreCase(xtr.getLocalName())) {
                    multiInstanceDef.setLoopCardinality(xtr.getElementText());

                } else if (xtr.isStartElement() && BpmnXMLConstants.ELEMENT_MULTIINSTANCE_DATAINPUT.equalsIgnoreCase(xtr.getLocalName())) {
                    multiInstanceDef.setInputDataItem(xtr.getElementText());

                } else if (xtr.isStartElement() && BpmnXMLConstants.ELEMENT_MULTIINSTANCE_DATAITEM.equalsIgnoreCase(xtr.getLocalName())) {
                    if (xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_NAME) != null) {
                        multiInstanceDef.setElementVariable(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_NAME));
                    }

                } else if (xtr.isStartElement() && BpmnXMLConstants.ELEMENT_MULTIINSTANCE_CONDITION.equalsIgnoreCase(xtr.getLocalName())) {
                    multiInstanceDef.setCompletionCondition(xtr.getElementText());

                } else if (xtr.isEndElement() && getElementName().equalsIgnoreCase(xtr.getLocalName())) {
                    readyWithMultiInstance = true;
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Error parsing multi instance definition", e);
        }
        ((Activity) parentElement).setLoopCharacteristics(multiInstanceDef);
    }
}

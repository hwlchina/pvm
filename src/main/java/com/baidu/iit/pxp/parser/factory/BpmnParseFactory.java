package com.baidu.iit.pxp.parser.factory;

import com.baidu.iit.pxp.parser.BpmnParse;
import com.baidu.iit.pxp.parser.BpmnParser;

/**
 * User: huangweili
 * Date: 14-4-24
 * Time: 下午7:05
 */
public interface BpmnParseFactory {
    BpmnParse createBpmnParse(BpmnParser bpmnParser);
}

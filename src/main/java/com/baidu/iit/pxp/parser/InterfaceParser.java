package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.Interface;
import com.baidu.iit.pxp.model.Operation;
import com.baidu.iit.pxp.util.BpmnXMLUtil;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午5:45
 */
public class InterfaceParser {

    protected static final Logger LOGGER = Logger.getLogger(InterfaceParser.class.getName());


    /**
     * 解析xml中的结构
     *
     * @param xtr
     * @param model
     * @throws Exception
     */
    public void parse(XMLStreamReader xtr, BpmnModel model) throws Exception {

        Interface interfaceObject = new Interface();
        BpmnXMLUtil.addXMLLocation(interfaceObject, xtr);
        interfaceObject.setId(model.getTargetNamespace() + ":" + xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_ID));
        interfaceObject.setName(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_NAME));
        interfaceObject.setImplementationRef(parseMessageRef(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_IMPLEMENTATION_REF), model));

        boolean readyWithInterface = false;
        Operation operation = null;
        try {
            while (readyWithInterface == false && xtr.hasNext()) {
                xtr.next();
                if (xtr.isStartElement() && BpmnXMLConstants.ELEMENT_OPERATION.equals(xtr.getLocalName())) {
                    operation = new Operation();
                    BpmnXMLUtil.addXMLLocation(operation, xtr);
                    operation.setId(model.getTargetNamespace() + ":" + xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_ID));
                    operation.setName(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_NAME));
                    operation.setImplementationRef(parseMessageRef(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_IMPLEMENTATION_REF), model));

                } else if (xtr.isStartElement() && BpmnXMLConstants.ELEMENT_IN_MESSAGE.equals(xtr.getLocalName())) {
                    String inMessageRef = xtr.getElementText();
                    if (operation != null && StringUtils.isNotEmpty(inMessageRef)) {
                        operation.setInMessageRef(parseMessageRef(inMessageRef.trim(), model));
                    }

                } else if (xtr.isStartElement() && BpmnXMLConstants.ELEMENT_OUT_MESSAGE.equals(xtr.getLocalName())) {
                    String outMessageRef = xtr.getElementText();
                    if (operation != null && StringUtils.isNotEmpty(outMessageRef)) {
                        operation.setOutMessageRef(parseMessageRef(outMessageRef.trim(), model));
                    }

                } else if (xtr.isEndElement() && BpmnXMLConstants.ELEMENT_OPERATION.equalsIgnoreCase(xtr.getLocalName())) {
                    if (operation != null && StringUtils.isNotEmpty(operation.getImplementationRef())) {
                        interfaceObject.getOperations().add(operation);
                    }

                } else if (xtr.isEndElement() && BpmnXMLConstants.ELEMENT_INTERFACE.equals(xtr.getLocalName())) {
                    readyWithInterface = true;
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error parsing interface child elements", e);
        }

        model.getInterfaces().add(interfaceObject);
    }

    protected String parseMessageRef(String messageRef, BpmnModel model) {
        String result = null;
        if (StringUtils.isNotEmpty(messageRef)) {
            int indexOfP = messageRef.indexOf(':');
            if (indexOfP != -1) {
                String prefix = messageRef.substring(0, indexOfP);
                String resolvedNamespace = model.getNamespace(prefix);
                result = resolvedNamespace + ":" + messageRef.substring(indexOfP + 1);
            } else {
                result = model.getTargetNamespace() + ":" + messageRef;
            }
        }
        return result;
    }
}

package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.constants.ImplementationTypeConstants;
import com.baidu.iit.pxp.converter.BaseChildElementParser;
import com.baidu.iit.pxp.model.ActivitiListener;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.util.BpmnXMLUtil;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:46
 */
public abstract class ActivitiListenerParser extends BaseChildElementParser {

    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {

        ActivitiListener listener = new ActivitiListener();
        BpmnXMLUtil.addXMLLocation(listener, xtr);
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_CLASS))) {
            listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_CLASS));
            listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_CLASS);
        } else if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_EXPRESSION))) {
            listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_EXPRESSION));
            listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_EXPRESSION);
        } else if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_DELEGATEEXPRESSION))) {
            listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_DELEGATEEXPRESSION));
            listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION);
        }
        listener.setEvent(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_EVENT));
        addListenerToParent(listener, parentElement);
        parseChildElements(xtr, listener, model, new FieldExtensionParser());
    }

    public abstract void addListenerToParent(ActivitiListener listener, BaseElement parentElement);
}

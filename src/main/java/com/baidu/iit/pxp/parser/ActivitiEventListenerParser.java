package com.baidu.iit.pxp.parser;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.constants.ImplementationTypeConstants;
import com.baidu.iit.pxp.converter.BaseChildElementParser;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.EventListener;
import com.baidu.iit.pxp.model.Process;
import com.baidu.iit.pxp.util.BpmnXMLUtil;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午3:57
 */
public class ActivitiEventListenerParser extends BaseChildElementParser {

    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        EventListener listener = new EventListener();
        BpmnXMLUtil.addXMLLocation(listener, xtr);
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_CLASS))) {
            listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_CLASS));
            listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_CLASS);
        } else if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_DELEGATEEXPRESSION))) {
            listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_DELEGATEEXPRESSION));
            listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION);
        } else if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_EVENT_TYPE))) {
            String eventType = xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_EVENT_TYPE);
            if (BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_EVENT_TYPE_SIGNAL.equals(eventType)) {
                listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_THROW_SIGNAL_EVENT);
                listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_SIGNAL_EVENT_NAME));
            } else if (BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_EVENT_TYPE_GLOBAL_SIGNAL.equals(eventType)) {
                listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_THROW_GLOBAL_SIGNAL_EVENT);
                listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_SIGNAL_EVENT_NAME));
            } else if (BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_EVENT_TYPE_MESSAGE.equals(eventType)) {
                listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_THROW_MESSAGE_EVENT);
                listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_MESSAGE_EVENT_NAME));
            } else if (BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_EVENT_TYPE_ERROR.equals(eventType)) {
                listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_THROW_ERROR_EVENT);
                listener.setImplementation(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_THROW_ERROR_EVENT_CODE));
            } else {
                listener.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_INVALID_THROW_EVENT);
            }
        }
        listener.setEvents(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_EVENTS));
        listener.setEntityType(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_LISTENER_ENTITY_TYPE));

        Process parentProcess = (Process) parentElement;
        parentProcess.getEventListeners().add(listener);
    }

    @Override
    public String getElementName() {
        return BpmnXMLConstants.ELEMENT_EVENT_LISTENER;
    }
}

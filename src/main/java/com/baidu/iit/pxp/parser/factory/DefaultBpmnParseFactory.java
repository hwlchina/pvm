package com.baidu.iit.pxp.parser.factory;

import com.baidu.iit.pxp.parser.BpmnParse;
import com.baidu.iit.pxp.parser.BpmnParser;

/**
 * User: huangweili
 * Date: 14-4-26
 * Time: 下午10:50
 */
public class DefaultBpmnParseFactory implements BpmnParseFactory {

    public BpmnParse createBpmnParse(BpmnParser bpmnParser) {
        return new BpmnParse(bpmnParser);
    }
}

package com.baidu.iit.pxp.invocation;

import com.baidu.iit.pvm.delegate.ActivityBehavior;
import com.baidu.iit.pvm.delegate.ActivityExecution;
import com.baidu.iit.pvm.delegate.DelegateInvocation;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午5:38
 */
public class ActivityBehaviorInvocation extends DelegateInvocation {

    protected final ActivityBehavior behaviorInstance;

    protected final ActivityExecution execution;

    public ActivityBehaviorInvocation(ActivityBehavior behaviorInstance, ActivityExecution execution) {
        this.behaviorInstance = behaviorInstance;
        this.execution = execution;
    }

    protected void invoke() throws Exception {
        behaviorInstance.execute(execution);
    }

    public Object getTarget() {
        return behaviorInstance;
    }

}

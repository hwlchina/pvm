package com.baidu.iit.pxp.invocation;

import com.baidu.iit.pvm.delegate.DelegateInvocation;
import com.baidu.iit.pxp.listener.TaskListener;
import com.baidu.iit.pxp.model.DelegateTask;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午1:13
 */
public class TaskListenerInvocation extends DelegateInvocation {

    protected final TaskListener executionListenerInstance;
    protected final DelegateTask delegateTask;

    public TaskListenerInvocation(TaskListener executionListenerInstance, DelegateTask delegateTask) {
        this.executionListenerInstance = executionListenerInstance;
        this.delegateTask = delegateTask;
    }

    protected void invoke() throws Exception {
        executionListenerInstance.notify(delegateTask);
    }

    public Object getTarget() {
        return executionListenerInstance;
    }

}

package com.baidu.iit.pxp.invocation;

import com.baidu.iit.pvm.delegate.DelegateExecution;
import com.baidu.iit.pvm.delegate.DelegateInvocation;
import com.baidu.iit.pxp.delegate.JavaDelegate;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午3:41
 */
public class JavaDelegateInvocation extends DelegateInvocation {

    protected final JavaDelegate delegateInstance;
    protected final DelegateExecution execution;

    public JavaDelegateInvocation(JavaDelegate delegateInstance, DelegateExecution execution) {
        this.delegateInstance = delegateInstance;
        this.execution = execution;
    }

    protected void invoke() throws Exception {
        delegateInstance.execute((DelegateExecution) execution);
    }

    public Object getTarget() {
        return delegateInstance;
    }

}
package com.baidu.iit.pxp.invocation;

import com.baidu.iit.pvm.delegate.DelegateExecution;
import com.baidu.iit.pvm.delegate.DelegateInvocation;
import com.baidu.iit.pvm.delegate.ExecutionListener;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午4:58
 */
public class ExecutionListenerInvocation extends DelegateInvocation {

    protected final ExecutionListener executionListenerInstance;
    protected final DelegateExecution execution;

    public ExecutionListenerInvocation(ExecutionListener executionListenerInstance, DelegateExecution execution) {
        this.executionListenerInstance = executionListenerInstance;
        this.execution = execution;
    }

    protected void invoke() throws Exception {
        executionListenerInstance.notify(execution);
    }

    public Object getTarget() {
        return executionListenerInstance;
    }

}

package com.baidu.iit.pxp.converter;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.event.EndEvent;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.util.BpmnXMLUtil;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 下午5:18
 */
public class EndEventXMLConverter extends BaseBpmnXMLConverter {

    public Class<? extends BaseElement> getBpmnElementType() {
        return EndEvent.class;
    }

    @Override
    protected String getXMLElementName() {
        return BpmnXMLConstants.ELEMENT_EVENT_END;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        EndEvent endEvent = new EndEvent();
        BpmnXMLUtil.addXMLLocation(endEvent, xtr);
        parseChildElements(getXMLElementName(), endEvent, model, xtr);
        return endEvent;
    }

}


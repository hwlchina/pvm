package com.baidu.iit.pxp.converter;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.event.StartEvent;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.util.BpmnXMLUtil;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 下午5:18
 */
public class StartEventXMLConverter extends BaseBpmnXMLConverter {

    public Class<? extends BaseElement> getBpmnElementType() {
        return StartEvent.class;
    }

    @Override
    protected String getXMLElementName() {
        return BpmnXMLConstants.ELEMENT_EVENT_START;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        String formKey = xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_FORM_FORMKEY);
        StartEvent startEvent = null;
        if (startEvent == null) {
            startEvent = new StartEvent();
        }
        BpmnXMLUtil.addXMLLocation(startEvent, xtr);
        startEvent.setInitiator(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_EVENT_START_INITIATOR));
        startEvent.setFormKey(formKey);

        parseChildElements(getXMLElementName(), startEvent, model, xtr);

        return startEvent;
    }

}

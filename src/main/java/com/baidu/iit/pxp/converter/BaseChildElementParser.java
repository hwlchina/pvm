package com.baidu.iit.pxp.converter;

import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-25
 * Time: 下午1:10
 */
public abstract class BaseChildElementParser {

    protected static final Logger LOGGER = LoggerFactory.getLogger(BaseChildElementParser.class);

    public abstract String getElementName();

    public abstract void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception;

    protected void parseChildElements(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model, BaseChildElementParser parser) throws Exception {
        boolean readyWithChildElements = false;
        while (readyWithChildElements == false && xtr.hasNext()) {
            xtr.next();
            if (xtr.isStartElement()) {
                if (parser.getElementName().equals(xtr.getLocalName())) {
                    parser.parseChildElement(xtr, parentElement, model);
                }

            } else if (xtr.isEndElement() && getElementName().equalsIgnoreCase(xtr.getLocalName())) {
                readyWithChildElements = true;
            }
        }
    }
}

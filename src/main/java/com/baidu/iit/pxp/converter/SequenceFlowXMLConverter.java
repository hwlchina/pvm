package com.baidu.iit.pxp.converter;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.SequenceFlow;
import com.baidu.iit.pxp.util.BpmnXMLUtil;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 下午5:23
 */
public class SequenceFlowXMLConverter extends BaseBpmnXMLConverter {

    public Class<? extends BaseElement> getBpmnElementType() {
        return SequenceFlow.class;
    }

    @Override
    protected String getXMLElementName() {
        return BpmnXMLConstants.ELEMENT_SEQUENCE_FLOW;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        SequenceFlow sequenceFlow = new SequenceFlow();
        BpmnXMLUtil.addXMLLocation(sequenceFlow, xtr);
        sequenceFlow.setSourceRef(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_FLOW_SOURCE_REF));
        sequenceFlow.setTargetRef(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_FLOW_TARGET_REF));
        sequenceFlow.setName(xtr.getAttributeValue(null, BpmnXMLConstants.ATTRIBUTE_NAME));
        parseChildElements(getXMLElementName(), sequenceFlow, model, xtr);
        return sequenceFlow;
    }


}

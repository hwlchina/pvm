package com.baidu.iit.pxp.converter;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.ParallelGateway;
import com.baidu.iit.pxp.util.BpmnXMLUtil;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-27
 * Time: 下午5:33
 */
public class ParallelGatewayXMLConverter extends BaseBpmnXMLConverter {

    public Class<? extends BaseElement> getBpmnElementType() {
        return ParallelGateway.class;
    }

    @Override
    protected String getXMLElementName() {
        return BpmnXMLConstants.ELEMENT_GATEWAY_PARALLEL;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        ParallelGateway gateway = new ParallelGateway();
        BpmnXMLUtil.addXMLLocation(gateway, xtr);
        parseChildElements(getXMLElementName(), gateway, model, xtr);
        return gateway;
    }


}

package com.baidu.iit.pxp.converter;

import com.baidu.iit.pxp.constants.BpmnXMLConstants;
import com.baidu.iit.pxp.constants.ImplementationTypeConstants;
import com.baidu.iit.pxp.model.BaseElement;
import com.baidu.iit.pxp.model.BpmnModel;
import com.baidu.iit.pxp.model.ServiceTask;
import com.baidu.iit.pxp.util.BpmnXMLUtil;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午5:42
 */
public class ServiceTaskXMLConverter extends BaseBpmnXMLConverter {

    public Class<? extends BaseElement> getBpmnElementType() {
        return ServiceTask.class;
    }

    @Override
    protected String getXMLElementName() {
        return BpmnXMLConstants.ELEMENT_TASK_SERVICE;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        ServiceTask serviceTask = new ServiceTask();
        BpmnXMLUtil.addXMLLocation(serviceTask, xtr);

        if (StringUtils.isNotEmpty(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TASK_SERVICE_CLASS))) {
            serviceTask.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_CLASS);
            serviceTask.setImplementation(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TASK_SERVICE_CLASS));

        } else if (StringUtils.isNotEmpty(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TASK_SERVICE_EXPRESSION))) {
            serviceTask.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_EXPRESSION);
            serviceTask.setImplementation(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TASK_SERVICE_EXPRESSION));

        } else if (StringUtils.isNotEmpty(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TASK_SERVICE_DELEGATEEXPRESSION))) {
            serviceTask.setImplementationType(ImplementationTypeConstants.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION);
            serviceTask.setImplementation(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TASK_SERVICE_DELEGATEEXPRESSION));

        }
        serviceTask.setResultVariableName(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TASK_SERVICE_RESULTVARIABLE));
        if (StringUtils.isEmpty(serviceTask.getResultVariableName())) {
            serviceTask.setResultVariableName(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, "resultVariable"));
        }
        serviceTask.setType(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TYPE));
        serviceTask.setExtensionId(xtr.getAttributeValue(BpmnXMLConstants.ACTIVITI_EXTENSIONS_NAMESPACE, BpmnXMLConstants.ATTRIBUTE_TASK_SERVICE_EXTENSIONID));
        parseChildElements(getXMLElementName(), serviceTask, model, xtr);
        return serviceTask;
    }
}

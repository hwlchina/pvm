package com.baidu.iit.pxp.el.juel;

import java.io.Serializable;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:17
 */
public interface TreeBuilder extends Serializable {

    public Tree build(String expression) throws TreeBuilderException;
}

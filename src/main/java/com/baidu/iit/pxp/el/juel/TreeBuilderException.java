package com.baidu.iit.pxp.el.juel;

import com.baidu.iit.pxp.el.ELException;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:46
 */
public class TreeBuilderException extends ELException {


    private final String expression;
    private final int position;
    private final String encountered;
    private final String expected;

    public TreeBuilderException(String expression, int position, String encountered, String expected, String message) {
        super("error.build:" + expression);
        this.expression = expression;
        this.position = position;
        this.encountered = encountered;
        this.expected = expected;
    }


    public String getExpression() {
        return expression;
    }


    public int getPosition() {
        return position;
    }


    public String getEncountered() {
        return encountered;
    }


    public String getExpected() {
        return expected;
    }
}

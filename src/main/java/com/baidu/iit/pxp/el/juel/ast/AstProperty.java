package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ELException;
import com.baidu.iit.pxp.el.MethodInfo;
import com.baidu.iit.pxp.el.ValueReference;
import com.baidu.iit.pxp.el.juel.Bindings;
import com.baidu.iit.pxp.el.juel.PropertyNotFoundException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午3:51
 */
public abstract class AstProperty extends AstNode {
    protected final AstNode prefix;
    protected final boolean lvalue;
    protected final boolean strict;

    public AstProperty(AstNode prefix, boolean lvalue, boolean strict) {
        this.prefix = prefix;
        this.lvalue = lvalue;
        this.strict = strict;
    }

    protected abstract Object getProperty(Bindings bindings, ELContext context) throws ELException;

    protected AstNode getPrefix() {
        return prefix;
    }

    public ValueReference getValueReference(Bindings bindings, ELContext context) {
        return new ValueReference(prefix.eval(bindings, context), getProperty(bindings, context));
    }

    @Override
    public Object eval(Bindings bindings, ELContext context) {
        Object base = prefix.eval(bindings, context);
        if (base == null) {
            return null;
        }
        Object property = getProperty(bindings, context);
        if (property == null && strict) {
            return null;
        }
        context.setPropertyResolved(false);
        Object result = context.getELResolver().getValue(context, base, property);
        if (!context.isPropertyResolved()) {
            throw new PropertyNotFoundException(String.format("Cannot resolve property %s in %s", property, base));

        }
        return result;
    }

    public final boolean isLiteralText() {
        return false;
    }

    public final boolean isLeftValue() {
        return lvalue;
    }

    public boolean isMethodInvocation() {
        return false;
    }

    public Class<?> getType(Bindings bindings, ELContext context) {
        if (!lvalue) {
            return null;
        }
        Object base = prefix.eval(bindings, context);
        if (base == null) {
            throw new PropertyNotFoundException(String.format("Target unreachable, base expression %s resolved to null", prefix));
        }
        Object property = getProperty(bindings, context);
        if (property == null && strict) {
            throw new PropertyNotFoundException(String.format("Cannot resolve property %s in %s", "null", base));
        }
        context.setPropertyResolved(false);
        Class<?> result = context.getELResolver().getType(context, base, property);
        if (!context.isPropertyResolved()) {
            throw new PropertyNotFoundException(String.format("Cannot resolve property %s in %s", "null", base));
        }
        return result;
    }

    public boolean isReadOnly(Bindings bindings, ELContext context) throws ELException {
        if (!lvalue) {
            return true;
        }
        Object base = prefix.eval(bindings, context);
        if (base == null) {
            throw new PropertyNotFoundException(String.format("Target unreachable, base expression %s resolved to null", prefix));
        }
        Object property = getProperty(bindings, context);
        if (property == null && strict) {
            throw new PropertyNotFoundException(String.format("Cannot resolve property %s in %s", "null", base));
        }
        context.setPropertyResolved(false);
        boolean result = context.getELResolver().isReadOnly(context, base, property);
        if (!context.isPropertyResolved()) {
            throw new PropertyNotFoundException(String.format("Cannot resolve property %s in %s", "null", base));
        }
        return result;
    }

    public void setValue(Bindings bindings, ELContext context, Object value) throws ELException {
        if (!lvalue) {
            throw new ELException(String.format("Cannot set value of a non-lvalue expression %s", getStructuralId(bindings)));
        }
        Object base = prefix.eval(bindings, context);
        if (base == null) {
            throw new PropertyNotFoundException(String.format("Target unreachable, base expression %s resolved to null", prefix));
        }
        Object property = getProperty(bindings, context);
        if (property == null && strict) {
            throw new PropertyNotFoundException(String.format("Cannot resolve property %s in %s", "null", base));
        }
        context.setPropertyResolved(false);
        context.getELResolver().setValue(context, base, property, value);
        if (!context.isPropertyResolved()) {
            throw new PropertyNotFoundException(String.format("Cannot resolve property %s in %s", "null", base));
        }
    }

    protected Method findMethod(String name, Class<?> clazz, Class<?> returnType, Class<?>[] paramTypes) {
        Method method = null;
        try {
            method = clazz.getMethod(name, paramTypes);
        } catch (NoSuchMethodException e) {
            throw new PropertyNotFoundException(String.format("Cannot find method %s in %s", name, clazz));
        }
        if (returnType != null && !returnType.isAssignableFrom(method.getReturnType())) {
            throw new PropertyNotFoundException(String.format("Cannot find method %s in %s", name, clazz));
        }
        return method;
    }

    public MethodInfo getMethodInfo(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes) {
        Object base = prefix.eval(bindings, context);
        if (base == null) {
            throw new PropertyNotFoundException(String.format("Target unreachable, base expression %s resolved to null", prefix));
        }
        Object property = getProperty(bindings, context);
        if (property == null && strict) {
            throw new PropertyNotFoundException(String.format("Cannot resolve property %s in %s", "null", base));
        }
        String name = bindings.convert(property, String.class);
        Method method = findMethod(name, base.getClass(), returnType, paramTypes);
        return new MethodInfo(method.getName(), method.getReturnType(), paramTypes);
    }

    public Object invoke(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes, Object[] paramValues) {
        Object base = prefix.eval(bindings, context);
        if (base == null) {
            throw new PropertyNotFoundException(String.format("Target unreachable, base expression %s resolved to null", prefix));
        }
        Object property = getProperty(bindings, context);
        if (property == null && strict) {
            throw new PropertyNotFoundException(String.format("Cannot find method %s in %s", "null", base));
        }
        String name = bindings.convert(property, String.class);
        Method method = findMethod(name, base.getClass(), returnType, paramTypes);
        try {
            return method.invoke(base, paramValues);
        } catch (IllegalAccessException e) {
            throw new ELException(String.format("Cannot access method %s in %s", name, base.getClass()));
        } catch (IllegalArgumentException e) {
            throw new ELException(String.format("Error invoking method %s in %s", name, base.getClass()), e);
        } catch (InvocationTargetException e) {
            throw new ELException(String.format("Cannot access method %s in %s", name, base.getClass()), e.getCause());
        }
    }

    public AstNode getChild(int i) {
        return i == 0 ? prefix : null;
    }
}

package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.juel.Bindings;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:33
 */
public final class AstNull extends AstLiteral {
    @Override
    public Object eval(Bindings bindings, ELContext context) {
        return null;
    }

    @Override
    public String toString() {
        return "null";
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        b.append("null");
    }
}

package com.baidu.iit.pxp.el.juel;

import com.baidu.iit.pxp.el.ELException;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午3:53
 */
public class PropertyNotFoundException extends ELException {
    private static final long serialVersionUID = 1L;


    public PropertyNotFoundException() {
        super();
    }


    public PropertyNotFoundException(String message) {
        super(message);
    }


    public PropertyNotFoundException(Throwable cause) {
        super(cause);
    }


    public PropertyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.baidu.iit.pxp.el.juel;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:23
 */
public interface IdentifierNode extends Node {

    public String getName();


    public int getIndex();
}

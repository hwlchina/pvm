package com.baidu.iit.pxp.el;


import com.baidu.iit.pxp.data.ExpressionData;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:49
 */
public abstract class ValueExpression extends ExpressionData {

    public abstract Class<?> getExpectedType();

    public abstract Class<?> getType(ELContext context);

    public abstract Object getValue(ELContext context);

    public abstract boolean isReadOnly(ELContext context);

    public abstract void setValue(ELContext context, Object value);

    public ValueReference getValueReference(ELContext context) {
        return null;
    }
}

package com.baidu.iit.pxp.el.juel;

import com.baidu.iit.pvm.delegate.DelegateInvocation;
import com.baidu.iit.pxp.el.ValueExpression;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:05
 */
public abstract class ExpressionInvocation extends DelegateInvocation {

    protected final ValueExpression valueExpression;

    public ExpressionInvocation(ValueExpression valueExpression) {
        this.valueExpression = valueExpression;
    }

    public Object getTarget() {
        return valueExpression;
    }

}

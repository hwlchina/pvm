package com.baidu.iit.pxp.el;

import java.io.Serializable;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:09
 */
public interface TypeConverter extends Serializable {

    public static final TypeConverter DEFAULT = new TypeConverterImpl();

    public <T> T convert(Object value, Class<T> type) throws ELException;
}

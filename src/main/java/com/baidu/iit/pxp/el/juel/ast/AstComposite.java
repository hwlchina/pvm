package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.juel.Bindings;

import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:49
 */
public class AstComposite extends AstRightValue {
    private final List<AstNode> nodes;

    public AstComposite(List<AstNode> nodes) {
        this.nodes = nodes;
    }

    @Override
    public Object eval(Bindings bindings, ELContext context) {
        StringBuilder b = new StringBuilder(16);
        for (int i = 0; i < getCardinality(); i++) {
            b.append(bindings.convert(nodes.get(i).eval(bindings, context), String.class));
        }
        return b.toString();
    }

    @Override
    public String toString() {
        return "composite";
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        for (int i = 0; i < getCardinality(); i++) {
            nodes.get(i).appendStructure(b, bindings);
        }
    }

    public int getCardinality() {
        return nodes.size();
    }

    public AstNode getChild(int i) {
        return nodes.get(i);
    }
}

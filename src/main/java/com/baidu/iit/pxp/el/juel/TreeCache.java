package com.baidu.iit.pxp.el.juel;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:59
 */
public interface TreeCache {

    public Tree get(String expression);


    public void put(String expression, Tree tree);
}


package com.baidu.iit.pxp.el.juel;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.MethodInfo;
import com.baidu.iit.pxp.el.ValueReference;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:20
 */
public interface ExpressionNode extends Node {

    public boolean isLiteralText();


    public boolean isLeftValue();


    public boolean isMethodInvocation();

    public Object getValue(Bindings bindings, ELContext context, Class<?> expectedType);


    public ValueReference getValueReference(Bindings bindings, ELContext context);


    public Class<?> getType(Bindings bindings, ELContext context);


    public boolean isReadOnly(Bindings bindings, ELContext context);


    public void setValue(Bindings bindings, ELContext context, Object value);


    public MethodInfo getMethodInfo(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes);


    public Object invoke(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes, Object[] paramValues);


    public String getStructuralId(Bindings bindings);
}

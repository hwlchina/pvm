package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ELException;
import com.baidu.iit.pxp.el.MethodInfo;
import com.baidu.iit.pxp.el.ValueReference;
import com.baidu.iit.pxp.el.juel.Bindings;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:43
 */
public final class AstText extends AstNode {
    private final String value;

    public AstText(String value) {
        this.value = value;
    }

    public boolean isLiteralText() {
        return true;
    }

    public boolean isLeftValue() {
        return false;
    }

    public boolean isMethodInvocation() {
        return false;
    }

    public Class<?> getType(Bindings bindings, ELContext context) {
        return null;
    }

    public boolean isReadOnly(Bindings bindings, ELContext context) {
        return true;
    }

    public void setValue(Bindings bindings, ELContext context, Object value) {
        throw new ELException(String.format("Cannot set rvalue value %s", getStructuralId(bindings)));
    }

    public ValueReference getValueReference(Bindings bindings, ELContext context) {
        return null;
    }

    @Override
    public Object eval(Bindings bindings, ELContext context) {
        return value;
    }

    public MethodInfo getMethodInfo(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes) {
        return null;
    }

    public Object invoke(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes, Object[] paramValues) {
        return returnType == null ? value : bindings.convert(value, returnType);
    }

    @Override
    public String toString() {
        return "\"" + value + "\"";
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        int end = value.length() - 1;
        for (int i = 0; i < end; i++) {
            char c = value.charAt(i);
            if ((c == '#' || c == '$') && value.charAt(i + 1) == '{') {
                b.append('\\');
            }
            b.append(c);
        }
        if (end >= 0) {
            b.append(value.charAt(end));
        }
    }

    public int getCardinality() {
        return 0;
    }

    public AstNode getChild(int i) {
        return null;
    }
}

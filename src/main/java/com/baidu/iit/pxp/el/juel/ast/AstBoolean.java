package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.juel.Bindings;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:35
 */
public final class AstBoolean extends AstLiteral {
    private final boolean value;

    public AstBoolean(boolean value) {
        this.value = value;
    }

    @Override
    public Object eval(Bindings bindings, ELContext context) {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        b.append(value);
    }
}

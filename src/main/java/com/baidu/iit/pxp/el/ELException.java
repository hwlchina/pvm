package com.baidu.iit.pxp.el;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午2:39
 */
public class ELException extends RuntimeException {

    public ELException() {
        super();
    }

    public ELException(String message) {
        super(message);
    }

    public ELException(Throwable cause) {
        super(cause);
    }


    public ELException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.baidu.iit.pxp.el.resolver;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.PropertyNotWritableException;
import com.baidu.iit.pxp.el.juel.PropertyNotFoundException;

import java.beans.FeatureDescriptor;
import java.lang.reflect.Array;
import java.util.Iterator;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午5:39
 */
public class ArrayELResolver extends ELResolver {

    private final boolean readOnly;


    public ArrayELResolver() {
        this(false);
    }


    public ArrayELResolver(boolean readOnly) {
        this.readOnly = readOnly;
    }


    @Override
    public Class<?> getCommonPropertyType(ELContext context, Object base) {
        return isResolvable(base) ? Integer.class : null;
    }


    @Override
    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
        return null;
    }


    @Override
    public Class<?> getType(ELContext context, Object base, Object property) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        Class<?> result = null;
        if (isResolvable(base)) {
            toIndex(base, property);
            result = base.getClass().getComponentType();
            context.setPropertyResolved(true);
        }
        return result;
    }


    @Override
    public Object getValue(ELContext context, Object base, Object property) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        Object result = null;
        if (isResolvable(base)) {
            int index = toIndex(null, property);
            result = index < 0 || index >= Array.getLength(base) ? null : Array.get(base, index);
            context.setPropertyResolved(true);
        }
        return result;
    }

    @Override
    public boolean isReadOnly(ELContext context, Object base, Object property) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (isResolvable(base)) {
            toIndex(base, property);
            context.setPropertyResolved(true);
        }
        return readOnly;
    }

    @Override
    public void setValue(ELContext context, Object base, Object property, Object value) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (isResolvable(base)) {
            if (readOnly) {
                throw new PropertyNotWritableException("resolver is read-only");
            }
            Array.set(base, toIndex(base, property), value);
            context.setPropertyResolved(true);
        }
    }

    private final boolean isResolvable(Object base) {
        return base != null && base.getClass().isArray();
    }

    private final int toIndex(Object base, Object property) {
        int index = 0;
        if (property instanceof Number) {
            index = ((Number) property).intValue();
        } else if (property instanceof String) {
            try {
                index = Integer.valueOf((String) property);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Cannot parse array index: " + property);
            }
        } else if (property instanceof Character) {
            index = ((Character) property).charValue();
        } else if (property instanceof Boolean) {
            index = ((Boolean) property).booleanValue() ? 1 : 0;
        } else {
            throw new IllegalArgumentException("Cannot coerce property to array index: " + property);
        }
        if (base != null && (index < 0 || index >= Array.getLength(base))) {
            throw new PropertyNotFoundException("Array index out of bounds: " + index);
        }
        return index;
    }
}

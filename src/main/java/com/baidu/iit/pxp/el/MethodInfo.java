package com.baidu.iit.pxp.el;

/**
 *
 * 表达式中的方法信息
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:45
 *
 */
public class MethodInfo {
    private final String name;
    private final Class<?> returnType;
    private final Class<?>[] paramTypes;

    public MethodInfo(String name, Class<?> returnType, Class<?>[] paramTypes) {
        this.name = name;
        this.returnType = returnType;
        this.paramTypes = paramTypes;
    }

    public String getName() {
        return name;
    }

    public Class<?>[] getParamTypes() {
        return paramTypes;
    }

    public Class<?> getReturnType() {
        return returnType;
    }
}

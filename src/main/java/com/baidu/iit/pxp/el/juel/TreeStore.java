package com.baidu.iit.pxp.el.juel;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:59
 */
public class TreeStore {
    private final TreeCache cache;
    private final TreeBuilder builder;


    public TreeStore(TreeBuilder builder, TreeCache cache) {
        super();

        this.builder = builder;
        this.cache = cache;
    }

    public TreeBuilder getBuilder() {
        return builder;
    }


    public Tree get(String expression) throws TreeBuilderException {
        if (cache == null) {
            return builder.build(expression);
        }
        Tree tree = cache.get(expression);
        if (tree == null) {
            cache.put(expression, tree = builder.build(expression));
        }
        return tree;
    }
}

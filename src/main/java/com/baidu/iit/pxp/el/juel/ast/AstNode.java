package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.juel.Bindings;
import com.baidu.iit.pxp.el.juel.ExpressionNode;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午3:14
 */
public abstract class AstNode implements ExpressionNode {


    public final Object getValue(Bindings bindings, ELContext context, Class<?> type) {
        Object value = eval(bindings, context);
        if (type != null) {
            value = bindings.convert(value, type);
        }
        return value;
    }

    public abstract void appendStructure(StringBuilder builder, Bindings bindings);

    public abstract Object eval(Bindings bindings, ELContext context);

    public final String getStructuralId(Bindings bindings) {
        StringBuilder builder = new StringBuilder();
        appendStructure(builder, bindings);
        return builder.toString();
    }
}

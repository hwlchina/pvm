package com.baidu.iit.pxp.el;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:48
 */
public abstract class VariableMapper {

    public abstract ValueExpression resolveVariable(String variable);

    public abstract ValueExpression setVariable(String variable, ValueExpression expression);
}

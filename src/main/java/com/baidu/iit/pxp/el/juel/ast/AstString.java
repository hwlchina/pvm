package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.juel.Bindings;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:35
 */
public final class AstString extends AstLiteral {
    private final String value;

    public AstString(String value) {
        this.value = value;
    }

    @Override
    public Object eval(Bindings bindings, ELContext context) {
        return value;
    }

    @Override
    public String toString() {
        return "\"" + value + "\"";
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        b.append("'");
        int length = value.length();
        for (int i = 0; i < length; i++) {
            char c = value.charAt(i);
            if (c == '\\' || c == '\'') {
                b.append('\\');
            }
            b.append(c);
        }
        b.append("'");
    }
}

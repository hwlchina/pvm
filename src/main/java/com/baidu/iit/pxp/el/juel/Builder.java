package com.baidu.iit.pxp.el.juel;


import com.baidu.iit.pxp.el.*;
import com.baidu.iit.pxp.el.resolver.ELResolver;

import java.io.PrintWriter;
import java.util.EnumSet;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午3:09
 */
public class Builder implements TreeBuilder {

    public static enum Feature {

        METHOD_INVOCATIONS,

        NULL_PROPERTIES,

        VARARGS
    }

    protected final EnumSet<Feature> features;

    public Builder() {
        this.features = EnumSet.noneOf(Feature.class);
    }

    public Builder(Feature... features) {
        if (features == null || features.length == 0) {
            this.features = EnumSet.noneOf(Feature.class);
        } else if (features.length == 1) {
            this.features = EnumSet.of(features[0]);
        } else {
            Feature[] rest = new Feature[features.length - 1];
            for (int i = 1; i < features.length; i++) {
                rest[i - 1] = features[i];
            }
            this.features = EnumSet.of(features[0], rest);
        }
    }

    public boolean isEnabled(Feature feature) {
        return features.contains(feature);
    }


    public Tree build(String expression) throws TreeBuilderException {
        try {
            return createParser(expression).tree();
        } catch (Scanner.ScanException e) {
            throw new TreeBuilderException(expression, e.position, e.encountered, e.expected, e.getMessage());
        } catch (Parser.ParseException e) {
            throw new TreeBuilderException(expression, e.position, e.encountered, e.expected, e.getMessage());
        }
    }

    protected Parser createParser(String expression) {
        return new Parser(this, expression);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return features.equals(((Builder) obj).features);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    /**
     * Dump out abstract syntax tree for a given expression
     *
     * @param args array with one element, containing the expression string
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("usage: java " + Builder.class.getName() + " <expression string>");
            System.exit(1);
        }
        PrintWriter out = new PrintWriter(System.out);
        Tree tree = null;
        try {
            tree = new Builder(Feature.METHOD_INVOCATIONS).build(args[0]);
        } catch (TreeBuilderException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
        NodePrinter.dump(out, tree.getRoot());
        if (!tree.getFunctionNodes().iterator().hasNext() && !tree.getIdentifierNodes().iterator().hasNext()) {
            ELContext context = new ELContext() {
                @Override
                public VariableMapper getVariableMapper() {
                    return null;
                }

                @Override
                public FunctionMapper getFunctionMapper() {
                    return null;
                }

                @Override
                public ELResolver getELResolver() {
                    return null;
                }
            };
            out.print(">> ");
            try {
                out.println(tree.getRoot().getValue(new Bindings(null, null), context, null));
            } catch (ELException e) {
                out.println(e.getMessage());
            }
        }
        out.flush();
    }
}

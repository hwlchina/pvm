package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.*;
import com.baidu.iit.pxp.el.juel.Bindings;
import com.baidu.iit.pxp.el.juel.IdentifierNode;
import com.baidu.iit.pxp.el.juel.MethodNotFoundException;
import com.baidu.iit.pxp.el.juel.PropertyNotFoundException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:22
 */
public class AstIdentifier extends AstNode implements IdentifierNode {
    private final String name;
    private final int index;

    public AstIdentifier(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public Class<?> getType(Bindings bindings, ELContext context) {
        ValueExpression expression = bindings.getVariable(index);
        if (expression != null) {
            return expression.getType(context);
        }
        context.setPropertyResolved(false);
        Class<?> result = context.getELResolver().getType(context, null, name);
        if (!context.isPropertyResolved()) {
            throw new PropertyNotFoundException(String.format("Cannot resolve identifier %s", name));
        }
        return result;
    }


    public boolean isLeftValue() {
        return true;
    }

    public boolean isMethodInvocation() {
        return false;
    }

    public boolean isLiteralText() {
        return false;
    }

    public ValueReference getValueReference(Bindings bindings, ELContext context) {
        ValueExpression expression = bindings.getVariable(index);
        if (expression != null) {
            return expression.getValueReference(context);
        }
        return new ValueReference(null, name);
    }

    @Override
    public Object eval(Bindings bindings, ELContext context) {
        ValueExpression expression = bindings.getVariable(index);
        if (expression != null) {
            return expression.getValue(context);
        }
        context.setPropertyResolved(false);
        Object result = context.getELResolver().getValue(context, null, name);
        if (!context.isPropertyResolved()) {
            throw new PropertyNotFoundException(String.format("Cannot resolve identifier %s", name));
        }
        return result;
    }

    public void setValue(Bindings bindings, ELContext context, Object value) {
        ValueExpression expression = bindings.getVariable(index);
        if (expression != null) {
            expression.setValue(context, value);
            return;
        }
        context.setPropertyResolved(false);
        context.getELResolver().setValue(context, null, name, value);
        if (!context.isPropertyResolved()) {
            throw new PropertyNotFoundException(String.format("Cannot resolve identifier %s", name));
        }
    }

    public boolean isReadOnly(Bindings bindings, ELContext context) {
        ValueExpression expression = bindings.getVariable(index);
        if (expression != null) {
            return expression.isReadOnly(context);
        }
        context.setPropertyResolved(false);
        boolean result = context.getELResolver().isReadOnly(context, null, name);
        if (!context.isPropertyResolved()) {
            throw new PropertyNotFoundException(String.format("Cannot resolve identifier %s", name));
        }
        return result;
    }

    protected Method getMethod(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes) {
        Object value = eval(bindings, context);
        if (value == null) {
            throw new MethodNotFoundException(String.format("Cannot resolve identifier %s", name));
        }
        if (value instanceof Method) {
            Method method = (Method) value;
            if (returnType != null && !returnType.isAssignableFrom(method.getReturnType())) {
                throw new MethodNotFoundException(String.format("Cannot resolve identifier %s", name));
            }
            if (!Arrays.equals(method.getParameterTypes(), paramTypes)) {
                throw new MethodNotFoundException(String.format("Cannot resolve identifier %s", name));
            }
            return method;
        }
        throw new MethodNotFoundException(String.format("Cannot find method expression for identifier %s (found %s instead)", name, value.getClass()));
    }

    public MethodInfo getMethodInfo(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes) {
        Method method = getMethod(bindings, context, returnType, paramTypes);
        return new MethodInfo(method.getName(), method.getReturnType(), paramTypes);
    }

    public Object invoke(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes, Object[] params) {
        Method method = getMethod(bindings, context, returnType, paramTypes);
        try {
            return method.invoke(null, params);
        } catch (IllegalAccessException e) {
            throw new ELException(String.format("Cannot access method {0}", name));
        } catch (IllegalArgumentException e) {
            throw new ELException(String.format("Error invoking method %s: %s", name, e));
        } catch (InvocationTargetException e) {
            throw new ELException(String.format("Error invoking method %s: %s", name, e.getCause()));
        }
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        b.append(bindings != null && bindings.isVariableBound(index) ? "<var>" : name);
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public int getCardinality() {
        return 0;
    }

    public AstNode getChild(int i) {
        return null;
    }
}

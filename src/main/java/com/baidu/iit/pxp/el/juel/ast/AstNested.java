package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.juel.Bindings;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午5:25
 */
public final class AstNested extends AstRightValue {
    private final AstNode child;

    public AstNested(AstNode child) {
        this.child = child;
    }

    @Override
    public Object eval(Bindings bindings, ELContext context) {
        return child.eval(bindings, context);
    }

    @Override
    public String toString() {
        return "(...)";
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        b.append("(");
        child.appendStructure(b, bindings);
        b.append(")");
    }

    public int getCardinality() {
        return 1;
    }

    public AstNode getChild(int i) {
        return i == 0 ? child : null;
    }
}

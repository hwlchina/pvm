package com.baidu.iit.pxp.el;

import com.baidu.iit.pxp.el.resolver.ELResolver;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:47
 */
public abstract class ELContext {
    private final Map<Class<?>, Object> context = new HashMap<Class<?>, Object>();

    private Locale locale;
    private boolean resolved;

    public Object getContext(Class<?> key) {
        return context.get(key);
    }

    public abstract ELResolver getELResolver();

    public abstract FunctionMapper getFunctionMapper();

    public Locale getLocale() {
        return locale;
    }

    public abstract VariableMapper getVariableMapper();

    public boolean isPropertyResolved() {
        return resolved;
    }

    public void putContext(Class<?> key, Object contextObject) {
        context.put(key, contextObject);
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public void setPropertyResolved(boolean resolved) {
        this.resolved = resolved;
    }
}

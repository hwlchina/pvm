package com.baidu.iit.pxp.el;

import com.baidu.iit.pxp.el.resolver.ELResolver;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:55
 */
public class ActivitiElContext extends ELContext {

    protected ELResolver elResolver;

    public ActivitiElContext(ELResolver elResolver) {
        this.elResolver = elResolver;
    }

    public ELResolver getELResolver() {
        return elResolver;
    }

    public FunctionMapper getFunctionMapper() {
        return new ActivitiFunctionMapper();
    }

    public VariableMapper getVariableMapper() {
        return null;
    }
}

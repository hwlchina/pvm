package com.baidu.iit.pxp.el;

import java.lang.reflect.Method;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午2:33
 */
public abstract class FunctionMapper {

    public abstract Method resolveFunction(String prefix, String localName);
}

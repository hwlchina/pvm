package com.baidu.iit.pxp.el.juel;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ValueExpression;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:06
 */
public class ExpressionSetInvocation extends ExpressionInvocation {

    protected final Object value;
    protected ELContext elContext;

    public ExpressionSetInvocation(ValueExpression valueExpression, ELContext elContext, Object value) {
        super(valueExpression);
        this.value = value;
        this.elContext = elContext;
        this.invocationParameters = new Object[]{value};
    }

    @Override
    protected void invoke() throws Exception {
        valueExpression.setValue(elContext, value);
    }

}

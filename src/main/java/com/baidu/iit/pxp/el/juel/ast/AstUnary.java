package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ELException;
import com.baidu.iit.pxp.el.TypeConverter;
import com.baidu.iit.pxp.el.juel.Bindings;
import com.baidu.iit.pxp.el.juel.operation.BooleanOperations;
import com.baidu.iit.pxp.el.juel.operation.NumberOperations;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:17
 */
public class AstUnary extends AstRightValue {
    public interface Operator {
        public Object eval(Bindings bindings, ELContext context, AstNode node);
    }

    public static abstract class SimpleOperator implements Operator {
        public Object eval(Bindings bindings, ELContext context, AstNode node) {
            return apply(bindings, node.eval(bindings, context));
        }

        protected abstract Object apply(TypeConverter converter, Object o);
    }


    public static final Operator EMPTY = new SimpleOperator() {
        @Override
        public Object apply(TypeConverter converter, Object o) {
            return BooleanOperations.empty(converter, o);
        }

        @Override
        public String toString() {
            return "empty";
        }
    };

    public static final Operator NEG = new SimpleOperator() {
        @Override
        public Object apply(TypeConverter converter, Object o) {
            return NumberOperations.neg(converter, o);
        }

        @Override
        public String toString() {
            return "-";
        }
    };

    public static final Operator NOT = new SimpleOperator() {
        @Override
        public Object apply(TypeConverter converter, Object o) {
            return !converter.convert(o, Boolean.class);
        }

        @Override
        public String toString() {
            return "!";
        }
    };

    private final Operator operator;
    private final AstNode child;

    public AstUnary(AstNode child, AstUnary.Operator operator) {
        this.child = child;
        this.operator = operator;
    }

    public Operator getOperator() {
        return operator;
    }

    @Override
    public Object eval(Bindings bindings, ELContext context) throws ELException {
        return operator.eval(bindings, context, child);
    }

    @Override
    public String toString() {
        return "'" + operator.toString() + "'";
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        b.append(operator);
        b.append(' ');
        child.appendStructure(b, bindings);
    }

    public int getCardinality() {
        return 1;
    }

    public AstNode getChild(int i) {
        return i == 0 ? child : null;
    }
}

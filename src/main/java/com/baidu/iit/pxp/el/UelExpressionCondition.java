package com.baidu.iit.pxp.el;

import com.baidu.iit.pvm.ActivityException;
import com.baidu.iit.pvm.delegate.Condition;
import com.baidu.iit.pvm.delegate.DelegateExecution;
import com.baidu.iit.pxp.delegate.Expression;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 上午10:05
 */
public class UelExpressionCondition implements Condition {

    protected Expression expression;

    public UelExpressionCondition(Expression expression) {
        this.expression = expression;
    }

    public boolean evaluate(DelegateExecution execution) {
        Object result = expression.getValue(execution);

        if (result==null) {
            throw new ActivityException("condition expression returns null");
        }
        if (! (result instanceof Boolean)) {
            throw new ActivityException("condition expression returns non-Boolean: "+result+" ("+result.getClass().getName()+")");
        }
        return (Boolean) result;
    }

}

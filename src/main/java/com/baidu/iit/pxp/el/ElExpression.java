package com.baidu.iit.pxp.el;

import com.baidu.iit.pvm.delegate.VariableScope;
import com.baidu.iit.pxp.delegate.Expression;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:52
 */
public interface ElExpression extends Expression {

    Object getValue(VariableScope variableScope);

    void setValue(Object value, VariableScope variableScope);

    String getExpressionText();

}
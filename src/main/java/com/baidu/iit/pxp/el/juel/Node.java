package com.baidu.iit.pxp.el.juel;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:18
 */
public interface Node {

    public int getCardinality();

    public Node getChild(int i);
}

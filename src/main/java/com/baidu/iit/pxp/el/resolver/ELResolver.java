package com.baidu.iit.pxp.el.resolver;

import com.baidu.iit.pxp.el.ELContext;

import java.beans.FeatureDescriptor;
import java.util.Iterator;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 下午2:31
 */
public abstract class ELResolver {

    public static final String RESOLVABLE_AT_DESIGN_TIME = "resolvableAtDesignTime";


    public static final String TYPE = "type";


    public abstract Class<?> getCommonPropertyType(ELContext context, Object base);

    public abstract Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base);


    public abstract Class<?> getType(ELContext context, Object base, Object property);

    public abstract Object getValue(ELContext context, Object base, Object property);


    public abstract void setValue(ELContext context, Object base, Object property, Object value);


    public Object invoke(ELContext context, Object base, Object method, Class<?>[] paramTypes, Object[] params) {
        return null;
    }

    public abstract boolean isReadOnly(ELContext context, Object base, Object property);

}

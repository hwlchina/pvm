package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.juel.Bindings;

import java.util.List;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:21
 */
public class AstParameters extends AstRightValue {
    private final List<AstNode> nodes;

    public AstParameters(List<AstNode> nodes) {
        this.nodes = nodes;
    }

    @Override
    public Object[] eval(Bindings bindings, ELContext context) {
        Object[] result = new Object[nodes.size()];
        for (int i = 0; i < nodes.size(); i++) {
            result[i] = nodes.get(i).eval(bindings, context);
        }
        return result;
    }

    @Override
    public String toString() {
        return "(...)";
    }

    @Override
    public void appendStructure(StringBuilder builder, Bindings bindings) {
        builder.append("(");
        for (int i = 0; i < nodes.size(); i++) {
            if (i > 0) {
                builder.append(", ");
            }
            nodes.get(i).appendStructure(builder, bindings);
        }
        builder.append(")");
    }

    public int getCardinality() {
        return nodes.size();
    }

    public AstNode getChild(int i) {
        return nodes.get(i);
    }
}

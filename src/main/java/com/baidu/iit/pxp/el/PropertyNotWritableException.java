package com.baidu.iit.pxp.el;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午5:44
 */
public class PropertyNotWritableException extends ELException {
    private static final long serialVersionUID = 1L;

    public PropertyNotWritableException() {
        super();
    }


    public PropertyNotWritableException(String message) {
        super(message);
    }


    public PropertyNotWritableException(Throwable cause) {
        super(cause);
    }


    public PropertyNotWritableException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.baidu.iit.pxp.el;

import com.baidu.iit.pxp.data.ExpressionData;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:44
 */
public abstract class MethodExpression extends ExpressionData {
    private static final long serialVersionUID = 1L;

    public abstract MethodInfo getMethodInfo(ELContext context);

    public abstract Object invoke(ELContext context, Object[] params);

    public boolean isParmetersProvided() {
        return false;
    }
}

package com.baidu.iit.pxp.el;

import com.baidu.iit.pvm.ActivityException;
import com.baidu.iit.pvm.delegate.VariableScope;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午1:57
 */
public class FixedValue implements ElExpression {

    private static final long serialVersionUID = 1L;
    private Object value;

    public FixedValue(Object value) {
        this.value = value;
    }

    public Object getValue(VariableScope variableScope) {
        return value;
    }

    public void setValue(Object value, VariableScope variableScope) {
        throw new ActivityException("Cannot change fixed value");
    }

    public String getExpressionText() {
        return value.toString();
    }

}

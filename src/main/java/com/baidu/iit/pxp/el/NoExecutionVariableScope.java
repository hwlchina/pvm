package com.baidu.iit.pxp.el;

import com.baidu.iit.pvm.delegate.VariableScope;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * User: huangweili
 * Date: 14-4-30
 * Time: 下午4:07
 */
public class NoExecutionVariableScope implements VariableScope {

    private static final NoExecutionVariableScope INSTANCE = new NoExecutionVariableScope();

    public static NoExecutionVariableScope getSharedInstance()  {
        return INSTANCE;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> getVariables() {
        return Collections.EMPTY_MAP;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> getVariablesLocal() {
        return Collections.EMPTY_MAP;
    }

    public Object getVariable(String variableName) {
        return null;
    }

    public Object getVariableLocal(String variableName) {
        return null;
    }

    @SuppressWarnings("unchecked")
    public Set<String> getVariableNames() {
        return Collections.EMPTY_SET;
    }

    public Set<String> getVariableNamesLocal() {
        return null;
    }

    public void setVariable(String variableName, Object value) {
        throw new UnsupportedOperationException("No execution active, no variables can be set");
    }

    public Object setVariableLocal(String variableName, Object value) {
        throw new UnsupportedOperationException("No execution active, no variables can be set");
    }

    public void setVariables(Map<String, ? extends Object> variables) {
        throw new UnsupportedOperationException("No execution active, no variables can be set");
    }

    public void setVariablesLocal(Map<String, ? extends Object> variables) {
        throw new UnsupportedOperationException("No execution active, no variables can be set");
    }

    public boolean hasVariables() {
        return false;
    }

    public boolean hasVariablesLocal() {
        return false;
    }

    public boolean hasVariable(String variableName) {
        return false;
    }

    public boolean hasVariableLocal(String variableName) {
        return false;
    }

    public void createVariableLocal(String variableName, Object value) {
        throw new UnsupportedOperationException("No execution active, no variables can be created");
    }

    public void createVariablesLocal(Map<String, ? extends Object> variables) {
        throw new UnsupportedOperationException("No execution active, no variables can be created");
    }

    public void removeVariable(String variableName) {
        throw new UnsupportedOperationException("No execution active, no variables can be removed");
    }

    public void removeVariableLocal(String variableName) {
        throw new UnsupportedOperationException("No execution active, no variables can be removed");
    }

    public void removeVariables() {
        throw new UnsupportedOperationException("No execution active, no variables can be removed");
    }

    public void removeVariablesLocal() {
        throw new UnsupportedOperationException("No execution active, no variables can be removed");
    }

    public void removeVariables(Collection<String> variableNames) {
        throw new UnsupportedOperationException("No execution active, no variables can be removed");
    }

    public void removeVariablesLocal(Collection<String> variableNames) {
        throw new UnsupportedOperationException("No execution active, no variables can be removed");
    }

}

package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ELException;
import com.baidu.iit.pxp.el.MethodInfo;
import com.baidu.iit.pxp.el.ValueReference;
import com.baidu.iit.pxp.el.juel.Bindings;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午3:33
 */
public abstract class AstRightValue extends AstNode {

    public final boolean isLiteralText() {
        return false;
    }


    public final Class<?> getType(Bindings bindings, ELContext context) {
        return null;
    }


    public final boolean isReadOnly(Bindings bindings, ELContext context) {
        return true;
    }

    public final void setValue(Bindings bindings, ELContext context, Object value) {
        throw new ELException(String.format("Cannot set value of a non-lvalue expression %s", getStructuralId(bindings)));
    }

    public final MethodInfo getMethodInfo(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes) {
        return null;
    }

    public final Object invoke(Bindings bindings, ELContext context, Class<?> returnType, Class<?>[] paramTypes, Object[] paramValues) {
        throw new ELException(String.format("Expression %s is not a valid method expression", getStructuralId(bindings)));
    }

    public final boolean isLeftValue() {
        return false;
    }

    public boolean isMethodInvocation() {
        return false;
    }

    public final ValueReference getValueReference(Bindings bindings, ELContext context) {
        return null;
    }
}

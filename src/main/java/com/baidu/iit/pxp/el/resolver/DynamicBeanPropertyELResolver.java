package com.baidu.iit.pxp.el.resolver;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ELException;
import com.baidu.iit.pxp.util.ReflectUtil;

import java.beans.FeatureDescriptor;
import java.util.Iterator;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午5:40
 */
public class DynamicBeanPropertyELResolver extends ELResolver {

    protected Class<?> subject;

    protected String readMethodName;

    protected String writeMethodName;

    protected boolean readOnly;

    public DynamicBeanPropertyELResolver(boolean readOnly, Class<?> subject, String readMethodName, String writeMethodName) {
        this.readOnly = readOnly;
        this.subject = subject;
        this.readMethodName = readMethodName;
        this.writeMethodName = writeMethodName;
    }

    public DynamicBeanPropertyELResolver(Class<?> subject, String readMethodName, String writeMethodName) {
        this(false, subject, readMethodName, writeMethodName);
    }

    @Override
    public Class<?> getCommonPropertyType(ELContext context, Object base) {
        if (this.subject.isInstance(base)) {
            return Object.class;
        } else {
            return null;
        }
    }

    @Override
    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
        return null;
    }

    @Override
    public Class<?> getType(ELContext context, Object base, Object property) {
        return Object.class;
    }

    @Override
    public Object getValue(ELContext context, Object base, Object property) {
        if (base == null || this.getCommonPropertyType(context, base) == null) {
            return null;
        }

        String propertyName = property.toString();

        try {
            Object value = ReflectUtil.invoke(base, this.readMethodName, new Object[]{propertyName});
            context.setPropertyResolved(true);
            return value;
        } catch (Exception e) {
            throw new ELException(e);
        }
    }

    @Override
    public boolean isReadOnly(ELContext context, Object base, Object property) {
        return this.readOnly;
    }

    @Override
    public void setValue(ELContext context, Object base, Object property, Object value) {
        if (base == null || this.getCommonPropertyType(context, base) == null) {
            return;
        }

        String propertyName = property.toString();
        try {
            ReflectUtil.invoke(base, this.writeMethodName, new Object[]{propertyName, value});
            context.setPropertyResolved(true);
        } catch (Exception e) {
            throw new ELException(e);
        }
    }
}

package com.baidu.iit.pxp.el;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:10
 */
public class TypeConverterImpl implements TypeConverter {


    protected Boolean coerceToBoolean(Object value) {
        if (value == null || "".equals(value)) {
            return Boolean.FALSE;
        }
        if (value instanceof Boolean) {
            return (Boolean)value;
        }
        if (value instanceof String) {
            return Boolean.valueOf((String)value);
        }
        throw new ELException("error.coerce.type");
    }

    protected Character coerceToCharacter(Object value) {
        if (value == null || "".equals(value)) {
            return Character.valueOf((char)0);
        }
        if (value instanceof Character) {
            return (Character)value;
        }
        if (value instanceof Number) {
            return Character.valueOf((char)((Number)value).shortValue());
        }
        if (value instanceof String) {
            return Character.valueOf(((String)value).charAt(0));
        }
        throw new ELException("error.coerce.type");
    }

    protected BigDecimal coerceToBigDecimal(Object value) {
        if (value == null || "".equals(value)) {
            return BigDecimal.valueOf(0l);
        }
        if (value instanceof BigDecimal) {
            return (BigDecimal)value;
        }
        if (value instanceof BigInteger) {
            return new BigDecimal((BigInteger)value);
        }
        if (value instanceof Number) {
            return new BigDecimal(((Number)value).doubleValue());
        }
        if (value instanceof String) {
            try {
                return new BigDecimal((String)value);
            } catch (NumberFormatException e) {
                throw new ELException("error.coerce.value");
            }
        }
        if (value instanceof Character) {
            return new BigDecimal((short)((Character)value).charValue());
        }
        throw new ELException("error.coerce.type");
    }

    protected BigInteger coerceToBigInteger(Object value) {
        if (value == null || "".equals(value)) {
            return BigInteger.valueOf(0l);
        }
        if (value instanceof BigInteger) {
            return (BigInteger)value;
        }
        if (value instanceof BigDecimal) {
            return ((BigDecimal)value).toBigInteger();
        }
        if (value instanceof Number) {
            return BigInteger.valueOf(((Number)value).longValue());
        }
        if (value instanceof String) {
            try {
                return new BigInteger((String)value);
            } catch (NumberFormatException e) {
                throw new ELException("error.coerce.value");
            }
        }
        if (value instanceof Character) {
            return BigInteger.valueOf((short)((Character)value).charValue());
        }
        throw new ELException("error.coerce.type");
    }

    protected Double coerceToDouble(Object value) {
        if (value == null || "".equals(value)) {
            return Double.valueOf(0);
        }
        if (value instanceof Double) {
            return (Double)value;
        }
        if (value instanceof Number) {
            return Double.valueOf(((Number)value).doubleValue());
        }
        if (value instanceof String) {
            try {
                return Double.valueOf((String)value);
            } catch (NumberFormatException e) {
                throw new ELException("error.coerce.value");
            }
        }
        if (value instanceof Character) {
            return Double.valueOf((short)((Character)value).charValue());
        }
        throw new ELException("error.coerce.type");
    }

    protected Float coerceToFloat(Object value) {
        if (value == null || "".equals(value)) {
            return Float.valueOf(0);
        }
        if (value instanceof Float) {
            return (Float)value;
        }
        if (value instanceof Number) {
            return Float.valueOf(((Number)value).floatValue());
        }
        if (value instanceof String) {
            try {
                return Float.valueOf((String)value);
            } catch (NumberFormatException e) {
                throw new ELException("error.coerce.value");
            }
        }
        if (value instanceof Character) {
            return Float.valueOf((short)((Character)value).charValue());
        }
        throw new ELException("error.coerce.type");
    }

    protected Long coerceToLong(Object value) {
        if (value == null || "".equals(value)) {
            return Long.valueOf(0l);
        }
        if (value instanceof Long) {
            return (Long)value;
        }
        if (value instanceof Number) {
            return Long.valueOf(((Number)value).longValue());
        }
        if (value instanceof String) {
            try {
                return Long.valueOf((String)value);
            } catch (NumberFormatException e) {
                throw new ELException("error.coerce.value");
            }
        }
        if (value instanceof Character) {
            return Long.valueOf((short)((Character)value).charValue());
        }
        throw new ELException("error.coerce.type");
    }

    protected Integer coerceToInteger(Object value) {
        if (value == null || "".equals(value)) {
            return Integer.valueOf(0);
        }
        if (value instanceof Integer) {
            return (Integer)value;
        }
        if (value instanceof Number) {
            return Integer.valueOf(((Number)value).intValue());
        }
        if (value instanceof String) {
            try {
                return Integer.valueOf((String)value);
            } catch (NumberFormatException e) {
                throw new ELException("error.coerce.value");
            }
        }
        if (value instanceof Character) {
            return Integer.valueOf((short)((Character)value).charValue());
        }
        throw new ELException("error.coerce.type");
    }

    protected Short coerceToShort(Object value) {
        if (value == null || "".equals(value)) {
            return Short.valueOf((short)0);
        }
        if (value instanceof Short) {
            return (Short)value;
        }
        if (value instanceof Number) {
            return Short.valueOf(((Number)value).shortValue());
        }
        if (value instanceof String) {
            try {
                return Short.valueOf((String)value);
            } catch (NumberFormatException e) {
                throw new ELException("error.coerce.value");
            }
        }
        if (value instanceof Character) {
            return Short.valueOf((short)((Character)value).charValue());
        }
        throw new ELException("error.coerce.type");
    }

    protected Byte coerceToByte(Object value) {
        if (value == null || "".equals(value)) {
            return Byte.valueOf((byte)0);
        }
        if (value instanceof Byte) {
            return (Byte)value;
        }
        if (value instanceof Number) {
            return Byte.valueOf(((Number)value).byteValue());
        }
        if (value instanceof String) {
            try {
                return Byte.valueOf((String)value);
            } catch (NumberFormatException e) {
                throw new ELException("error.coerce.value");
            }
        }
        if (value instanceof Character) {
            return Byte.valueOf(Short.valueOf((short)((Character)value).charValue()).byteValue());
        }
        throw new ELException("error.coerce.type");
    }

    protected String coerceToString(Object value) {
        if (value == null) {
            return "";
        }
        if (value instanceof String) {
            return (String)value;
        }
        if (value instanceof Enum<?>) {
            return ((Enum<?>)value).name();
        }
        return value.toString();
    }

    @SuppressWarnings("unchecked")
    protected <T extends Enum<T>> T coerceToEnum(Object value, Class<T> type) {
        if (value == null || "".equals(value)) {
            return null;
        }
        if (type.isInstance(value)) {
            return (T)value;
        }
        if (value instanceof String) {
            try {
                return Enum.valueOf(type, (String)value);
            } catch (IllegalArgumentException e) {
                throw new ELException("error.coerce.value");
            }
        }
        throw new ELException("error.coerce.type");
    }

    protected Object coerceStringToType(String value, Class<?> type) {
        PropertyEditor editor = PropertyEditorManager.findEditor(type);
        if (editor == null) {
            if ("".equals(value)) {
                return null;
            }
            throw new ELException("error.coerce.type");
        } else {
            if ("".equals(value)) {
                try {
                    editor.setAsText(value);
                } catch (IllegalArgumentException e) {
                    return null;
                }
            } else {
                try {
                    editor.setAsText(value);
                } catch (IllegalArgumentException e) {
                    throw new ELException("error.coerce.value");
                }
            }
            return editor.getValue();
        }
    }

    @SuppressWarnings("unchecked")
    protected Object coerceToType(Object value, Class<?> type) {
        if (type == String.class) {
            return coerceToString(value);
        }
        if (type == Long.class || type == long.class) {
            return coerceToLong(value);
        }
        if (type == Double.class || type == double.class) {
            return coerceToDouble(value);
        }
        if (type == Boolean.class || type == boolean.class) {
            return coerceToBoolean(value);
        }
        if (type == Integer.class || type == int.class) {
            return coerceToInteger(value);
        }
        if (type == Float.class || type == float.class) {
            return coerceToFloat(value);
        }
        if (type == Short.class || type == short.class) {
            return coerceToShort(value);
        }
        if (type == Byte.class || type == byte.class) {
            return coerceToByte(value);
        }
        if (type == Character.class || type == char.class) {
            return coerceToCharacter(value);
        }
        if (type == BigDecimal.class) {
            return coerceToBigDecimal(value);
        }
        if (type == BigInteger.class) {
            return coerceToBigInteger(value);
        }
        if (type.getSuperclass() == Enum.class) {
            return coerceToEnum(value, (Class<? extends Enum>)type);
        }
        if (value == null || value.getClass() == type || type.isInstance(value)) {
            return value;
        }
        if (value instanceof String) {
            return coerceStringToType((String)value, type);
        }
        throw new ELException("error.coerce.type");
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass().equals(getClass());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @SuppressWarnings("unchecked")
    public <T> T convert(Object value, Class<T> type) throws ELException {
        return (T)coerceToType(value, type);
    }
}

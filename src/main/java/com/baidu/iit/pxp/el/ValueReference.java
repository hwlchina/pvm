package com.baidu.iit.pxp.el;

import java.io.Serializable;

/**
 * User: huangweili
 * Date: 14-4-28
 * Time: 上午10:54
 */
public class ValueReference implements Serializable {
    private Object base;
    private Object property;

    public ValueReference(Object base, Object property) {
        this.base = base;
        this.property = property;
    }

    public Object getBase() {
        return base;
    }

    public Object getProperty() {
        return property;
    }
}
package com.baidu.iit.pxp.el;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午2:07
 */
public final class ObjectValueExpression extends ValueExpression {
    private static final long serialVersionUID = 1L;

    private final TypeConverter converter;
    private final Object object;
    private final Class<?> type;


    public ObjectValueExpression(TypeConverter converter, Object object, Class<?> type) {
        super();

        this.converter = converter;
        this.object = object;
        this.type = type;

        if (type == null) {
            throw new NullPointerException("error.value.notype");
        }
    }


    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            ObjectValueExpression other = (ObjectValueExpression)obj;
            if (type != other.type) {
                return false;
            }
            return object == other.object || object != null && object.equals(other.object);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return object == null ? 0 : object.hashCode();
    }

    @Override
    public Object getValue(ELContext context) {
        return converter.convert(object, type);
    }


    @Override
    public String getExpressionString() {
        return null;
    }


    @Override
    public boolean isLiteralText() {
        return false;
    }


    @Override
    public Class<?> getType(ELContext context) {
        return null;
    }

    /**
     * Answer <code>true</code>.
     */
    @Override
    public boolean isReadOnly(ELContext context) {
        return true;
    }


    @Override
    public void setValue(ELContext context, Object value) {
        throw new ELException("<object value expression>");
    }

    @Override
    public String toString() {
        return "ValueExpression(" + object + ")";
    }

    @Override
    public Class<?> getExpectedType() {
        return type;
    }
}

package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ELException;
import com.baidu.iit.pxp.el.juel.Bindings;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:48
 */
public class AstDot extends AstProperty {
    protected final String property;

    public AstDot(AstNode base, String property, boolean lvalue) {
        super(base, lvalue, true);
        this.property = property;
    }

    @Override
    protected String getProperty(Bindings bindings, ELContext context) throws ELException {
        return property;
    }

    @Override
    public String toString() {
        return ". " + property;
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        getChild(0).appendStructure(b, bindings);
        b.append(".");
        b.append(property);
    }

    public int getCardinality() {
        return 1;
    }
}

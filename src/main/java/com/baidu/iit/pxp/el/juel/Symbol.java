package com.baidu.iit.pxp.el.juel;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午5:10
 */
public enum Symbol {
    EOF,
    PLUS("'+'"), MINUS("'-'"),
    MUL("'*'"), DIV("'/'|'div'"), MOD("'%'|'mod'"),
    LPAREN("'('"), RPAREN("')'"),
    IDENTIFIER,
    NOT("'!'|'not'"), AND("'&&'|'and'"), OR("'||'|'or'"),
    EMPTY("'empty'"), INSTANCEOF("'instanceof'"),
    INTEGER, FLOAT, TRUE("'true'"), FALSE("'false'"), STRING, NULL("'null'"),
    LE("'<='|'le'"), LT("'<'|'lt'"), GE("'>='|'ge'"), GT("'>'|'gt'"),
    EQ("'=='|'eq'"), NE("'!='|'ne'"),
    QUESTION("'?'"), COLON("':'"),
    TEXT,
    DOT("'.'"), LBRACK("'['"), RBRACK("']'"),
    COMMA("','"),
    START_EVAL_DEFERRED("'#{'"), START_EVAL_DYNAMIC("'${'"), END_EVAL("'}'"),
    EXTENSION; // used in syntax extensions
    private final String string;

    private Symbol() {
        this(null);
    }

    private Symbol(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string == null ? "<" + name() + ">" : string;
    }
}

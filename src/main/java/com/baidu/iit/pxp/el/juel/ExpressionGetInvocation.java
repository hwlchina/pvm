package com.baidu.iit.pxp.el.juel;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ValueExpression;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午4:05
 */
public class ExpressionGetInvocation extends ExpressionInvocation {

    protected final ELContext elContext;

    public ExpressionGetInvocation(ValueExpression valueExpression, ELContext elContext) {
        super(valueExpression);
        this.elContext = elContext;
    }

    protected void invoke() throws Exception {
        invocationResult = valueExpression.getValue(elContext);
    }

}
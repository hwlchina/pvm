package com.baidu.iit.pxp.el.juel;

import com.baidu.iit.pxp.el.ELException;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午3:54
 */
public class MethodNotFoundException extends ELException {
    private static final long serialVersionUID = 1L;


    public MethodNotFoundException() {
        super();
    }

    public MethodNotFoundException(String message) {
        super(message);
    }

    public MethodNotFoundException(Throwable cause) {
        super(cause);
    }

    public MethodNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

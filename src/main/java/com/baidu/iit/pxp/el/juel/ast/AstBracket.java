package com.baidu.iit.pxp.el.juel.ast;

import com.baidu.iit.pxp.el.ELContext;
import com.baidu.iit.pxp.el.ELException;
import com.baidu.iit.pxp.el.juel.Bindings;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午3:51
 */
public class AstBracket extends AstProperty {
    protected final AstNode property;

    public AstBracket(AstNode base, AstNode property, boolean lvalue, boolean strict) {
        super(base, lvalue, strict);
        this.property = property;
    }

    @Override
    protected Object getProperty(Bindings bindings, ELContext context) throws ELException {
        return property.eval(bindings, context);
    }

    @Override
    public String toString() {
        return "[...]";
    }

    @Override
    public void appendStructure(StringBuilder b, Bindings bindings) {
        getChild(0).appendStructure(b, bindings);
        b.append("[");
        getChild(1).appendStructure(b, bindings);
        b.append("]");
    }

    public int getCardinality() {
        return 2;
    }

    @Override
    public AstNode getChild(int i) {
        return i == 1 ? property : super.getChild(i);
    }
}

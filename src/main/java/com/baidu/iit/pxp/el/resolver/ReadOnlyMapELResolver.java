package com.baidu.iit.pxp.el.resolver;

import com.baidu.iit.pvm.ActivityException;
import com.baidu.iit.pxp.el.ELContext;

import java.beans.FeatureDescriptor;
import java.util.Iterator;
import java.util.Map;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午1:51
 */
public class ReadOnlyMapELResolver extends ELResolver {

    protected Map<Object, Object> wrappedMap;

    public ReadOnlyMapELResolver(Map<Object, Object> map) {
        this.wrappedMap = map;
    }

    public Object getValue(ELContext context, Object base, Object property) {
        if (base == null) {
            if (wrappedMap.containsKey(property)) {
                context.setPropertyResolved(true);
                return wrappedMap.get(property);
            }
        }
        return null;
    }

    public boolean isReadOnly(ELContext context, Object base, Object property) {
        return true;
    }

    public void setValue(ELContext context, Object base, Object property, Object value) {
        if(base == null) {
            if (wrappedMap.containsKey(property)) {
                throw new ActivityException("Cannot set value of '" + property + "', it's readonly!");
            }
        }
    }

    public Class< ? > getCommonPropertyType(ELContext context, Object arg) {
        return Object.class;
    }

    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object arg) {
        return null;
    }

    public Class< ? > getType(ELContext context, Object arg1, Object arg2) {
        return Object.class;
    }
}

package com.baidu.iit.pxp.el.juel;

import com.baidu.iit.pxp.el.juel.ast.*;

import java.util.*;

/**
 * User: huangweili
 * Date: 14-4-29
 * Time: 下午3:11
 */
public class Parser {

    public static class ParseException extends Exception {
        final int position;
        final String encountered;
        final String expected;

        public ParseException(int position, String encountered, String expected) {
            super(String.format("syntax error at position %s, encountered %s, expected %s", position, encountered, expected));
            this.position = position;
            this.encountered = encountered;
            this.expected = expected;
        }
    }



    private static final class LookaheadToken {
        final Scanner.Token token;
        final int position;

        LookaheadToken(Scanner.Token token, int position) {
            this.token = token;
            this.position = position;
        }
    }

    public enum ExtensionPoint {
        OR,
        AND,
        EQ,
        CMP,
        ADD,
        MUL,
        UNARY,
        LITERAL
    }

    public static abstract class ExtensionHandler {
        private final ExtensionPoint point;

        public ExtensionHandler(ExtensionPoint point) {
            this.point = point;
        }

        public ExtensionPoint getExtensionPoint() {
            return point;
        }


        public abstract AstNode createAstNode(AstNode... children);
    }

    private static final String EXPR_FIRST =
            Symbol.IDENTIFIER + "|" +
                    Symbol.STRING + "|" + Symbol.FLOAT + "|" + Symbol.INTEGER + "|" + Symbol.TRUE + "|" + Symbol.FALSE + "|" + Symbol.NULL + "|" +
                    Symbol.MINUS + "|" + Symbol.NOT + "|" + Symbol.EMPTY + "|" +
                    Symbol.LPAREN;

    protected final Builder context;
    protected final Scanner scanner;

    private List<IdentifierNode> identifiers = Collections.emptyList();
    private List<FunctionNode> functions = Collections.emptyList();
    private List<LookaheadToken> lookahead = Collections.emptyList();

    private Scanner.Token token;
    private int position;

    protected Map<Scanner.ExtensionToken, ExtensionHandler> extensions = Collections.emptyMap();

    public Parser(Builder context, String input) {
        this.context = context;
        this.scanner = createScanner(input);
    }

    protected Scanner createScanner(String expression) {
        return new Scanner(expression);
    }

    public void putExtensionHandler(Scanner.ExtensionToken token, ExtensionHandler extension) {
        if (extensions.isEmpty()) {
            extensions = new HashMap<Scanner.ExtensionToken, ExtensionHandler>(16);
        }
        extensions.put(token, extension);
    }

    protected ExtensionHandler getExtensionHandler(Scanner.Token token) {
        return extensions.get(token);
    }


    protected Number parseInteger(String string) throws ParseException {
        try {
            return Long.valueOf(string);
        } catch (NumberFormatException e) {
            fail(Symbol.INTEGER);
            return null;
        }
    }


    protected Number parseFloat(String string) throws ParseException {
        try {
            return Double.valueOf(string);
        } catch (NumberFormatException e) {
            fail(Symbol.FLOAT);
            return null;
        }
    }

    protected AstBinary createAstBinary(AstNode left, AstNode right, AstBinary.Operator operator) {
        return new AstBinary(left, right, operator);
    }

    protected AstBracket createAstBracket(AstNode base, AstNode property, boolean lvalue, boolean strict) {
        return new AstBracket(base, property, lvalue, strict);
    }

    protected AstChoice createAstChoice(AstNode question, AstNode yes, AstNode no) {
        return new AstChoice(question, yes, no);
    }

    protected AstComposite createAstComposite(List<AstNode> nodes) {
        return new AstComposite(nodes);
    }

    protected AstDot createAstDot(AstNode base, String property, boolean lvalue) {
        return new AstDot(base, property, lvalue);
    }

    protected AstFunction createAstFunction(String name, int index, AstParameters params) {
        return new AstFunction(name, index, params, context.isEnabled(Builder.Feature.VARARGS));
    }

    protected AstIdentifier createAstIdentifier(String name, int index) {
        return new AstIdentifier(name, index);
    }

    protected AstMethod createAstMethod(AstProperty property, AstParameters params) {
        return new AstMethod(property, params);
    }

    protected AstUnary createAstUnary(AstNode child, AstUnary.Operator operator) {
        return new AstUnary(child, operator);
    }

    protected final List<FunctionNode> getFunctions() {
        return functions;
    }

    protected final List<IdentifierNode> getIdentifiers() {
        return identifiers;
    }

    protected final Scanner.Token getToken() {
        return token;
    }


    protected void fail(String expected) throws ParseException {
        throw new ParseException(position, "'" + token.getImage() + "'", expected);
    }


    protected void fail(Symbol expected) throws ParseException {
        fail(expected.toString());
    }

    protected final Scanner.Token lookahead(int index) throws Scanner.ScanException, ParseException {
        if (lookahead.isEmpty()) {
            lookahead = new LinkedList<LookaheadToken>();
        }
        while (index >= lookahead.size()) {
            lookahead.add(new LookaheadToken(scanner.next(), scanner.getPosition()));
        }
        return lookahead.get(index).token;
    }

    protected final Scanner.Token consumeToken() throws Scanner.ScanException, ParseException {
        Scanner.Token result = token;
        if (lookahead.isEmpty()) {
            token = scanner.next();
            position = scanner.getPosition();
        } else {
            LookaheadToken next = lookahead.remove(0);
            token = next.token;
            position = next.position;
        }
        return result;
    }

    protected final Scanner.Token consumeToken(Symbol expected) throws Scanner.ScanException, ParseException {
        if (token.getSymbol() != expected) {
            fail(expected);
        }
        return consumeToken();
    }

    public Tree tree() throws Scanner.ScanException, ParseException {
        consumeToken();
        AstNode t = text();
        if (token.getSymbol() == Symbol.EOF) {
            if (t == null) {
                t = new AstText("");
            }
            return new Tree(t, functions, identifiers, false);
        }
        AstEval e = eval();
        if (token.getSymbol() == Symbol.EOF && t == null) {
            return new Tree(e, functions, identifiers, e.isDeferred());
        }
        ArrayList<AstNode> list = new ArrayList<AstNode>();
        if (t != null) {
            list.add(t);
        }
        list.add(e);
        t = text();
        if (t != null) {
            list.add(t);
        }
        while (token.getSymbol() != Symbol.EOF) {
            if (e.isDeferred()) {
                list.add(eval(true, true));
            } else {
                list.add(eval(true, false));
            }
            t = text();
            if (t != null) {
                list.add(t);
            }
        }
        return new Tree(createAstComposite(list), functions, identifiers, e.isDeferred());
    }


    protected AstNode text() throws Scanner.ScanException, ParseException {
        AstNode v = null;
        if (token.getSymbol() == Symbol.TEXT) {
            v = new AstText(token.getImage());
            consumeToken();
        }
        return v;
    }

    protected AstEval eval() throws Scanner.ScanException, ParseException {
        AstEval e = eval(false, false);
        if (e == null) {
            e = eval(false, true);
            if (e == null) {
                fail(Symbol.START_EVAL_DEFERRED + "|" + Symbol.START_EVAL_DYNAMIC);
            }
        }
        return e;
    }

    protected AstEval eval(boolean required, boolean deferred) throws Scanner.ScanException, ParseException {
        AstEval v = null;
        Symbol start_eval = deferred ? Symbol.START_EVAL_DEFERRED : Symbol.START_EVAL_DYNAMIC;
        if (token.getSymbol() == start_eval) {
            consumeToken();
            v = new AstEval(expr(true), deferred);
            consumeToken(Symbol.END_EVAL);
        } else if (required) {
            fail(start_eval);
        }
        return v;
    }

    protected AstNode expr(boolean required) throws Scanner.ScanException, ParseException {
        AstNode v = or(required);
        if (v == null) {
            return null;
        }
        if (token.getSymbol() == Symbol.QUESTION) {
            consumeToken();
            AstNode a = expr(true);
            consumeToken(Symbol.COLON);
            AstNode b = expr(true);
            v = createAstChoice(v, a, b);
        }
        return v;
    }

    protected AstNode or(boolean required) throws Scanner.ScanException, ParseException {
        AstNode v = and(required);
        if (v == null) {
            return null;
        }
        while (true) {
            switch (token.getSymbol()) {
                case OR:
                    consumeToken();
                    v = createAstBinary(v, and(true), AstBinary.OR);
                    break;
                case EXTENSION:
                    if (getExtensionHandler(token).getExtensionPoint() == ExtensionPoint.OR) {
                        v = getExtensionHandler(consumeToken()).createAstNode(v, and(true));
                        break;
                    }
                default:
                    return v;
            }
        }
    }

    protected AstNode and(boolean required) throws Scanner.ScanException, ParseException {
        AstNode v = eq(required);
        if (v == null) {
            return null;
        }
        while (true) {
            switch (token.getSymbol()) {
                case AND:
                    consumeToken();
                    v = createAstBinary(v, eq(true), AstBinary.AND);
                    break;
                case EXTENSION:
                    if (getExtensionHandler(token).getExtensionPoint() == ExtensionPoint.AND) {
                        v = getExtensionHandler(consumeToken()).createAstNode(v, eq(true));
                        break;
                    }
                default:
                    return v;
            }
        }
    }

    protected AstNode eq(boolean required) throws Scanner.ScanException, ParseException {
        AstNode v = cmp(required);
        if (v == null) {
            return null;
        }
        while (true) {
            switch (token.getSymbol()) {
                case EQ:
                    consumeToken();
                    v = createAstBinary(v, cmp(true), AstBinary.EQ);
                    break;
                case NE:
                    consumeToken();
                    v = createAstBinary(v, cmp(true), AstBinary.NE);
                    break;
                case EXTENSION:
                    if (getExtensionHandler(token).getExtensionPoint() == ExtensionPoint.EQ) {
                        v = getExtensionHandler(consumeToken()).createAstNode(v, cmp(true));
                        break;
                    }
                default:
                    return v;
            }
        }
    }

    protected AstNode cmp(boolean required) throws Scanner.ScanException, ParseException {
        AstNode v = add(required);
        if (v == null) {
            return null;
        }
        while (true) {
            switch (token.getSymbol()) {
                case LT:
                    consumeToken();
                    v = createAstBinary(v, add(true), AstBinary.LT);
                    break;
                case LE:
                    consumeToken();
                    v = createAstBinary(v, add(true), AstBinary.LE);
                    break;
                case GE:
                    consumeToken();
                    v = createAstBinary(v, add(true), AstBinary.GE);
                    break;
                case GT:
                    consumeToken();
                    v = createAstBinary(v, add(true), AstBinary.GT);
                    break;
                case EXTENSION:
                    if (getExtensionHandler(token).getExtensionPoint() == ExtensionPoint.CMP) {
                        v = getExtensionHandler(consumeToken()).createAstNode(v, add(true));
                        break;
                    }
                default:
                    return v;
            }
        }
    }

    protected AstNode add(boolean required) throws Scanner.ScanException, ParseException {
        AstNode v = mul(required);
        if (v == null) {
            return null;
        }
        while (true) {
            switch (token.getSymbol()) {
                case PLUS:
                    consumeToken();
                    v = createAstBinary(v, mul(true), AstBinary.ADD);
                    break;
                case MINUS:
                    consumeToken();
                    v = createAstBinary(v, mul(true), AstBinary.SUB);
                    break;
                case EXTENSION:
                    if (getExtensionHandler(token).getExtensionPoint() == ExtensionPoint.ADD) {
                        v = getExtensionHandler(consumeToken()).createAstNode(v, mul(true));
                        break;
                    }
                default:
                    return v;
            }
        }
    }

    protected AstNode mul(boolean required) throws Scanner.ScanException, ParseException {
        AstNode v = unary(required);
        if (v == null) {
            return null;
        }
        while (true) {
            switch (token.getSymbol()) {
                case MUL:
                    consumeToken();
                    v = createAstBinary(v, unary(true), AstBinary.MUL);
                    break;
                case DIV:
                    consumeToken();
                    v = createAstBinary(v, unary(true), AstBinary.DIV);
                    break;
                case MOD:
                    consumeToken();
                    v = createAstBinary(v, unary(true), AstBinary.MOD);
                    break;
                case EXTENSION:
                    if (getExtensionHandler(token).getExtensionPoint() == ExtensionPoint.MUL) {
                        v = getExtensionHandler(consumeToken()).createAstNode(v, unary(true));
                        break;
                    }
                default:
                    return v;
            }
        }
    }

    protected AstNode unary(boolean required) throws Scanner.ScanException, ParseException {
        AstNode v = null;
        switch (token.getSymbol()) {
            case NOT:
                consumeToken();
                v = createAstUnary(unary(true), AstUnary.NOT);
                break;
            case MINUS:
                consumeToken();
                v = createAstUnary(unary(true), AstUnary.NEG);
                break;
            case EMPTY:
                consumeToken();
                v = createAstUnary(unary(true), AstUnary.EMPTY);
                break;
            case EXTENSION:
                if (getExtensionHandler(token).getExtensionPoint() == ExtensionPoint.UNARY) {
                    v = getExtensionHandler(consumeToken()).createAstNode(unary(true));
                    break;
                }
            default:
                v = value();
        }
        if (v == null && required) {
            fail(EXPR_FIRST);
        }
        return v;
    }

    protected AstNode value() throws Scanner.ScanException, ParseException {
        boolean lvalue = true;
        AstNode v = nonliteral();
        if (v == null) {
            v = literal();
            if (v == null) {
                return null;
            }
            lvalue = false;
        }
        while (true) {
            switch (token.getSymbol()) {
                case DOT:
                    consumeToken();
                    String name = consumeToken(Symbol.IDENTIFIER).getImage();
                    AstDot dot = createAstDot(v, name, lvalue);
                    if (token.getSymbol() == Symbol.LPAREN && context.isEnabled(Builder.Feature.METHOD_INVOCATIONS)) {
                        v = createAstMethod(dot, params());
                    } else {
                        v = dot;
                    }
                    break;
                case LBRACK:
                    consumeToken();
                    AstNode property = expr(true);
                    boolean strict = !context.isEnabled(Builder.Feature.NULL_PROPERTIES);
                    consumeToken(Symbol.RBRACK);
                    AstBracket bracket = createAstBracket(v, property, lvalue, strict);
                    if (token.getSymbol() == Symbol.LPAREN && context.isEnabled(Builder.Feature.METHOD_INVOCATIONS)) {
                        v = createAstMethod(bracket, params());
                    } else {
                        v = bracket;
                    }
                    break;
                default:
                    return v;
            }
        }
    }

    protected AstNode nonliteral() throws Scanner.ScanException, ParseException {
        AstNode v = null;
        switch (token.getSymbol()) {
            case IDENTIFIER:
                String name = consumeToken().getImage();
                if (token.getSymbol() == Symbol.COLON && lookahead(0).getSymbol() == Symbol.IDENTIFIER && lookahead(1).getSymbol() == Symbol.LPAREN) {
                    consumeToken();
                    name += ":" + token.getImage();
                    consumeToken();
                }
                if (token.getSymbol() == Symbol.LPAREN) { // function
                    v = function(name, params());
                } else {
                    v = identifier(name);
                }
                break;
            case LPAREN:
                consumeToken();
                v = expr(true);
                consumeToken(Symbol.RPAREN);
                v = new AstNested(v);
                break;
        }
        return v;
    }


    protected AstParameters params() throws Scanner.ScanException, ParseException {
        consumeToken(Symbol.LPAREN);
        List<AstNode> l = Collections.emptyList();
        AstNode v = expr(false);
        if (v != null) {
            l = new ArrayList<AstNode>();
            l.add(v);
            while (token.getSymbol() == Symbol.COMMA) {
                consumeToken();
                l.add(expr(true));
            }
        }
        consumeToken(Symbol.RPAREN);
        return new AstParameters(l);
    }


    protected AstNode literal() throws Scanner.ScanException, ParseException {
        AstNode v = null;
        switch (token.getSymbol()) {
            case TRUE:
                v = new AstBoolean(true);
                consumeToken();
                break;
            case FALSE:
                v = new AstBoolean(false);
                consumeToken();
                break;
            case STRING:
                v = new AstString(token.getImage());
                consumeToken();
                break;
            case INTEGER:
                v = new AstNumber(parseInteger(token.getImage()));
                consumeToken();
                break;
            case FLOAT:
                v = new AstNumber(parseFloat(token.getImage()));
                consumeToken();
                break;
            case NULL:
                v = new AstNull();
                consumeToken();
                break;
            case EXTENSION:
                if (getExtensionHandler(token).getExtensionPoint() == ExtensionPoint.LITERAL) {
                    v = getExtensionHandler(consumeToken()).createAstNode();
                    break;
                }
        }
        return v;
    }

    protected final AstFunction function(String name, AstParameters params) {
        if (functions.isEmpty()) {
            functions = new ArrayList<FunctionNode>(4);
        }
        AstFunction function = createAstFunction(name, functions.size(), params);
        functions.add(function);
        return function;
    }

    protected final AstIdentifier identifier(String name) {
        if (identifiers.isEmpty()) {
            identifiers = new ArrayList<IdentifierNode>(4);
        }
        AstIdentifier identifier = createAstIdentifier(name, identifiers.size());
        identifiers.add(identifier);
        return identifier;
    }
}